﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class TournamentPersonScore
    {
        [Key]
        [Display(Name = "ID")]
        public int TournamentPersonScoreId { get; set; }

        [Required(ErrorMessage = "Please enter team score!")]
        [Display(Name = "Score")]
        public decimal Score { get; set; }

        [Display(Name = "Person Id")]
        public int SoloId { get; set; }
        [Display(Name = "Person")]
        public virtual Solo Solo { get; set; }

        [Display(Name = "Tournament id")]
        public int TournamentId { get; set; }
        [Display(Name = "Tournament")]
        public virtual Tournament Tournament { get; set; }
    }
}
