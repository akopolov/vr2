﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class PartyMatch
    {
        [Key]
        public int PartyMatchId { get; set; }

        [Display(Name="Match result")]
        [Range(0,Int32.MaxValue)]
        public int Result { get; set; }

        [Display(Name = "Party Id")]
        public int PartyId { get; set; }
        [Display(Name = "Party")]
        public virtual Party Party { get; set; }

        [Display(Name="Match Id")]
        public int GameMatchId { get; set; }
        [Display(Name = "Match")]
        public virtual GameMatch GameMatch { get; set; }
    }
}
