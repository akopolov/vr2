﻿namespace Domain.Enum
{
    public enum PartyRoll
    {
        None = 0,
        Leader,
        Tank,
        DPS,
        Support,
        Nuke,
    }
}
