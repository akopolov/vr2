﻿using System;

namespace Domain.Enum
{
    public sealed class GameMatchType
    {
        private readonly String _name;
        private readonly int _id;

        public static readonly GameMatchType None = new GameMatchType(0,"None");
        public static readonly GameMatchType CaptureTheFlag = new GameMatchType(1, "Capture The Flag");
        public static readonly GameMatchType TeamDeathmatch = new GameMatchType(2, "Team Deathmatch");
        

        private GameMatchType(int id, String name)
        {
            _name = name;
            _id = id;
        }

        public override String ToString()
        {
            return _name;
        }
    }
}
