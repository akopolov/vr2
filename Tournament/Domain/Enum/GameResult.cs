﻿namespace Domain.Enum
{
    public enum GameResult
    {
        None = 0,
        Winner,
        Loser,
        Draw,
    }
}
