﻿namespace Domain.Enum
{
    public enum TeamRoll
    {
        NoRoll = 0,
        Manager,
        Leader,
        Member,
    }
}
