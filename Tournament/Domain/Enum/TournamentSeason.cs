﻿namespace Domain.Enum
{
    public enum TournamentSeason
    {
        None = 0,
        Spring,
        Summer,
        Fall,
        Winter,
    }
}
