﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class SoloMatch
    {
        [Key]
        [Display(Name = "ID")]
        public int SoloMatchId { get; set; }

        [Display(Name = "Match Result")]
        [Range(0,Int32.MaxValue)]
        public int GameResult { get; set; }

        [Display(Name = "Person Id")]
        public int SoloId { get; set; }
        [Display(Name = "Person")]
        public virtual Solo Solo { get; set; }

        [Display(Name = "Match Id")]
        public int GameMatchId { get; set; }
        [Display(Name = "Match")]
        public virtual GameMatch GameMatch { get; set; }
    }
}
