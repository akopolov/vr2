﻿using Domain.Enum;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class TeamPersonInParty
    {
        [Key]
        [Display(Name = "ID")]
        public int TeamPersonInPartyId { get; set; }

        [Display(Name = "Party Id")]
        public int PartyId { get; set; }
        [Display(Name = "Party")]
        public virtual Party Party { get; set; }

        [Display(Name = "Person Id")]
        public int PersonInTeamId { get; set; }
        [Display(Name = "Person")]
        public virtual PersonInTeam PersonInTeam { get; set; }

        [Display(Name = "Party roll")]
        public PartyRoll PartyRoll { get; set; }
    }
}
