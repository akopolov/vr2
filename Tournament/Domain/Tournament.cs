﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Enum;
using Domain.Identity;

namespace Domain
{
    public class Tournament
    {
        [Key]
        [Display(Name = "ID")]
        public int TournamentId { get; set; }

        [Required(ErrorMessage = "Please enter tournament name!")]
        [StringLength(70, ErrorMessage = "Tournament name can not be longer than 70 characters!")]
        [Display(Name = "Tournament name")]
        public string TournamentName { get; set; }

        [Required(ErrorMessage = "Starting date must be provided")]
        [DataType(DataType.Date)]
        [Display(Name = "From date")]
        public DateTime TournamentFromDate { get; set; }

        [Required(ErrorMessage = "End date must be provided")]
        [DataType(DataType.Date)]
        [Display(Name = "To date")]
        public DateTime TournamentToDate { get; set; }

        [Display(Name = "Game id")]
        public int GameId { get; set; }
        [Display(Name = "Game")]
        public virtual Game Game { get; set; }

        [Display(Name = "Season")]
        public TournamentSeason TournamentSeason { get; set; }

        [Display(Name = "Tournament admin id")]
        public int AdminUserId { get; set; }
        [Display(Name = "Tournament admin")]
        public UserInt AdminUser { get; set; }

        public virtual List<GameMatch> GameMatches { get; set; }
        public virtual List<TournamentPersonScore> TournamentPersonScores { get; set; }
        public virtual List<TournamentPartyScore> TournamentPartyScores { get; set; }
    }
}
