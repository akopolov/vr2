﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class GameMode
    {
        [Key]
        [Display(Name = "ID")]
        public int GameModeId { get; set; }

        [Required(ErrorMessage = "Please enter game mode name!")]
        [StringLength(50, ErrorMessage = "Contact type name can not be longer than 50 characters!")]
        [Display(Name = "Game mode")]
        public string GameModeName { get; set; }

        [Required(ErrorMessage = "Please enter the number of players in one team!")]
        [Range(1,Int32.MaxValue)]
        [Display(Name = "Players in one team")]
        public int PlayersInOneTeam { get; set; }

        [Display(Name = "Game id")]
        public int GameId { get; set; }
        [Display(Name = "Game")]
        public virtual Game Game { get; set; }

        public virtual List<GameMatch> GameMatches { get; set; }
    }
}
