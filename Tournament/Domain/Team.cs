﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Team
    {
        [Key]
        [Display(Name = "ID")]
        public int TeamId { get; set; }

        [Required(ErrorMessage = "Please enter team name!")]
        [StringLength(50, ErrorMessage = "Contact type name can not be longer than 50 characters!")]
        [Display(Name = "Name")]
        public string TeamName { get; set; }

        [Required(ErrorMessage = "Please enter team short name!")]
        [StringLength(8, ErrorMessage = "Contact type name can not be longer than 8 characters!")]
        [Display(Name = "Short name")]
        public string TeamShortName { get; set; }

        [Required(ErrorMessage = "Please enter team score!")]
        [DisplayFormat(DataFormatString = "{0:#0.00}", ApplyFormatInEditMode = true)]
        [Display(Name = "Team rating")]
        public decimal TeamRating { get; set; }

        public virtual List<PersonInTeam> PersonsInTeam { get; set; }
        public virtual List<Party> Parties { get; set; }
    }
}
