﻿using Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Identity;

namespace Domain
{
    public class PersonInTeam
    {
        [Key]
        [Display(Name = "ID")]
        public int PersonInTeamId { get; set; }

        [Required(ErrorMessage = "Starting date must be provided")]
        [DataType(DataType.Date)]
        [Display(Name = "From date")]
        public DateTime PersonInTeamFromDate { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "To date")]
        public DateTime? PersonInTeamToDate { get; set; }

        [Display(Name = "Team Id")]
        public int TeamId { get; set; }
        [Display(Name = "Team")]
        public virtual Team Team { get; set; }

        [Display(Name = "Person Id")]
        public int UserId { get; set; }
        [Display(Name = "Person")]
        public virtual UserInt User { get; set; }

        [Display(Name = "Roll name")]
        public TeamRoll TeamRoll { get; set; }

        public virtual List<TeamPersonInParty> TeamPersonInParties { get; set; }
    }
}
