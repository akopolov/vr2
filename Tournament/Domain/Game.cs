﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Game
    {
        [Key]
        [Display(Name = "ID")]
        public int GameId { get; set; }

        [Required(ErrorMessage = "Please enter game name!")]
        [StringLength(50, ErrorMessage = "Game name can not be longer than 50 characters!")]
        [Display(Name = "Game name")]
        public string GameName { get; set; }

        public virtual List<Party> Parties { get; set; }
        public virtual List<Solo> Solos { get; set; }
        public virtual List<GameMode> GameModes { get; set; }
        public virtual List<Tournament> Tournaments { get; set; }

    }
}
