﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Identity;

namespace Domain
{
    public class Solo
    {
        [Key]
        public int SoloId { get; set; }

        [Display(Name = "Person score")]
        [Range(0,Double.MaxValue)]
        public decimal SoloScore { get; set; }

        [Display(Name = "User Id")]
        public int UserIntId { get; set; }
        [Display(Name = "User")]
        public UserInt UserInt { get; set; }

        [Display(Name = "Game Id")]
        public int GameId { get; set; }
        [Display(Name = "Game")]
        public Game Game { get; set; }

        public List<TournamentPersonScore> TournamentPersonScores { get; set; }
    }
}
