﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class GameMatch
    {
        [Key]
        [Display(Name = "ID")]
        public int GameMatchId { get; set; }

        [Required(ErrorMessage = "Starting date and time must be provided")]
        [DataType(DataType.DateTime)]
        [Display(Name = "Start")]
        public DateTime GameMatchStart { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "End")]
        public DateTime? GameMatchEnd { get; set; }

        [Display(Name = "Game mode Id")]
        public int GameModeId { get; set; }
        [Display(Name = "Game mode")]
        public virtual GameMode GameMode { get; set; }

        [Display(Name = "Tournament Id")]
        public int TournamentId { get; set; }
        [Display(Name = "Tournament")]
        public virtual Tournament Tournament { get; set; }

        public virtual List<SoloMatch> SoloMatches { get; set; }
        public virtual List<PartyMatch> PartyMatches { get; set; }
    }
}
