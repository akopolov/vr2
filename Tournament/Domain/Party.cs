﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Party
    {
        [Key]
        [Display(Name = "ID")]
        public int PartyId { get; set; }

        [Required]
        [Display(Name = "Party name")]
        [StringLength(70, ErrorMessage = "Party name can not be longer than 70 chars")]
        public string PartyName { get; set; }

        [Required]
        [Display(Name = "PartyScore")]
        [Range(0,Double.MaxValue)]
        public decimal PartyScore { get; set; }

        [Display(Name = "Game Id")]
        public int GameId { get; set; }
        [Display(Name = "Game")]
        public Game Game { get; set; }

        [Display(Name="Team id")]
        public int TeamId { get; set; }
        [Display(Name = "Team")]
        public virtual Team Team { get; set; }

        public virtual List<PartyMatch> PartyMatches { get; set; }
        public virtual List<TeamPersonInParty> TeamPersonInParties { get; set; }
        public virtual List<TournamentPartyScore> TournamentPartyScores { get; set; }
    }
}
