﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class TournamentPartyScore
    {
        [Key]
        [Display(Name = "ID")]
        public int TournamentPartyScoreId { get; set; }

        [Required(ErrorMessage = "Please enter team score!")]
        [DisplayFormat(DataFormatString = "{0:#0.00}", ApplyFormatInEditMode = true)]
        [Display(Name = "Score")]
        public decimal Score { get; set; }

        [Display(Name = "Party Id")]
        public int PartyId { get; set; }
        [Display(Name = "Party")]
        public virtual Party Party { get; set; }

        [Display(Name = "Tournament id")]
        public int TournamentId { get; set; }
        [Display(Name = "Tournament")]
        public virtual Tournament Tournament { get; set; }
    }
}
