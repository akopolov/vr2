﻿using System.Collections.Generic;
using Domain;

namespace Interfaces.IRepositories
{
    public interface ITournamentPersonScoreRepository : IRepository<TournamentPersonScore>
    {
        List<TournamentPersonScore> FetchAll();
        TournamentPersonScore FetchById(int id);
        List<TournamentPersonScore> GetByTournamentId(int tournamentId);
    }
}
