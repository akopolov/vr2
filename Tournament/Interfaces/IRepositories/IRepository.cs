﻿using System.Collections.Generic;

namespace Interfaces.IRepositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        List<TEntity> All { get; }

        TEntity Find(params object[] id);

        void Remove(params object[] id);

        void Remove(TEntity entity);

        TEntity Add(TEntity entity);

        void Update(TEntity entity);

        int SaveChanges();
    }
}
