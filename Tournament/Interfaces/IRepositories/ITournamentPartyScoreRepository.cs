﻿using System.Collections.Generic;
using Domain;

namespace Interfaces.IRepositories
{
    public interface ITournamentPartyScoreRepository : IRepository<TournamentPartyScore>
    {
        List<TournamentPartyScore> FetchAll();
        TournamentPartyScore FetchById(int id);
        List<TournamentPartyScore> GetByTournamentId(int tournamentId);
    }
}
