﻿using System.Collections.Generic;
using Domain;

namespace Interfaces.IRepositories
{
    public interface ITeamPersonInPartyRepository : IRepository<TeamPersonInParty>
    {
        List<TeamPersonInParty> FetchByPersonInTeam(int personInTeamId);
        List<TeamPersonInParty> FetchAll();
        TeamPersonInParty FetchById(int id);
    }
}
