﻿using System.Collections.Generic;
using Domain;

namespace Interfaces.IRepositories
{
    public interface IPartyRepository : IRepository<Party>
    {
        List<Party> FetchAll();
        Party FetchById(int id);
    }
}
