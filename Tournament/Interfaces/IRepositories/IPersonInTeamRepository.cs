﻿using System.Collections.Generic;
using Domain;

namespace Interfaces.IRepositories
{
    public interface IPersonInTeamRepository : IRepository<PersonInTeam>
    {
        List<PersonInTeam> FetchMembersByTeamId(int id);
        List<PersonInTeam> FetchAll();
        PersonInTeam FetchById(int id);
        List<PersonInTeam> FetchByTeamId(int id);
        List<PersonInTeam> FetchTeamManagers(int teamId);
    }
}
