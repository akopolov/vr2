﻿using System.Collections.Generic;
using Domain;

namespace Interfaces.IRepositories
{
    public interface IPartyMatchRepository : IRepository<PartyMatch>
    {
        List<PartyMatch> FetchAll();
        PartyMatch FetchById(int id);
    }
}
