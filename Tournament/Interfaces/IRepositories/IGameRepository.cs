﻿using Domain;

namespace Interfaces.IRepositories
{
    public interface IGameRepository : IRepository<Game>
    {

    }
}
