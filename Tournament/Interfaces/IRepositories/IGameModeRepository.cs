﻿using Domain;
using System.Collections.Generic;

namespace Interfaces.IRepositories
{
    public interface IGameModeRepository : IRepository<GameMode>
    {
        List<GameMode> FetchAll();
        GameMode FetchById(int id);
    }
}
