﻿using Domain;

namespace Interfaces.IRepositories
{
    public interface ITeamRepository : IRepository<Team>
    {
    }
}
