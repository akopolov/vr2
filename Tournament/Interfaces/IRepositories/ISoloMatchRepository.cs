﻿using System.Collections.Generic;
using Domain;

namespace Interfaces.IRepositories
{
    public interface ISoloMatchRepository : IRepository<SoloMatch>
    {
        List<SoloMatch> FetchAll();
        SoloMatch FetchById(int id);
    }
}
