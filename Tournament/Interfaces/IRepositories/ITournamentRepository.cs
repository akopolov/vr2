﻿using System.Collections.Generic;
using Domain;

namespace Interfaces.IRepositories
{
    public interface ITournamentRepository : IRepository<Tournament>
    {
        List<Tournament> FetchAll();
        Tournament FetchById(int id);
    }
}
