﻿using System.Collections.Generic;
using Domain;

namespace Interfaces.IRepositories
{
    public interface IGameMatchRepository : IRepository<GameMatch>
    {
        List<GameMatch> FetchAll();
        GameMatch FetchById(int id);
    }
}
