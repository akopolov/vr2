﻿using System.Collections.Generic;
using Domain;

namespace Interfaces.IRepositories
{
    public interface ISoloRepository : IRepository<Solo>
    {
        List<Solo> FetchAll();
        Solo FetchById(int id);
        List<Solo> FetchByGameId(int gameId);
        List<Solo> FetchByUserId(int userId);
    }
}
