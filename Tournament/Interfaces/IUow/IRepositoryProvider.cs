﻿using Interfaces.IRepositories;

namespace Interfaces.IUow
{
    public interface IRepositoryProvider
    {
        IRepository<TEntity> GetStandardRepo<TEntity>() where TEntity : class;
        TRepoInterface GetCustomRepo<TRepoInterface>();
    }
}
