﻿using Interfaces.IRepositories;
using System;

namespace Interfaces.IUow
{
    public interface IRepositoryFactory
    {
        Func<IDataContext, object> GetStandardRepositoryFactory<TEntity>() where TEntity : class;
        Func<IDataContext, object> GetCustomRepositoryFactory<TRepoInterface>();
    }
}
