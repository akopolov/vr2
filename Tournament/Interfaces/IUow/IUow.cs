﻿using Domain;
using Interfaces.IIdentity;
using Interfaces.IRepositories;
using System;
using System.Threading.Tasks;

namespace Interfaces.IUow
{
    public interface IUow : IDisposable
    {
        //Standart Repos
        IRepository<Team> Teams { get; }
        IRepository<Game> Games { get; }
        
        //Custom Repos
        ITeamPersonInPartyRepository TeamPersoneInParties { get; }
        IPersonInTeamRepository PersonInTeams { get; }
        ISoloMatchRepository SoloMatches { get; }
        IGameMatchRepository GameMatches { get; }
        IPartyRepository Parties { get; }
        IGameModeRepository GameModes { get; }
        ITournamentRepository Tournaments { get; }
        ITournamentPersonScoreRepository TournamentPersonScores { get; }
        ITournamentPartyScoreRepository TournamentPartyScores { get; }
        IPartyMatchRepository PartyMatches { get; }
        ISoloRepository Solos { get; }

        // Identity, PK - int
        IUserIntRepository UsersInt { get; }
        IUserRoleIntRepository UserRolesInt { get; }
        IRoleIntRepository RolesInt { get; }
        IUserClaimIntRepository UserClaimsInt { get; }
        IUserLoginIntRepository UserLoginsInt { get; }

        T GetRepository<T>() where T : class;
        int SaveChanges();
        Task<int> SaveChangesAsync();

    }
}
