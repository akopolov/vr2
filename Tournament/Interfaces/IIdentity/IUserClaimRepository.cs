﻿using System;
using System.Collections.Generic;
using Domain.Identity;
using Interfaces.IRepositories;

namespace Interfaces.IIdentity
{
    public interface IUserClaimIntRepository : IUserClaimRepository<int, UserClaimInt>
    {
    }

    public interface IUserClaimRepository : IUserClaimRepository<string, UserClaim>
    {
    }

    public interface IUserClaimRepository<TKey, TUserClaim> : IRepository<TUserClaim>
        where TUserClaim : class
        where TKey: IEquatable<TKey>
    {
        List<TUserClaim> AllIncludeUser();
        List<TUserClaim> AllForUserId(TKey userId);
    }
}