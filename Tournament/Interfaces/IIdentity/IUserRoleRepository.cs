﻿using Domain.Identity;
using Interfaces.IRepositories;

namespace Interfaces.IIdentity
{
    public interface IUserRoleIntRepository : IUserRoleRepository<int, UserRoleInt>
    {
    }

    public interface IUserRoleRepository : IUserRoleRepository<string, UserRole>
    {
    }

    public interface IUserRoleRepository<in TKey, TUserRole> : IRepository<TUserRole>
        where TUserRole : class
    {
        TUserRole GetByUserIdAndRoleId(TKey roleId, TKey userId);
    }
}