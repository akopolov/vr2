﻿using DAL.EFConfiguration;
using DAL.Helpers;
using Domain;
using Domain.Identity;
using Interfaces.IRepositories;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace DAL.Data
{
    public class AppDbContext : DbContext, IDataContext
    {
        public AppDbContext() : base(nameOrConnectionString: "name=TournamentDb")
        {
            Database.SetInitializer(new DbInit());
        }

        public IDbSet<Game> Games { get; set; }
        public IDbSet<GameMatch> GameMatches { get; set; }
        public IDbSet<GameMode> GameModes { get; set; }
        public IDbSet<Party> Parties { get; set; }
        public IDbSet<PersonInTeam> PersonInTeams { get; set; }
        public IDbSet<TeamPersonInParty> TeamPersonInParties { get; set; }
        public IDbSet<SoloMatch> SoloMatches { get; set; }
        public IDbSet<Team> Teams { get; set; }
        public IDbSet<Tournament> Tournaments { get; set; }
        public IDbSet<TournamentPersonScore> TournamentPersonScores { get; set; }
        public IDbSet<TournamentPartyScore> TournamentPartyScores { get; set; }
        public IDbSet<PartyMatch> PartyMatches { get; set; }
        public IDbSet<Domain.Solo> Solos { get; set; }

        // Identity tables, PK - int
        public IDbSet<RoleInt> RolesInt { get; set; }
        public IDbSet<UserClaimInt> UserClaimsInt { get; set; }
        public IDbSet<UserLoginInt> UserLoginsInt { get; set; }
        public IDbSet<UserInt> UsersInt { get; set; }
        public IDbSet<UserRoleInt> UserRolesInt { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);

            // remove tablename pluralizing
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            // remove cascade delete
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            // Identity, PK - int 
            modelBuilder.Configurations.Add(new RoleIntMap());
            modelBuilder.Configurations.Add(new UserClaimIntMap());
            modelBuilder.Configurations.Add(new UserLoginIntMap());
            modelBuilder.Configurations.Add(new UserIntMap());
            modelBuilder.Configurations.Add(new UserRoleIntMap());

            Precision.ConfigureModelBuilder(modelBuilder);
        }
    }
}