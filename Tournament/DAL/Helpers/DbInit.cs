﻿using DAL.Data;
using Domain.Identity;
using Microsoft.AspNet.Identity;
using System;
using System.Data.Entity;
using System.Linq;
using Domain;
using Domain.Enum;

namespace DAL.Helpers
{
    public class DbInit : DropCreateDatabaseAlways<AppDbContext>
    {
        protected override void Seed(AppDbContext context)
        {
            var pwdHasher = new PasswordHasher();
            Random random = new Random();


            if (!context.RolesInt.Any())
            {
                // Roles
                context.RolesInt.Add(new RoleInt()
                {
                    Name = "Admin",
                });
                context.RolesInt.Add(new RoleInt()
                {
                    Name = "User",
                });
                context.SaveChanges();
            }

            if (!context.UsersInt.Any())
            {
                // Users
                context.UsersInt.Add(new UserInt()
                {
                    UserName = "admin",
                    FirstName = "Super",
                    LastName = "User",
                    Birthday = DateTime.Now,
                    PasswordHash = pwdHasher.HashPassword("asd"),
                    SecurityStamp = Guid.NewGuid().ToString()
                });
                context.UsersInt.Add(new UserInt()
                {
                    UserName = "cool_user1",
                    FirstName = "Kalle",
                    LastName = "Kadakas",
                    Birthday = DateTime.Now,
                    PasswordHash = pwdHasher.HashPassword("asd"),
                    SecurityStamp = Guid.NewGuid().ToString()
                });
                context.UsersInt.Add(new UserInt()
                {
                    UserName = "not_cool2",
                    FirstName = "Mihkel",
                    LastName = "Mänd",
                    Birthday = DateTime.Now,
                    PasswordHash = pwdHasher.HashPassword("asd"),
                    SecurityStamp = Guid.NewGuid().ToString()
                });
                context.UsersInt.Add(new UserInt()
                {
                    UserName = "rympo",
                    FirstName = "Rynner",
                    LastName = "Remmor",
                    Birthday = DateTime.Now,
                    PasswordHash = pwdHasher.HashPassword("asd"),
                    SecurityStamp = Guid.NewGuid().ToString()
                });
                context.UsersInt.Add(new UserInt()
                {
                    UserName = "henry666",
                    FirstName = "Henri",
                    LastName = "Ilves",
                    Birthday = DateTime.Now,
                    PasswordHash = pwdHasher.HashPassword("asd"),
                    SecurityStamp = Guid.NewGuid().ToString()
                });
                context.UsersInt.Add(new UserInt()
                {
                    UserName = "uku",
                    FirstName = "Uku",
                    LastName = "Toots",
                    Birthday = DateTime.Now,
                    PasswordHash = pwdHasher.HashPassword("asd"),
                    SecurityStamp = Guid.NewGuid().ToString()
                });
                context.UsersInt.Add(new UserInt()
                {
                    UserName = "ju55ike",
                    FirstName = "Juss",
                    LastName = "Klaipeda",
                    Birthday = DateTime.Now,
                    PasswordHash = pwdHasher.HashPassword("asd"),
                    SecurityStamp = Guid.NewGuid().ToString()
                });
                context.UsersInt.Add(new UserInt()
                {
                    UserName = "karma",
                    FirstName = "Kermo",
                    LastName = "Spinner",
                    Birthday = DateTime.Now,
                    PasswordHash = pwdHasher.HashPassword("asd"),
                    SecurityStamp = Guid.NewGuid().ToString()
                });
                context.SaveChanges();
            }

            if (!context.UserRolesInt.Any())
            {
                // Users in Roles
                context.UserRolesInt.Add(new UserRoleInt()
                {
                    User = context.UsersInt.FirstOrDefault(a => a.UserName == "admin"),
                    Role = context.RolesInt.FirstOrDefault(a => a.Name == "Admin")
                });
                context.UserRolesInt.Add(new UserRoleInt()
                {
                    User = context.UsersInt.FirstOrDefault(a => a.UserName == "cool_user1"),
                    Role = context.RolesInt.FirstOrDefault(a => a.Name == "User")
                });
                context.UserRolesInt.Add(new UserRoleInt()
                {
                    User = context.UsersInt.FirstOrDefault(a => a.UserName == "not_cool2"),
                    Role = context.RolesInt.FirstOrDefault(a => a.Name == "User")
                });
                context.UserRolesInt.Add(new UserRoleInt()
                {
                    User = context.UsersInt.FirstOrDefault(a => a.UserName == "rympo"),
                    Role = context.RolesInt.FirstOrDefault(a => a.Name == "User")
                });
                context.SaveChanges();
            }

            if (!context.Teams.Any())
            {
                context.Teams.Add(new Team()
                {
                    TeamName = "EV",
                    TeamShortName = "EV",
                    TeamRating = 1000.00M
                });
                context.Teams.Add(new Team()
                {
                    TeamName = "Võitjad",
                    TeamShortName = "VTD",
                    TeamRating = 999.00M
                });
                context.Teams.Add(new Team()
                {
                    TeamName = "Täheke",
                    TeamShortName = "TK",
                    TeamRating = 456.00M
                });
                context.SaveChanges();
            }

            // .OrderBy(c => Guid.NewGuid())   <---- random

            if (!context.PersonInTeams.Any())
            {
                for (int i = 0; i <= 3; i++)
                {
                    context.PersonInTeams.Add(new PersonInTeam()
                    {
                        User = context.UsersInt.FirstOrDefault(),
                        Team = context.Teams.FirstOrDefault(),
                        TeamRoll = TeamRoll.Leader,
                        PersonInTeamFromDate = DateTime.Today,
                        PersonInTeamToDate = DateTime.Today.AddDays(5)
                    });
                    context.PersonInTeams.Add(new PersonInTeam()
                    {
                        User = context.UsersInt.OrderBy(c => Guid.NewGuid()).FirstOrDefault(),
                        Team = context.Teams.OrderBy(c => Guid.NewGuid()).FirstOrDefault(),
                        TeamRoll = TeamRoll.Manager,
                        PersonInTeamFromDate = DateTime.Today,
                        PersonInTeamToDate = DateTime.Today.AddDays(5)
                    });
                    context.PersonInTeams.Add(new PersonInTeam()
                    {
                        User = context.UsersInt.OrderBy(c => Guid.NewGuid()).FirstOrDefault(),
                        Team = context.Teams.OrderBy(c => Guid.NewGuid()).FirstOrDefault(),
                        TeamRoll = TeamRoll.Member,
                        PersonInTeamFromDate = DateTime.Today,
                        PersonInTeamToDate = DateTime.Today.AddDays(5)
                    });
                }
                context.SaveChanges();
            }

            if (!context.Games.Any())
            {
                context.Games.Add(new Game()
                {
                    GameName = "Mario"
                });
                context.Games.Add(new Game()
                {
                    GameName = "Formel"
                });
                context.Games.Add(new Game()
                {
                    GameName = "GTA VI"
                });
                context.Games.Add(new Game()
                {
                    GameName = "Prince of something"
                });
                context.Games.Add(new Game()
                {
                    GameName = "Grand theft something"
                });
                context.Games.Add(new Game()
                {
                    GameName = "Need for something"
                });
                context.Games.Add(new Game()
                {
                    GameName = "Call of something"
                });
                context.SaveChanges();
            }

            if (!context.GameModes.Any())
            {
                context.GameModes.Add(new GameMode()
                {
                    Game = context.Games.FirstOrDefault(),
                    GameModeName = "1v1",
                    PlayersInOneTeam = 1
                });
                context.SaveChanges();
            }

            if (!context.Tournaments.Any())
            {
                context.Tournaments.Add(new Tournament()
                {
                    TournamentName = "asd of asd",
                    TournamentFromDate = DateTime.Today,
                    TournamentToDate = DateTime.Today.AddDays(5),
                    AdminUser = context.UsersInt.FirstOrDefault(),
                    TournamentSeason = TournamentSeason.Summer,
                    Game = context.Games.FirstOrDefault()
                });
                context.Tournaments.Add(new Tournament()
                {
                    TournamentName = "rally turna",
                    TournamentFromDate = DateTime.Today,
                    TournamentToDate = DateTime.Today.AddDays(5),
                    AdminUser = context.UsersInt.OrderBy(c => Guid.NewGuid()).FirstOrDefault(),
                    TournamentSeason = TournamentSeason.Summer,
                    Game = context.Games.OrderBy(c => Guid.NewGuid()).FirstOrDefault()
                });
                context.Tournaments.Add(new Tournament()
                {
                    TournamentName = "fps tournament",
                    TournamentFromDate = DateTime.Today,
                    TournamentToDate = DateTime.Today.AddDays(5),
                    AdminUser = context.UsersInt.OrderBy(c => Guid.NewGuid()).FirstOrDefault(),
                    TournamentSeason = TournamentSeason.Summer,
                    Game = context.Games.OrderBy(c => Guid.NewGuid()).FirstOrDefault()
                });
                context.Tournaments.Add(new Tournament()
                {
                    TournamentName = "spring is cool",
                    TournamentFromDate = DateTime.Today,
                    TournamentToDate = DateTime.Today.AddDays(5),
                    AdminUser = context.UsersInt.OrderBy(c => Guid.NewGuid()).FirstOrDefault(),
                    TournamentSeason = TournamentSeason.Summer,
                    Game = context.Games.OrderBy(c => Guid.NewGuid()).FirstOrDefault()
                });
                context.Tournaments.Add(new Tournament()
                {
                    TournamentName = "last man standing",
                    TournamentFromDate = DateTime.Today,
                    TournamentToDate = DateTime.Today.AddDays(5),
                    AdminUser = context.UsersInt.OrderBy(c => Guid.NewGuid()).FirstOrDefault(),
                    TournamentSeason = TournamentSeason.Summer,
                    Game = context.Games.OrderBy(c => Guid.NewGuid()).FirstOrDefault()
                });
                context.SaveChanges();
            }

            if (!context.GameMatches.Any())
            {
                context.GameMatches.Add(new GameMatch()
                {
                    Tournament = context.Tournaments.FirstOrDefault(),
                    GameMode = context.GameModes.FirstOrDefault(),
                    GameMatchStart = DateTime.Now,
                    GameMatchEnd = DateTime.Now.AddHours(2)
                });
                context.SaveChanges();
            }

            if (!context.Solos.Any())
            {
                for (int i = 0; i < 20; i++)
                {
                    context.Solos.Add(new Solo()
                    {
                        Game = context.Games.OrderBy(c => Guid.NewGuid()).FirstOrDefault(),
                        UserInt = context.UsersInt.OrderBy(c => Guid.NewGuid()).FirstOrDefault(),
                        SoloScore = random.Next(1, 200)
                    });
                }
                context.SaveChanges();
            }

            if (!context.SoloMatches.Any())
            {
                context.SoloMatches.Add(new SoloMatch()
                {
                    GameMatch = context.GameMatches.FirstOrDefault(),
                    Solo = context.Solos.FirstOrDefault(),
                    GameResult = 0,
                });
                context.SaveChanges();
            }

            if (!context.Parties.Any())
            {
                for (int i = 0; i <= 10; i++)
                {
                    context.Parties.Add(new Party()
                    {
                        Team = context.Teams.OrderBy(c => Guid.NewGuid()).FirstOrDefault(),
                        Game = context.Games.OrderBy(c => Guid.NewGuid()).FirstOrDefault(),
                        PartyName = "Party nr " + random.Next(1, 20),
                        PartyScore = random.Next(1, 200)
                    });
                }

                context.SaveChanges();
            }

            if (!context.PartyMatches.Any())
            {
                context.PartyMatches.Add(new PartyMatch()
                {
                    Party = context.Parties.FirstOrDefault(),
                    GameMatch = context.GameMatches.FirstOrDefault(),
                    Result = 0,
                });
            }

            if (!context.TeamPersonInParties.Any())
            {
                context.TeamPersonInParties.Add(new TeamPersonInParty()
                {
                    Party = context.Parties.FirstOrDefault(),
                    PersonInTeam = context.PersonInTeams.FirstOrDefault(),
                    PartyRoll = PartyRoll.None
                });
            }

            if (!context.TournamentPersonScores.Any())
            {
                for (int i = 0; i < 20; i++)
                {
                    context.TournamentPersonScores.Add(new TournamentPersonScore()
                    {
                        Tournament = context.Tournaments.OrderBy(c => Guid.NewGuid()).FirstOrDefault(),
                        Solo = context.Solos.OrderBy(c => Guid.NewGuid()).FirstOrDefault(),
                        Score = random.Next(1, 100)
                    });
                }

                context.SaveChanges();
            }

            if (!context.TournamentPartyScores.Any())
            {
                for (int i = 0; i <= 10; i++)
                {
                    context.TournamentPartyScores.Add(new TournamentPartyScore()
                    {
                        Tournament = context.Tournaments.OrderBy(c => Guid.NewGuid()).FirstOrDefault(),
                        Party = context.Parties.OrderBy(c => Guid.NewGuid()).FirstOrDefault(),
                        Score = random.Next(1, 200)
                    });
                }

                context.SaveChanges();
            }
        }
    }
}