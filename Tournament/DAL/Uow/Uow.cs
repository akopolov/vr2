﻿using System;
using Domain;
using Interfaces.IRepositories;
using Interfaces.IUow;
using System.Data.Entity;
using System.Threading.Tasks;
using Interfaces.IIdentity;
using NLog;

namespace DAL.Uow
{
    public class Uow : IUow
    {
        private readonly NLog.ILogger _logger;
        private readonly string _instanceId = Guid.NewGuid().ToString();

        private readonly IDataContext _dataContext;
        private readonly IRepositoryProvider _repositoryProvider;

        public Uow(IRepositoryProvider repositoryProvider, IDataContext dataContext, ILogger logger)
        {
            _logger = logger;
            _logger.Debug("InstanceId: " + _instanceId);

            _dataContext = dataContext;
            _repositoryProvider = repositoryProvider;
        }

        #region Standart Repos
        public IRepository<Team> Teams => GetStandardRepo<Team>();
        public IRepository<Game> Games => GetStandardRepo<Game>();
        #endregion

        #region Custom Repos
        public ITeamPersonInPartyRepository TeamPersoneInParties => GetCustomRepo<ITeamPersonInPartyRepository>();
        public IPersonInTeamRepository PersonInTeams => GetCustomRepo<IPersonInTeamRepository>();
        public ISoloMatchRepository SoloMatches => GetCustomRepo<ISoloMatchRepository>();
        public IGameMatchRepository GameMatches => GetCustomRepo<IGameMatchRepository>();
        public IPartyRepository Parties => GetCustomRepo<IPartyRepository>();
        public IGameModeRepository GameModes => GetCustomRepo<IGameModeRepository>();
        public ITournamentRepository Tournaments => GetCustomRepo<ITournamentRepository>();
        public ITournamentPersonScoreRepository TournamentPersonScores => GetCustomRepo<ITournamentPersonScoreRepository>();
        public ITournamentPartyScoreRepository TournamentPartyScores => GetCustomRepo<ITournamentPartyScoreRepository>();
        public IPartyMatchRepository PartyMatches => GetCustomRepo<IPartyMatchRepository>();
        public ISoloRepository Solos => GetCustomRepo<ISoloRepository>();

        #endregion

        #region Identity
        public IUserIntRepository UsersInt => GetCustomRepo<IUserIntRepository>();
        public IUserRoleIntRepository UserRolesInt => GetCustomRepo<IUserRoleIntRepository>();
        public IRoleIntRepository RolesInt => GetCustomRepo<IRoleIntRepository>();
        public IUserClaimIntRepository UserClaimsInt => GetCustomRepo<IUserClaimIntRepository>();
        public IUserLoginIntRepository UserLoginsInt => GetCustomRepo<IUserLoginIntRepository>();
        #endregion

        // calling standard EF repo provider
        private IRepository<TEntity> GetStandardRepo<TEntity>() where TEntity : class
        {
            return _repositoryProvider.GetStandardRepo<TEntity>();
        }

        //get custom repository
        private TRepoInterface GetCustomRepo<TRepoInterface>()
        {
            return _repositoryProvider.GetCustomRepo<TRepoInterface>();
        }

        // try to find repository
        public T GetRepository<T>() where T : class
        {
            var res = GetCustomRepo<T>() ?? GetStandardRepo<T>() as T;
            if (res == null)
            {
                throw new NotImplementedException("No repository for type, " + typeof(T).FullName);
            }
            return res;
        }

        public int SaveChanges()
        {
            return ((DbContext)_dataContext).SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return ((DbContext)_dataContext).SaveChangesAsync();
        }
        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            _logger.Debug("InstanceId: " + _instanceId + " Disposing:" + disposing);
        }

        #endregion
    }
}
