﻿using Interfaces.IRepositories;
using Interfaces.IUow;
using System;
using System.Collections.Generic;
using DAL.Repositories;
using DAL.Repositories.Identity;
using Interfaces.IIdentity;
using NLog;

namespace DAL.Uow
{
    public class EFRepositoryFactory : IRepositoryFactory, IDisposable
    {
        private readonly NLog.ILogger _logger;
        private readonly string _instanceId = Guid.NewGuid().ToString();

        public EFRepositoryFactory(ILogger logger)
        {
            _logger = logger;
            _logger.Debug("InstanceId: " + _instanceId);
        }

        // list all your custom repos here - match interfaces with real classes
        // dictionary of func, key is the type
        private static readonly Dictionary<Type, Func<IDataContext, object>> CustomRepositoryFactories = new Dictionary<Type, Func<IDataContext, object>>()
        {
            #region MyEntity
            { typeof(ISoloRepository), dbContext => new SoloRepository(dbContext)},
            { typeof(ITeamPersonInPartyRepository), dbContext => new TeamPersonInPartyRepository(dbContext)},
            { typeof(IPersonInTeamRepository), dbContext => new PersonInTeamRepository(dbContext)},
            { typeof(ISoloMatchRepository), dbContext => new SoloMatchRepository(dbContext)},
            { typeof(IGameMatchRepository), dbContext => new GameMatchRepository(dbContext)},
            { typeof(IPartyRepository), dbContext => new PartyRepository(dbContext)},
            { typeof(IGameModeRepository), dbContext => new GameModeRepository(dbContext)},
            { typeof(ITournamentRepository), dbContext => new TournamentRepository(dbContext)},
            { typeof(ITournamentPersonScoreRepository), dbContext => new TournamentPersonScoreRepository(dbContext)},
            { typeof(ITournamentPartyScoreRepository), dbContext => new TournamentPartyScoreRepository(dbContext)},
            #endregion

            #region Identity
            { typeof (IUserIntRepository), dbContext => new UserIntRepository(dbContext)},
            { typeof (IUserRoleIntRepository), dbContext => new UserRoleIntRepository(dbContext)},
            { typeof (IUserClaimIntRepository), dbContext => new UserClaimIntRepository(dbContext)},
            { typeof (IUserLoginIntRepository), dbContext => new UserLoginIntRepository(dbContext)},
            { typeof (IRoleIntRepository), dbContext => new RoleIntRepository(dbContext)},
            #endregion
        };

        // return func which takes one parameter and returns object
        public Func<IDataContext, object> GetStandardRepositoryFactory<TEntity>() where TEntity : class
        {
            // there is only one fuction to create the standard repo
            return dataContext => new EFRepository<TEntity>(dbContext: dataContext);
        }

        // return function which takes one parameter and returns object
        public Func<IDataContext, object> GetCustomRepositoryFactory<TRepoInterface>()
        {
            // try to get the func from dictionary
            Func<IDataContext, object> customRepositoryFactory;
            CustomRepositoryFactories.TryGetValue(key: typeof(TRepoInterface), value: out customRepositoryFactory);
            return customRepositoryFactory;
        }

        public void Dispose()
        {
            _logger.Debug("InstanceId: " + _instanceId);
        }
    }
}
