﻿using Interfaces.IRepositories;
using Interfaces.IUow;
using System;
using System.Collections.Generic;
using NLog;

namespace DAL.Uow
{
    public class EFRepositoryProvider : IRepositoryProvider, IDisposable
    {
        private readonly NLog.ILogger _logger;
        private readonly string _instanceId = Guid.NewGuid().ToString();
        private readonly IDataContext _dataContext;
        private readonly IRepositoryFactory _repositoryFactory;

        public EFRepositoryProvider(IDataContext dataContext, IRepositoryFactory repositoryFactory, ILogger logger)
        {
            _logger = logger;
            _logger.Debug("InstanceId: " + _instanceId);

            _dataContext = dataContext;
            _repositoryFactory = repositoryFactory;
        }

        protected Dictionary<Type, object> Repositories { get; } = new Dictionary<Type, object>();

        public IRepository<TEntity> GetStandardRepo<TEntity>() where TEntity : class
        {
            // get repo by type and provide factory for creating the repo when its not found
            return GetRepository<IRepository<TEntity>>(factory: _repositoryFactory.GetStandardRepositoryFactory<TEntity>());
        }

        public TRepoInterface GetCustomRepo<TRepoInterface>()
        {
            // get repo by type and provide factory for creating the repo when its not found
            return GetRepository<TRepoInterface>(factory: _repositoryFactory.GetCustomRepositoryFactory<TRepoInterface>());
        }

        private TRepository GetRepository<TRepository>(Func<IDataContext, object> factory)
        {
            object repositoryObject;

            // try to get repo from cache
            Repositories.TryGetValue(
                key: typeof(TRepository), value: out repositoryObject);

            // found it?
            if (repositoryObject != null)
            {
                return (TRepository)repositoryObject;
            }

            // if repo was not found in cache - create it
            return MakeRepository<TRepository>(factory: factory, dataContext: _dataContext);
        }

        private TRepository MakeRepository<TRepository>(Func<IDataContext, object> factory, IDataContext dataContext)
        {
            if (factory == null)
            {
                throw new ArgumentNullException(
                    paramName: $"No factory found for repo {typeof(TRepository).FullName}");
            }

            // create repo, use the supplied factory
            var repo = (TRepository)factory(arg: dataContext);

            //save repo to dictionary
            Repositories[key: typeof(TRepository)] = repo;
            return repo;
        }

        public void Dispose()
        {
            _logger.Debug("InstanceId: " + _instanceId);
        }
    }
}
