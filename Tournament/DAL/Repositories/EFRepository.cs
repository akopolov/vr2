﻿using Interfaces.IRepositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;

namespace DAL.Repositories
{
    public class EFRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly NLog.ILogger _logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly string _instanceId = Guid.NewGuid().ToString();

        protected DbContext RepositoryDbContext;
        protected DbSet<TEntity> RepositoryDbSet;

        public EFRepository(IDataContext dbContext)
        {

            RepositoryDbContext = dbContext as DbContext;
            if (RepositoryDbContext == null)
            {
                throw new ArgumentNullException(paramName: nameof(dbContext));
            }
            RepositoryDbSet = RepositoryDbContext.Set<TEntity>();
            if (RepositoryDbSet == null)
            {
                throw new NullReferenceException(message: nameof(RepositoryDbSet));
            }

            _logger.Info("_instanceId: " + _instanceId + " dbSet: " + RepositoryDbSet.GetType());
        }

        public List<TEntity> All => RepositoryDbSet.ToList();

        /*
        public List<TEntity> All
        {
            get
            {
                var resCount = RepositoryDbSet.Count();
                if (resCount < 10)
                {
                    return RepositoryDbSet.ToList();
                }
                throw new Exception(message: "WTF? To many records in resultset! Please add custom data access methods to repositry!");
            }
        }
        */

        public TEntity Find(params object[] id)
        {
            return RepositoryDbSet.Find(id);
        }

        public void Remove(params object[] id)
        {
            Remove(entity: Find(id: id));
        }

        // Entity Framework Add and Attach and Entity States
        // https://msdn.microsoft.com/en-us/library/jj592676(v=vs.113).aspx

        public void Remove(TEntity entity)
        {
            DbEntityEntry dbEntityEntry = RepositoryDbContext.Entry(entity: entity);
            if (dbEntityEntry.State != EntityState.Deleted)
            {
                dbEntityEntry.State = EntityState.Deleted;
            }
            else
            {
                RepositoryDbSet.Attach(entity: entity);
                RepositoryDbSet.Remove(entity: entity);
            }

        }

        public TEntity Add(TEntity entity)
        {
            DbEntityEntry dbEntityEntry = RepositoryDbContext.Entry(entity: entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
                return entity;
            }
            return RepositoryDbSet.Add(entity: entity);
        }

        public void Update(TEntity entity)
        {
            /*
            DbEntityEntry dbEntityEntry = RepositoryDbContext.Entry(entity: entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                RepositoryDbSet.Attach(entity: entity);
            }
            dbEntityEntry.State = EntityState.Modified;
            */
            RepositoryDbSet.AddOrUpdate(entities: entity);
        }

        public EntityKey GetPrimaryKeyInfo(TEntity entity)
        {
            var properties = typeof(DbSet).GetProperties();
            foreach (
                var objectContext in
                properties.Select(propertyInfo => ((IObjectContextAdapter)RepositoryDbContext).ObjectContext))
            {
                ObjectStateEntry objectStateEntry;
                if (null != entity && objectContext.ObjectStateManager
                        .TryGetObjectStateEntry(entity, out objectStateEntry))
                {
                    return objectStateEntry.EntityKey;
                }
            }
            return null;
        }

        public string[] GetKeyNames(TEntity entity)
        {
            var objectSet = ((IObjectContextAdapter)RepositoryDbContext).ObjectContext.CreateObjectSet<TEntity>();
            var keyNames = objectSet.EntitySet.ElementType.KeyMembers.Select(k => k.Name).ToArray();
            return keyNames;
        }

        public int SaveChanges()
        {
            return RepositoryDbContext.SaveChanges();
        }

        public void Dispose()
        {
            _logger.Debug("InstanceId: " + _instanceId);
        }
    }
}
