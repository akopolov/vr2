﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Domain;
using Domain.Enum;
using Interfaces.IRepositories;

namespace DAL.Repositories
{
    public class PersonInTeamRepository : EFRepository<PersonInTeam>, IPersonInTeamRepository
    {
        public PersonInTeamRepository(IDataContext dbContext) : base(dbContext)
        {

        }

        public List<PersonInTeam> FetchMembersByTeamId(int id)
        {
            return RepositoryDbSet
                .Where(p => p.TeamId == id)
                .Include(p => p.User)
                .ToList();
        }

        public List<PersonInTeam> FetchAll()
        {
            return RepositoryDbSet
                .Include(p => p.User)
                .Include(p => p.Team)
                .ToList();
        }

        public PersonInTeam FetchById(int id)
        {
            return RepositoryDbSet
                .Include(p => p.User)
                .Include(p => p.Team)
                .SingleOrDefault(p => p.PersonInTeamId == id);
        }

        public List<PersonInTeam> FetchByTeamId(int id)
        {
            return RepositoryDbSet
                .Include(p => p.User)
                .Include(p => p.Team)
                .Where(p => p.TeamId == id)
                .ToList();
        }

        public List<PersonInTeam> FetchTeamManagers(int teamId)
        {
            return RepositoryDbSet
                .Where(p => p.TeamRoll == TeamRoll.Manager && p.TeamId == teamId)
                .ToList();
        }
    }
}
