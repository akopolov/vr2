﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Domain;
using Interfaces.IRepositories;

namespace DAL.Repositories
{
    public class GameMatchRepository : EFRepository<GameMatch>,IGameMatchRepository
    {
        public GameMatchRepository(IDataContext dbContext) : base(dbContext)
        {

        }

        public List<GameMatch> FetchAll()
        {
            return RepositoryDbSet
                .Include(p => p.Tournament)
                .Include(p => p.GameMode.Game)
                .ToList();
        }

        public GameMatch FetchById(int id)
        {
            return RepositoryDbSet
                .Include(p => p.Tournament)
                .Include(p => p.GameMode.Game)
                .SingleOrDefault(p => p.GameMatchId == id);
        }
    }
}
