﻿using Domain;
using Interfaces.IRepositories;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System;

namespace DAL.Repositories
{
    public class SoloRepository : EFRepository<Solo>, ISoloRepository
    {
        public SoloRepository(IDataContext dbContext) : base(dbContext)
        {
        }

        public List<Solo> FetchAll()
        {
            return RepositoryDbSet
                .Include(p => p.UserInt)
                .Include(p => p.Game)
                .ToList();
        }

        public List<Solo> FetchByGameId(int gameId)
        {
            return RepositoryDbSet
                .Include(p => p.UserInt)
                .Include(p => p.Game)
                .Where(p => p.GameId == gameId)
                .ToList();
        }

        public Solo FetchById(int id)
        {
            return RepositoryDbSet
                .Include(p => p.UserInt)
                .Include(p => p.Game)
                .SingleOrDefault(p => p.SoloId == id);
        }

        public List<Solo> FetchByUserId(int userId)
        {
            return RepositoryDbSet
                .Include(p => p.UserInt)
                .Include(p => p.Game)
                .Where(p => p.UserIntId == userId)
                .ToList();
        }
    }
}
