﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Domain;
using Interfaces.IRepositories;

namespace DAL.Repositories
{
    public class PartyRepository : EFRepository<Party>, IPartyRepository
    {
        public PartyRepository(IDataContext dbContext) : base(dbContext)
        {

        }

        public List<Party> FetchAll()
        {
            return RepositoryDbSet
                .Include(p => p.Team)
                .Include(p => p.Game)
                .ToList();
        }

        public Party FetchById(int id)
        {
            return RepositoryDbSet
                .Include(p => p.Team)
                .Include(p => p.Game)
                .SingleOrDefault(p => p.PartyId == id);
        }
    }
}
