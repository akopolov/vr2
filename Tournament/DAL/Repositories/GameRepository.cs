﻿using Domain;
using Interfaces.IRepositories;

namespace DAL.Repositories
{
    public class GameRepository : EFRepository<Game>,IGameRepository
    {
        public GameRepository(IDataContext dbContext) : base(dbContext)
        {

        }
    }
}
