﻿using Domain;
using Interfaces.IRepositories;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace DAL.Repositories
{
    public class GameModeRepository : EFRepository<GameMode>, IGameModeRepository
    {
        public GameModeRepository(IDataContext dbContext) : base(dbContext)
        {

        }

        public List<GameMode> FetchAll()
        {
            return RepositoryDbSet
                .Include(p => p.Game)
                .ToList();
        }

        public GameMode FetchById(int id)
        {
            return RepositoryDbSet
                .Include(p => p.Game)
                .SingleOrDefault(p => p.GameId == id);
        }
    }
}
