﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Domain;
using Interfaces.IRepositories;

namespace DAL.Repositories
{
    public class TournamentPartyScoreRepository : EFRepository<TournamentPartyScore>, ITournamentPartyScoreRepository
    {
        public TournamentPartyScoreRepository(IDataContext dbContext) : base(dbContext)
        {

        }

        public List<TournamentPartyScore> FetchAll()
        {
            return RepositoryDbSet
                .Include(p => p.Tournament.Game)
                .Include(p => p.Party)
                .ToList();
        }

        public TournamentPartyScore FetchById(int id)
        {
            return RepositoryDbSet
                .Include(p => p.Tournament.Game)
                .Include(p => p.Party)
                .SingleOrDefault(p => p.TournamentPartyScoreId == id);
        }

        public List<TournamentPartyScore> GetByTournamentId(int tournamentId)
        {
            return RepositoryDbSet
                .Where(p => p.TournamentId == tournamentId)
                .Include(p => p.Tournament.Game)
                .Include(p => p.Party)
                .ToList();
        }
    }
}
