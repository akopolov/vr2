﻿using Domain.Identity;
using Interfaces.IIdentity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Interfaces.IRepositories;

namespace DAL.Repositories.Identity
{
    public class UserClaimIntRepository :
        UserClaimRepository<int, RoleInt, UserInt, UserClaimInt, UserLoginInt, UserRoleInt>, IUserClaimIntRepository
    {
        public UserClaimIntRepository(IDataContext dbContext) : base(dbContext)
        {
        }
    }

    public class UserClaimRepository : UserClaimRepository<string, Role, User, UserClaim, UserLogin, UserRole>,
        IUserClaimRepository
    {
        public UserClaimRepository(IDataContext dbContext) : base(dbContext)
        {
        }

    }

    public class UserClaimRepository<TKey, TRole, TUser, TUserClaim, TUserLogin, TUserRole> : EFRepository<TUserClaim>, IUserClaimRepository<TKey, TUserClaim>
        where TKey : IEquatable<TKey>
        where TRole : Role<TKey, TRole, TUser, TUserClaim, TUserLogin, TUserRole>
        where TUser : User<TKey, TRole, TUser, TUserClaim, TUserLogin, TUserRole>
        where TUserClaim : UserClaim<TKey, TRole, TUser, TUserClaim, TUserLogin, TUserRole>
        where TUserLogin : UserLogin<TKey, TRole, TUser, TUserClaim, TUserLogin, TUserRole>
        where TUserRole : UserRole<TKey, TRole, TUser, TUserClaim, TUserLogin, TUserRole>
    {
        public UserClaimRepository(IDataContext dbContext) : base(dbContext)
        {
        }

        public List<TUserClaim> AllIncludeUser()
        {
            return RepositoryDbSet.Include(a => a.User).ToList();
        }
        public List<TUserClaim> AllForUserId(TKey userId)
        {
            return RepositoryDbSet.Where(c => c.UserId.Equals(userId)).ToList();
        }

    }
}