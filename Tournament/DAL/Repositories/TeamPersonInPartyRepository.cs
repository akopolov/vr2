﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Domain;
using Interfaces.IRepositories;

namespace DAL.Repositories
{
    public class TeamPersonInPartyRepository : EFRepository<TeamPersonInParty>, ITeamPersonInPartyRepository
    {
        public TeamPersonInPartyRepository(IDataContext dbContext) : base(dbContext)
        {

        }

        public List<TeamPersonInParty> FetchByPersonInTeam(int personInTeamId)
        {
            return RepositoryDbSet
                .Where(p => p.PersonInTeamId == personInTeamId)
                .ToList();
        }

        public List<TeamPersonInParty> FetchAll()
        {
            return RepositoryDbSet
                .Include(p => p.Party)
                .Include(p => p.PersonInTeam.Team)
                .Include(p => p.PersonInTeam.User)
                .ToList();
        }

        public TeamPersonInParty FetchById(int id)
        {
            return RepositoryDbSet
                .Include(p => p.Party)
                .Include(p => p.PersonInTeam.Team)
                .Include(p => p.PersonInTeam.User)
                .SingleOrDefault(p => p.TeamPersonInPartyId == id);
        }
    }
}
