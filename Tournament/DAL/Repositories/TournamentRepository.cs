﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Domain;
using Interfaces.IRepositories;

namespace DAL.Repositories
{
    public class TournamentRepository : EFRepository<Tournament>, ITournamentRepository
    {
        public TournamentRepository(IDataContext dbContext) : base(dbContext)
        {

        }

        public List<Tournament> FetchAll()
        {
            return RepositoryDbSet
                .Include(p => p.Game)
                .Include(p => p.AdminUser)
                .ToList();
        }

        public Tournament FetchById(int id)
        {
            return RepositoryDbSet
                .Include(p => p.Game)
                .Include(p => p.AdminUser)
                .SingleOrDefault(p => p.TournamentId == id);
        }
    }
}
