﻿using Domain;
using Interfaces.IRepositories;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace DAL.Repositories
{
    public class PartyMatchRepository:EFRepository<PartyMatch>, IPartyMatchRepository
    {
        public PartyMatchRepository(IDataContext dbContext) : base(dbContext)
        {
        }

        public List<PartyMatch> FetchAll()
        {
            return RepositoryDbSet
                .Include(p => p.Party.Game)
                .Include(p => p.GameMatch.Tournament)
                .ToList();
        }

        public PartyMatch FetchById(int id)
        {
            return RepositoryDbSet
                .Include(p => p.Party.Game)
                .Include(p => p.GameMatch.Tournament)
                .SingleOrDefault(p => p.PartyMatchId == id);
        }
    }
}
