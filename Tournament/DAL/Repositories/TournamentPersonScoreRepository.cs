﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Domain;
using Interfaces.IRepositories;

namespace DAL.Repositories
{
    public class TournamentPersonScoreRepository : EFRepository<TournamentPersonScore>, ITournamentPersonScoreRepository
    {
        public TournamentPersonScoreRepository(IDataContext dbContext) : base(dbContext)
        {

        }

        public List<TournamentPersonScore> FetchAll()
        {
            return RepositoryDbSet
                .Include(p => p.Solo.UserInt)
                .Include(p => p.Tournament.Game)
                .ToList();
        }

        public TournamentPersonScore FetchById(int id)
        {
            return RepositoryDbSet
                .Include(p => p.Solo.UserInt)
                .Include(p => p.Tournament.Game)
                .SingleOrDefault(p => p.TournamentId == id);
        }

        public List<TournamentPersonScore> GetByTournamentId(int tournamentId)
        {
            return RepositoryDbSet
                .Where(p => p.TournamentId == tournamentId)
                .Include(p => p.Solo.UserInt)
                .Include(p => p.Tournament.Game)
                .ToList();
        }
    }
}
