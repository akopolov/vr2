﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Domain;
using Interfaces.IRepositories;

namespace DAL.Repositories
{
    public class SoloMatchRepository : EFRepository<SoloMatch>, ISoloMatchRepository
    {
        public SoloMatchRepository(IDataContext dbContext) : base(dbContext)
        {

        }

        public List<SoloMatch> FetchAll()
        {
            return RepositoryDbSet
                .Include(p => p.GameMatch.Tournament)
                .Include(p => p.Solo.UserInt)
                .Include(p => p.Solo.Game)
                .ToList();
        }

        public SoloMatch FetchById(int id)
        {
            return RepositoryDbSet
                .Include(p => p.GameMatch.Tournament)
                .Include(p => p.Solo.UserInt)
                .Include(p => p.Solo.Game)
                .SingleOrDefault(p => p.SoloMatchId == id);
        }
    }
}
