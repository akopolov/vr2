﻿using Domain;
using Interfaces.IRepositories;

namespace DAL.Repositories
{
    public class TeamRepository : EFRepository<Team>, ITeamRepository
    {
        public TeamRepository(IDataContext dbContext) : base(dbContext)
        {

        }
    }
}
