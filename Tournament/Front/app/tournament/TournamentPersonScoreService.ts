﻿import { IHttpService } from 'angular';
import { ObjectFactory } from '../main/ObjectFactory';
import { TournamentPersonScore } from '../objects/TournamentPersonScore';

export class TournamentPersonScoreService {

    private urlBase: string;

    constructor(private $http: IHttpService, private appSettings: any, private objectFactory: ObjectFactory) {
        this.urlBase = appSettings.serverPath + '/api/TournamentPersonScores';
    }

    getPersonScoresByTournamentId(id: number) {
        return this.$http.get(this.urlBase + '/GetByTournamentId/' + id)
            .then((result: any) => {
                if (result) {
                    let solos: TournamentPersonScore[] = [];
                    result.data.forEach((each: any) => {
                        let solo: TournamentPersonScore = this.objectFactory.generateObject(new TournamentPersonScore(), each);
                        solos.push(solo);
                    })
                    return solos;
                }
            })
    }
}

TournamentPersonScoreService.$inject = ['$http', 'appSettings', 'ObjectFactory'];
