﻿import { Tournament } from '../objects/Tournament';
import { TournamentPersonScore } from '../objects/TournamentPersonScore';
import { TournamentPartyScore } from '../objects/TournamentPartyScore';
import { TournamentService } from './TournamentService';
import { TournamentPersonScoreService } from './TournamentPersonScoreService';
import { TournamentPartyScoreService } from './TournamentPartyScoreService';

export class TournamentController {
    private tournament: Tournament;
    private soloScores: TournamentPersonScore[];
    private partyScores: TournamentPartyScore[];

    constructor(private tournamentService: TournamentService, private $routeParams: any, private tournamentPersonScoreService: TournamentPersonScoreService
        , private tournamentPartyScoreService: TournamentPartyScoreService) {
        this.init();
    }

    init() {
        this.tournamentService.getTournamentById(this.$routeParams.id)
            .then((result: Tournament) => {
                this.tournament = result;
            }).then(() => {
                this.tournamentPersonScoreService.getPersonScoresByTournamentId(this.tournament.tournamentId)
                    .then((result: TournamentPersonScore[]) => {
                        this.soloScores = result;
                    })
            }).then(() => {
                this.tournamentPartyScoreService.getPersonScoresByTournamentId(this.tournament.tournamentId)
                    .then((result: TournamentPartyScore[]) => {
                        this.partyScores = result;
                    })
            }).catch(() => { });
    }

}

TournamentController.$inject = ['TournamentService', '$routeParams', 'TournamentPersonScoreService', 'TournamentPartyScoreService'];