﻿import { IHttpService } from 'angular';
import { ObjectFactory } from '../main/ObjectFactory';
import { TournamentPartyScore } from '../objects/TournamentPartyScore';

export class TournamentPartyScoreService {

    private urlBase: string;

    constructor(private $http: IHttpService, private appSettings: any, private objectFactory: ObjectFactory) {
        this.urlBase = appSettings.serverPath + '/api/TournamentPartyScores';
    }

    getPersonScoresByTournamentId(id: number) {
        return this.$http.get(this.urlBase + '/GetByTournamentId/' + id)
            .then((result: any) => {
                if (result) {
                    let scores: TournamentPartyScore[] = [];
                    result.data.forEach((each: any) => {
                        let score: TournamentPartyScore = this.objectFactory.generateObject(new TournamentPartyScore(), each);
                        scores.push(score);
                    })
                    return scores;
                }
            })
    }
}

TournamentPartyScoreService.$inject = ['$http', 'appSettings', 'ObjectFactory'];
