﻿/// <reference path="../objects/tournament.ts" />
import { Tournament } from '../objects/Tournament';
import { TournamentService } from './TournamentService';
import { GameService } from '../game/GameService';
import { Game } from '../objects/Game';
import { TournamentSeason } from '../objects/enum/TournamentSeason';


export class TournamentsController {
    private tournaments: Tournament[];
    private newTournament: Tournament;
    private games: Game[];

    private seasons: string[] = [];
    private selectedSeason: string;


    constructor(private $location: any, private tournamentService: TournamentService, private gameService: GameService, private Flash: any) {
        this.getTournaments();
    }

    private getTournaments() {
        this.tournamentService.getAllTournaments()
            .then((result: Tournament[]) => {
                this.tournaments = result;
            }).catch(() => { });;
    }

    private openTournament(tournamentId: number) {
        this.$location.path('/tournaments/' + tournamentId);
    }

    private addTournament() {
        this.newTournament.tournamentSeason = TournamentSeason[this.newTournament.tournamentSeason];
        this.tournamentService.addTournament(this.newTournament)
            .then(() => {
                this.getTournaments();
                this.Flash.create('success', 'Tournament added');
                (<any>$('#addTournamentModal')).modal('hide');
            }).catch(() => { });
    }

    private initTournamentAdd() {
        this.getAllGames();
        this.getSeasonsNames();
    }

    private getAllGames() {
        this.gameService.getAllGames()
            .then(result => {
                this.games = result;
            }).catch(() => { });;
    }

    private getSeasonsNames() {
        for (var enumMember in TournamentSeason) {
            var isValueProperty = parseInt(enumMember, 10) >= 0
            if (isValueProperty) {
                this.seasons.push(TournamentSeason[enumMember]);
            }
        }
    }

    // calendar
    private calendarFrom = {
        opened: false
    };
    private calendarTo = {
        opened: false
    };
    private openCalendar(calendar: string) {
        this[calendar].opened = true;
    };
}

TournamentsController.$inject = ['$location', 'TournamentService', 'GameService', 'Flash'];