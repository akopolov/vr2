﻿import { IHttpService } from 'angular';
import { ObjectFactory } from '../main/ObjectFactory';
import { Tournament } from '../objects/Tournament';

export class TournamentService {
    private urlBase: string;

    constructor(private $http: IHttpService, private appSettings: any, private objectFactory: ObjectFactory) {
        this.urlBase = appSettings.serverPath + '/api/Tournaments';
    }

    getAllTournaments() {
        return this.$http.get(this.urlBase + '/getall')
            .then((result: any) => {
                if (result) {
                    let tournaments: Tournament[] = [];
                    result.data.forEach((each: any) => {
                        let tournament = this.objectFactory.generateObject(new Tournament(), each);
                        tournaments.push(tournament);
                    });
                    return tournaments;
                }
            })
    }

    getTournamentById(id: number) {
        return this.$http.get(this.urlBase + '/getById/' + id)
            .then((result: any) => {
                if (result) {
                    return this.objectFactory.generateObject(new Tournament(), result.data);
                }
            })
    }

    addTournament(tournament: Tournament) {
        return this.$http.post(this.urlBase + '/add', tournament);
    }
}

TournamentService.$inject = ['$http', 'appSettings', 'ObjectFactory'];