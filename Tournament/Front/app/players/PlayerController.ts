﻿import { SoloService } from '../solo/SoloService';
import { Solo } from '../objects/Solo';

export class PlayerController {
    private soloScores: Solo[];

    constructor(private soloService: SoloService, private $routeParams: any, private $location: any) {
        this.init();
    }

    private init() {
        this.soloService.getSolosByUserId(this.$routeParams.id)
            .then((result: Solo[]) => {
                this.soloScores = result;
            }).catch(() => { });
    }

    private openGame(gameId: number) {
        this.$location.path('/games/' + gameId);
    }
}

PlayerController.$inject = ['SoloService', '$routeParams', '$location'];