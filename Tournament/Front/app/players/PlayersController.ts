﻿import { UserInt } from '../objects/UserInt';
import { PlayerService } from './PlayerService';

export class PlayersController {
    private users: UserInt[];

    constructor(private playerService: PlayerService, private $location: any) {
        this.getPlayers();
    }


    private getPlayers() {
        this.playerService.getAllPlayers()
            .then((result: UserInt[]) => {
                this.users = result;
            }).catch(() => { });;
    }

    private openUser(userIntId: number) {
        this.$location.path('/players/' + userIntId);
    }
}

PlayersController.$inject = ['PlayerService', '$location'];