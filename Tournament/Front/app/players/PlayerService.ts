﻿import { IHttpService } from 'angular';
import { ObjectFactory } from '../main/ObjectFactory';
import { UserInt } from '../objects/UserInt';

export class PlayerService {
    private urlBase: string;

    constructor(private $http: IHttpService, private appSettings: any, private objectFactory: ObjectFactory) {
        this.urlBase = appSettings.serverPath + '/api/PersonInTeams';
    }

    // UserInt
    getAllPlayers(): Promise<UserInt[]> {
        return this.$http.get(this.appSettings.serverPath + '/api/UsersInt/getall')
            .then((result: any) => {
                if (result) {
                    let users: UserInt[] = [];
                    result.data.forEach((each: any) => {
                        let user = this.objectFactory.generateObject(new UserInt(), each);
                        users.push(user);
                    });
                    return users;
                }
            })
    }

    // takes PersonInTeam id, not UserInt
    removeMemberFromTeam(personInTeamId: number) {
        return this.$http.delete(this.urlBase + '/delete/' + personInTeamId);
    }

}

PlayerService.$inject = ['$http', 'appSettings', 'ObjectFactory'];