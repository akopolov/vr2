﻿import { UserProfile } from '../User/UserProfile';
import { LoginService } from '../User/LoginService';
import { User } from '../objects/User';

export class MainController {
    private user: User;
    private username: string = this.userProfile.getProfile().username;

    constructor(private Flash: any, private userProfile: UserProfile, private loginService: LoginService, private $location: any) {
    }

    private login() {
        if (!this.validateLogin()) {
            this.Flash.create('warning', 'Username and password must be filled');
            return;
        }

        this.loginService.login(this.user).then((response: any) => {
            this.userProfile.setProfile(response.data.userName, response.data.access_token, response.data.roles);
            this.user = null;
            this.username = this.userProfile.getProfile().username;
        }).catch((error: any) => {

        });
    }

    private isLoggedIn = function () {
        return this.userProfile.getProfile().isLoggedIn;
    };

    // system role
    private userIsInRole(role: string) {
        if (this.isLoggedIn()) {
            return this.userProfile.getProfile().roles.toUpperCase().indexOf(role.toUpperCase()) !== -1
        }
        return false;
    }

    private logout() {
        this.loginService.logout().then(() => {
            this.userProfile.clearProfile();
            this.$location.path('');
            this.user = null;
            this.username = null;
        }).catch((error: any) => {
            this.userProfile.clearProfile();
            this.$location.path('');
            this.user = null;
            this.username = null;
        });
    };

    private validateLogin() {
        return !(!this.user || !this.user.login || !this.user.password);
    }

    private gotoRegisterView() {
        this.$location.path('/register');
    }

}

MainController.$inject = ['Flash', 'UserProfile', 'LoginService', '$location'];