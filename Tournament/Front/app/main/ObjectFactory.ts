﻿import { BaseDataObj } from '../objects/BaseDataObj';
import { Game } from '../objects/Game';
import { Team } from '../objects/Team';
import { TeamInfo } from '../objects/TeamInfo';
import { Tournament } from '../objects/Tournament';
import { PersonInTeam } from '../objects/PersonInTeam';
import { Solo } from '../objects/Solo';
import { TournamentPersonScore } from '../objects/TournamentPersonScore';
import { TournamentPartyScore } from '../objects/TournamentPartyScore';

export class ObjectFactory {

    generateObject(dtObject: BaseDataObj, data: any) {
        let newObj: any = {};

        switch (dtObject.constructor.prototype.constructor.name) {
            case 'Game':
                newObj = new Game();
                break;
            case 'Team':
                newObj = new Team();
                break;
            case 'TeamInfo':
                newObj = new TeamInfo();
                break;
            case 'Tournament':
                newObj = new Tournament();
                break;
            case 'PersonInTeam':
                newObj = new PersonInTeam();
                break;
            case 'Solo':
                newObj = new Solo();
                break;
            case 'TournamentPersonScore':
                newObj = new TournamentPersonScore();
                break;
            case 'TournamentPartyScore':
                newObj = new TournamentPartyScore();
                break;

        }

        this.fillFromJSON(newObj, data);
        return newObj;
    }

    fillFromJSON(obj: BaseDataObj, json: any) {
        for (var propName in json) {
            if (propName.charAt(0) !== '$') {
                obj[propName] = json[propName]
            }
        }
    }

}