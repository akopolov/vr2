﻿import { UserProfile } from '../User/UserProfile';

// https://docs.angularjs.org/api/ng/service/$http
export class SessionInterceptor {

    constructor(private userProfile: UserProfile, private $location: any, private Flash: any, private $q: any) {

    }

    public request = (config: any) => {
        if (this.userProfile.getProfile().isLoggedIn) {
            config.headers['Authorization'] = 'Bearer ' + this.userProfile.getProfile().token;
        }
        return config;
    };

    public responseError = (rejection: any) => {
        if (rejection.status === 400) {
            var errors = this.getErrorMessages(rejection);
            if (errors) {
                errors.forEach((each: any) => {
                    this.Flash.create('warning', each);
                })
            }
        }
        if (rejection.status === 401) {
            this.$location.path('');
            this.Flash.create('danger', 'You are not authorized to access this location');
            return;
        }
        if (rejection.status === 404) {
            this.$location.path('');
            this.Flash.create('warning', 'Page you are looking for was not found');
            return;
        }
        if (rejection.status === 500) {
            this.Flash.create('warning', 'Something went wrong. Please contact admin');
            //return;
        }

        return this.$q.reject(rejection);
    };


    private getErrorMessages(rejection: any) {
        if (rejection.data.error_description) {  // login error, wrong psw.
            this.Flash.create('warning', rejection.data.error_description);
        } else if (rejection.data) { // modelState errors
            var errors = [];
            for (var key in rejection.data.modelState) {
                if (key.charAt(0) !== '$') {
                    for (var i = 0; i < rejection.data.modelState[key].length; i++) {
                        errors.push(rejection.data.modelState[key][i]);
                    }
                }
            }
            return errors;
        }
    }

}

SessionInterceptor.$inject = ['UserProfile', '$location', 'Flash', '$q'];