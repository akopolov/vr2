﻿import { IHttpService } from 'angular';
import { ObjectFactory } from '../main/ObjectFactory';
import { Solo } from '../objects/Solo';

export class SoloService {
    private urlBase: string;

    constructor(private $http: IHttpService, private appSettings: any, private objectFactory: ObjectFactory) {
        this.urlBase = appSettings.serverPath + '/api/solos';
    }

    getSolosByGameId(id: number) {
        return this.$http.get(this.urlBase + '/GetByGameId/' + id)
            .then((result: any) => {
                if (result) {
                    let solos: Solo[] = [];
                    result.data.forEach((each: any) => {
                        let solo: Solo = this.objectFactory.generateObject(new Solo(), each);
                        solos.push(solo);
                    })
                    return solos;
                }
            })
    }

    getSolosByUserId(id: number) {
        return this.$http.get(this.urlBase + '/GetByUserId/' + id)
            .then((result: any) => {
                if (result) {
                    let solos: Solo[] = [];
                    result.data.forEach((each: any) => {
                        let solo: Solo = this.objectFactory.generateObject(new Solo(), each);
                        solos.push(solo);
                    })
                    return solos;
                }
            })
    }

}

SoloService.$inject = ['$http', 'appSettings', 'ObjectFactory'];