﻿export class User {
    login: string;
    firstName: string;
    lastName: string;
    password: string;
    confirmPassword: string;
    birthday: Date;

    constructor() {

    }
}