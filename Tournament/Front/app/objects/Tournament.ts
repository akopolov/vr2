﻿import { BaseDataObj } from './BaseDataObj';

export class Tournament implements BaseDataObj{
    tournamentId: number;
    tournamentName: String;

    tournamentFromDate: string;
    tournamentToDate: string;

    gameId: number;
    gameName: string;
    tournamentSeason: string;

    adminUserId: number;
    adminFullName: string;

    constructor() {

    }
}