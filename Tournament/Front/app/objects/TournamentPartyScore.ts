﻿import { BaseDataObj } from './BaseDataObj';

export class TournamentPartyScore implements BaseDataObj {
    tournamentPartyScoreId: number;
    score: number;

    paryId: number;
    partyName: string;

    tournamentId: number;
    tournamentName: string;
    tournamentYear: string;

    constructor() {

    }
}