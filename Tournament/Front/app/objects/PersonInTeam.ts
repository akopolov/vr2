﻿import { BaseDataObj } from './BaseDataObj';

export class PersonInTeam implements BaseDataObj {
    personInTeamId: number;

    userId: number;
    userFullName: string;
    userName: string;

    teamId: number;
    teamName: string;

    teamRoll: number;
    teamRollString: string;
    toDate: Date;
    fromDate: Date;

    constructor() {

    }
}