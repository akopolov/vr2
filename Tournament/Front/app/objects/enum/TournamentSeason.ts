﻿export enum TournamentSeason {
    None = 0,
    Spring = 1,
    Summer = 2,
    Fall = 3,
    Winter = 4
}