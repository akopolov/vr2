﻿import { BaseDataObj } from './BaseDataObj';

export class TournamentPersonScore implements BaseDataObj {
    tournamentPersonScoreId: number;
    score: number;
    soloId: number;
    tournamentId: number;
    personFullName: string;
    tournamentName: string;
    gameName: string;
    tournamentYear: string;

    constructor() {

    }
}