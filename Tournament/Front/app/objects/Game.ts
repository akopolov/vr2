﻿import { BaseDataObj } from './BaseDataObj';

export class Game implements BaseDataObj {
    gameId: number;
    gameName: string;

    constructor(gameName?: string) {
        this.gameName = gameName;
    }

}