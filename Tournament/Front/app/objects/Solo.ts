﻿import { BaseDataObj } from './BaseDataObj';

export class Solo implements BaseDataObj {
    soloId: number;
    soloScore: number;

    userIntId: number;
    userName: string;
    userFullName: string;

    gameName: string;
    gameId: number;
}