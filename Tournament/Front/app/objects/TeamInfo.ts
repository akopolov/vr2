﻿import { Team } from './Team';
import { PersonInTeam } from './PersonInTeam';
import { BaseDataObj } from './BaseDataObj';

export class TeamInfo implements BaseDataObj{
    team: Team;
    personsInTeam: PersonInTeam[] = [];

    constructor() {

    }

}