﻿import { BaseDataObj } from './BaseDataObj';

export class Team implements BaseDataObj {
    teamId: number;
    teamName: string;
    teamRating: number;
    teamShortName: string;

    constructor() {

    }
}