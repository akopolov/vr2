﻿import { Team } from '../objects/Team';
import { TeamInfo } from '../objects/TeamInfo';
import { TeamService } from './TeamService';
import { PlayerService } from '../players/PlayerService';
import { UserProfile } from '../User/UserProfile';
import { PersonInTeam } from '../objects/PersonInTeam';
import { UserInt } from '../objects/UserInt';

export class TeamController {
    private teamInfo: TeamInfo;
    private loaded: boolean = false; // ng-if jaoks praeguse loogikaga
    private playersToAdd: UserInt[];

    constructor(private teamService: TeamService, private $routeParams: any, private playerService: PlayerService, private user: UserProfile, private Flash: any) {
        this.init();
    }

    private init() {
        this.teamService.getTeamInfoById(this.$routeParams.id)
            .then((result: TeamInfo) => {
                this.teamInfo = result;

                this.loaded = true;
            }).catch(() => { });;
    }

    private removeMember(id: number, username: string) {
        this.playerService.removeMemberFromTeam(id)
            .then(() => {
                this.init();
                this.Flash.create('success', 'Player ' + username + ' removed');
            }).catch((error) => { });
    }

    // find if logged in user is manager
    private isManager() {
        let isManager: boolean = false;
        this.teamInfo.personsInTeam.forEach((each) => {
            if (each.teamRollString === 'Manager'
                && each.userName === this.user.getProfile().username) {
                isManager = true;
            }
        })
        return isManager;
    }

    // TODO should actually ask users who are not already in this team
    private loadPlayersToAdd() {
        this.playerService.getAllPlayers()
            .then((result) => {
                this.playersToAdd = result;
            }).catch(() => { });;
    }

    private addMember(userIntId: number, username: string) {
        let newPersonInTeam = this.generateNewMember(userIntId);

        this.teamService.addPersonToTeam(newPersonInTeam)
            .then(() => {
                this.Flash.create('success', 'Player ' + username + ' added to team.');
                (<any>$('#addMemberModal')).modal('hide');
                this.init();
            }).catch(() => {
                this.Flash.create('warning', 'Couldn\'t add new member');
                (<any>$('#addMemberModal')).modal('hide');
            });
    }

    private generateNewMember(userIntId: number) {
        let p: PersonInTeam = new PersonInTeam();
        p.fromDate = new Date();
        p.toDate = new Date(); // todo something
        p.teamId = this.teamInfo.team.teamId;
        p.userId = userIntId;
        p.teamRoll = 3; // TODO role choosing?
        return p;
    }

}

TeamController.$inject = ['TeamService', '$routeParams', 'PlayerService', 'UserProfile', 'Flash'];