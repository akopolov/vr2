﻿import { IHttpService } from 'angular';
import { ObjectFactory } from '../main/ObjectFactory';
import { Team } from '../objects/Team';
import { PersonInTeam } from '../objects/PersonInTeam';
import { TeamInfo } from '../objects/TeamInfo';

export class TeamService {
    private urlBase: string;

    constructor(private $http: IHttpService, private appSettings: any, private objectFactory: ObjectFactory) {
        this.urlBase = appSettings.serverPath + '/api/teams';
    }

    getAllTeams() {
        return this.$http.get(this.urlBase + '/getall')
            .then((result: any) => {
                if (result) {
                    let teamArray: Team[] = [];
                    result.data.forEach((each: any) => {
                        let team = this.objectFactory.generateObject(new Team(), each);
                        teamArray.push(team);
                    });
                    return teamArray;
                }
            })
    }

    getTeamById(id: number) {
        return this.$http.get(this.urlBase + '/getById/' + id)
            .then((result: any) => {
                if (result) {
                    return this.objectFactory.generateObject(new Team(), result.data);
                }
            })
    }

    getTeamInfoById(id: number) {
        return this.$http.get(this.urlBase + '/getTeamInfo/' + id)
            .then((result: any) => {
                if (result) {
                    let teamInfo: TeamInfo = new TeamInfo();
                    teamInfo.team = this.objectFactory.generateObject(new Team(), result.data.team);
                    result.data.personsInTeam.forEach((each: any) => {
                        let personInTeam: PersonInTeam = this.objectFactory.generateObject(new PersonInTeam(), each);
                        teamInfo.personsInTeam.push(personInTeam);
                    })
                    return teamInfo;
                }
            })
    }

    addTeam(team: Team) {
        return this.$http.post(this.urlBase + '/add', team);
    }

    addPersonToTeam(person: PersonInTeam) {
        return this.$http.post(this.appSettings.serverPath + '/api/PersonInTeams/add', person);
    }

}

TeamService.$inject = ['$http', 'appSettings', 'ObjectFactory'];