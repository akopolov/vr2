﻿import { Team } from '../objects/Team';
import { TeamService } from './TeamService';

export class TeamsController {
    private teamList: Team[] = [];
    private newTeam: Team;

    constructor(private teamService: TeamService, private $location: any, private Flash: any) {
        this.getTeams();
    }

    getTeams() {
        this.teamService.getAllTeams()
            .then((result: Team[]) => {
                this.teamList = result;
            }).catch(() => { });;
    }

    openTeam(teamId: number) {
        this.$location.path('/teams/' + teamId);
    }

    // TODO who adds must get to be Manager
    addTeam() {
        this.teamService.addTeam(this.newTeam).then(() => {
            this.Flash.create('success', 'Team \"' + this.newTeam.teamName + '\" added.');
            this.getTeams();
            (<any>$('#addTeamModal')).modal('hide');
            this.newTeam = null;
        }).catch(() => {
            this.Flash.create('warning', 'Couldn\'t add new team');
            (<any>$('#addTeamModal')).modal('hide');
        })
    }

}

TeamsController.$inject = ['TeamService', '$location', 'Flash'];