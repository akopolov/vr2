﻿import { Game } from '../objects/Game';
import { GameService } from './GameService';
import { Solo } from '../objects/Solo';
import { SoloService } from '../solo/SoloService';

export class SingleGameController {
    private game: Game;
    private soloScores: Solo[];

    constructor(private gameService: GameService, private $routeParams: any, private Flash: any, private $location: any, private soloService: SoloService) {
        this.init();
    }

    private init() {
        this.gameService.getGameById(this.$routeParams.id)
            .then((result: Game) => {
                this.game = result;
            })
            .then(() => {
                if (this.game) {
                    this.soloService.getSolosByGameId(this.game.gameId)
                        .then((result) => {
                            this.soloScores = result;
                        });
                }
            }).catch(() => { });
    }

    private saveGame() {
        this.gameService.saveGame(this.game.gameId, this.game)
            .then(() => {
                this.Flash.create('success', 'Game saved');
                (<any>$('#editModal')).modal('hide');
            }).catch(() => {
                this.Flash.create('warning', 'Couldn\'t save game');
                (<any>$('#editModal')).modal('hide');
            })
    }

    private delete() {
        this.gameService.deleteGame(this.game.gameId)
            .then(() => {
                this.Flash.create('success', 'Game deleted');
                (<any>$('#editModal')).modal('hide');
                this.$location.path('/games')
            })
            .catch(() => {
                // referenced in GameMode
                this.Flash.create('danger', 'Could not delete game');
            });
    }

}

SingleGameController.$inject = ['GameService', '$routeParams', 'Flash', '$location', 'SoloService'];