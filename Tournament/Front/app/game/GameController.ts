﻿import { Game } from '../objects/Game';
import { GameService } from './GameService';

export class GameController {
    private gameList: Game[] = [];
    private newGame: Game;

    constructor(private gameService: GameService, private Flash: any, private $location: any) {
        this.getGames();
    }

    getGames() {
        this.gameService.getAllGames().then((result: Game[]) => {
            this.gameList = result;
        }).catch(() => {
        });
    }

    addGame() {
        this.gameService.addGame(this.newGame).then(() => {
            this.Flash.create('success', 'Game \"' + this.newGame.gameName + '\" added.');
            this.getGames();
            (<any>$('#myModal')).modal('hide');
            this.newGame = null;
        }).catch(() => {
            this.Flash.create('warning', 'Couldn\'t add game');
            (<any>$('#myModal')).modal('hide');
        })
    }

    openGame(gameId: number) {
        this.$location.path('/games/' + gameId);
    }

}

GameController.$inject = ['GameService', 'Flash', '$location'];