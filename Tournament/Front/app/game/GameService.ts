﻿import { IHttpService } from 'angular';
import { Game } from '../objects/Game';
import { ObjectFactory } from '../main/ObjectFactory';

export class GameService {
    private urlBase: string;

    constructor(private $http: IHttpService, private appSettings: any, private objectFactory: ObjectFactory) {
        this.urlBase = appSettings.serverPath + '/api/games';
    }

    getAllGames() {
        return this.$http.get(this.urlBase + '/getall')
            .then((result: any) => {
                if (result) {
                    let gameArray: Game[] = [];
                    result.data.forEach((each: any) => {
                        let game = this.objectFactory.generateObject(new Game(), each);
                        gameArray.push(game);
                    });
                    return gameArray;
                }
            });
    }

    addGame(game: Game) {
        return this.$http.post(this.urlBase + '/add', game);
    }

    getGameById(id: number) {
        return this.$http.get(this.urlBase + '/getById/' + id)
            .then((result: any) => {
                if (result) {
                    return this.objectFactory.generateObject(new Game(), result.data);
                }
            })
    }

    saveGame(id: number, game: Game) {
        return this.$http.put(this.urlBase + '/update', game, { params: { id: id } });
    }

    deleteGame(id: number) {
        return this.$http.delete(this.urlBase + '/delete/' + id);
    }

}

GameService.$inject = ['$http', 'appSettings', 'ObjectFactory'];