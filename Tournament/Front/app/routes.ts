﻿export function routeConf($routeProvider: any, $locationProvider: any) {

    $routeProvider
        .when('/games', {
            templateUrl: 'app/game/GameView.html',
            controller: 'GameController',
            controllerAs: 'vm'
        })
        .when('/games/:id', {
            templateUrl: 'app/game/SingleGameView.html',
            controller: 'SingleGameController',
            controllerAs: 'vm'
        })
        .when('/register', {
            templateUrl: 'app/User/RegisterView.html',
            controller: 'RegistrationController',
            controllerAs: 'vm'
        }).when('/teams', {
            templateUrl: 'app/team/TeamsView.html',
            controller: 'TeamsController',
            controllerAs: 'vm'
        })
        .when('/teams/:id', {
            templateUrl: 'app/team/TeamView.html',
            controller: 'TeamController',
            controllerAs: 'vm'
        })
        .when('/tournaments', {
            templateUrl: 'app/tournament/TournamentsView.html',
            controller: 'TournamentsController',
            controllerAs: 'vm'
        })
        .when('/tournaments/:id', {
            templateUrl: 'app/tournament/TournamentView.html',
            controller: 'TournamentController',
            controllerAs: 'vm'
        })
        .when('/players', {
            templateUrl: 'app/players/PlayersView.html',
            controller: 'PlayersController',
            controllerAs: 'vm'
        })
        .when('/players/:id', {
            templateUrl: 'app/players/PlayerView.html',
            controller: 'PlayerController',
            controllerAs: 'vm'
        })
        .otherwise('/');

    $locationProvider
        .hashPrefix('');
}

routeConf.$inject = ['$routeProvider', '$locationProvider'];