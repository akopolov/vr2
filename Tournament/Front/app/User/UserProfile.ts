﻿export class UserProfile {

    constructor() {
        return {
            setProfile: this.setProfile,
            getProfile: this.getProfile,
            clearProfile: this.clearProfile
        }
    }

    setProfile = function (username: string, token: string, roles: string) {
        sessionStorage.setItem('userName', username);
        sessionStorage.setItem('accessToken', token);
        sessionStorage.setItem('roles', roles);
        //sessionStorage.setItem('refreshToken', refreshToken);
    }

    getProfile = function () {
        return {
            isLoggedIn: sessionStorage.getItem('accessToken') != null,
            username: sessionStorage.getItem('userName'),
            token: sessionStorage.getItem('accessToken'),
            roles: sessionStorage.getItem('roles')
            //refreshToken: sessionStorage.getItem('refreshToken')
        }
    }

    clearProfile = function () {
        sessionStorage.removeItem('accessToken');
        sessionStorage.removeItem('userName');
        sessionStorage.removeItem('roles');
    }

}
