﻿import { IHttpService } from 'angular';

export class LoginService {
    private urlBase: string;

    constructor(private $http: IHttpService, private appSettings: any) {
        this.urlBase = appSettings.serverPath;
    }

    login = function (data: any) {
        return this.$http({
            method: 'POST',
            url: this.urlBase + '/token',
            data: 'grant_type=password&username=' + data.login + '&password=' + data.password,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        });
    }

    logout = function () {
        var accesstoken = sessionStorage.getItem('accessToken');
        var authHeaders: any = {};
        if (accesstoken) {
            authHeaders.Authorization = 'Bearer ' + accesstoken;
        }

        return this.$http({
            method: 'POST',
            url: this.urlBase + '/api/account/logout',
            headers: authHeaders
        });
    }

    register = function (data: any) {
        return this.$http({
            method: 'POST',
            url: this.urlBase + '/api/account/register',
            data: data,
            headers: {
                "Content-Type": "application/json"
            }
        });
    }

}

LoginService.$inject = ['$http', 'appSettings'];