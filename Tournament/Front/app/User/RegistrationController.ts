﻿import { LoginService } from './LoginService';
import { UserProfile } from './UserProfile';
import { User } from '../objects/User';

export class RegistrationController {
    private user: User;

    constructor(private loginService: LoginService, private userProfile: UserProfile, private Flash: any, private $location: any) {

    }

    // TODO initial validation, birthdate validation!!

    register() {
        this.loginService.register(this.user).then(() => {
            this.Flash.create('success', 'User registration successful');
            this.$location.path('');
        }).catch((error: any) => {
        });
    };


    private calendar = {
        opened: false
    };

    private openCalendar() {
        this.calendar.opened = true;
    };
}

RegistrationController.$inject = ['LoginService', 'UserProfile', 'Flash', '$location'];