﻿/// <reference path="angular.d.ts" />

import { routeConf } from './routes';
import { IHttpProvider } from 'angular';

import { SessionInterceptor } from './main/SessionInterceptor';
import { ObjectFactory } from './main/ObjectFactory';

import { LoginService } from './User/LoginService';
import { MainController } from './main/MainController';
import { RegistrationController } from './User/RegistrationController';
import { UserProfile } from './User/UserProfile';

import { GameService } from './game/GameService';
import { GameController } from './game/GameController';
import { SingleGameController } from './game/SingleGameController';

import { TeamService } from './team/TeamService';
import { TeamsController } from './team/TeamsController';
import { TeamController } from './team/TeamController';

import { TournamentService } from './tournament/TournamentService';
import { TournamentsController } from './tournament/TournamentsController';
import { TournamentController } from './tournament/TournamentController';

import { PlayerService } from './players/PlayerService';
import { PlayersController } from './players/PlayersController';
import { PlayerController } from './players/PlayerController';

import { SoloService } from './solo/SoloService';
import { TournamentPersonScoreService } from './tournament/TournamentPersonScoreService';
import { TournamentPartyScoreService } from './tournament/TournamentPartyScoreService';


angular
    .module('app', ['ngRoute', 'ngResource', 'ngFlash', 'ui.bootstrap'])

    .config(routeConf)
    .config((FlashProvider: any) => {
        FlashProvider.setTimeout(5000);
        FlashProvider.setShowClose(true);
    })
    .config(['uibDatepickerConfig', (uibDatepickerConfig: any) => {
        uibDatepickerConfig.startingDay = 1;
    }])
    .config(['uibDatepickerPopupConfig', (uibDatepickerPopupConfig: any) => {
        uibDatepickerPopupConfig.closeText = 'Close';
        uibDatepickerPopupConfig.placement = 'auto bottom';
        uibDatepickerPopupConfig.datepickerPopup = 'dd.MM.yyyy';
    }])
    .constant('appSettings', { serverPath: 'http://localhost:27326' })
    .factory('UserProfile', UserProfile)

    .service('SessionInterceptor', SessionInterceptor)
    .config(($httpProvider: IHttpProvider) => {
        $httpProvider.interceptors.push('SessionInterceptor');
    })

    .service('ObjectFactory', ObjectFactory)
    .service('LoginService', LoginService)
    .controller('MainController', MainController)
    .controller('RegistrationController', RegistrationController)

    .service('GameService', GameService)
    .controller('GameController', GameController)
    .controller('SingleGameController', SingleGameController)

    .service('TeamService', TeamService)
    .controller('TeamsController', TeamsController)
    .controller('TeamController', TeamController)

    .service('TournamentService', TournamentService)
    .controller('TournamentsController', TournamentsController)
    .controller('TournamentController', TournamentController)

    .service('PlayerService', PlayerService)
    .controller('PlayersController', PlayersController)
    .controller('PlayerController', PlayerController)

    .service('SoloService', SoloService)
    .service('TournamentPersonScoreService', TournamentPersonScoreService)
    .service('TournamentPartyScoreService', TournamentPartyScoreService)
    ;


angular.element(document)
    .ready(() => {
        angular.bootstrap(document, ['app']);
    });