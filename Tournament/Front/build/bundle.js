/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/build/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 30);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var PersonInTeam = (function () {
    function PersonInTeam() {
    }
    return PersonInTeam;
}());
exports.PersonInTeam = PersonInTeam;


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Game = (function () {
    function Game(gameName) {
        this.gameName = gameName;
    }
    return Game;
}());
exports.Game = Game;


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Solo = (function () {
    function Solo() {
    }
    return Solo;
}());
exports.Solo = Solo;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Team = (function () {
    function Team() {
    }
    return Team;
}());
exports.Team = Team;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var TeamInfo = (function () {
    function TeamInfo() {
        this.personsInTeam = [];
    }
    return TeamInfo;
}());
exports.TeamInfo = TeamInfo;


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Tournament = (function () {
    function Tournament() {
    }
    return Tournament;
}());
exports.Tournament = Tournament;


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var TournamentPartyScore = (function () {
    function TournamentPartyScore() {
    }
    return TournamentPartyScore;
}());
exports.TournamentPartyScore = TournamentPartyScore;


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var TournamentPersonScore = (function () {
    function TournamentPersonScore() {
    }
    return TournamentPersonScore;
}());
exports.TournamentPersonScore = TournamentPersonScore;


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var LoginService = (function () {
    function LoginService($http, appSettings) {
        this.$http = $http;
        this.appSettings = appSettings;
        this.login = function (data) {
            return this.$http({
                method: 'POST',
                url: this.urlBase + '/token',
                data: 'grant_type=password&username=' + data.login + '&password=' + data.password,
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            });
        };
        this.logout = function () {
            var accesstoken = sessionStorage.getItem('accessToken');
            var authHeaders = {};
            if (accesstoken) {
                authHeaders.Authorization = 'Bearer ' + accesstoken;
            }
            return this.$http({
                method: 'POST',
                url: this.urlBase + '/api/account/logout',
                headers: authHeaders
            });
        };
        this.register = function (data) {
            return this.$http({
                method: 'POST',
                url: this.urlBase + '/api/account/register',
                data: data,
                headers: {
                    "Content-Type": "application/json"
                }
            });
        };
        this.urlBase = appSettings.serverPath;
    }
    return LoginService;
}());
exports.LoginService = LoginService;
LoginService.$inject = ['$http', 'appSettings'];


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var RegistrationController = (function () {
    function RegistrationController(loginService, userProfile, Flash, $location) {
        this.loginService = loginService;
        this.userProfile = userProfile;
        this.Flash = Flash;
        this.$location = $location;
        this.calendar = {
            opened: false
        };
    }
    RegistrationController.prototype.register = function () {
        var _this = this;
        this.loginService.register(this.user).then(function () {
            _this.Flash.create('success', 'User registration successful');
            _this.$location.path('');
        }).catch(function (error) {
        });
    };
    ;
    RegistrationController.prototype.openCalendar = function () {
        this.calendar.opened = true;
    };
    ;
    return RegistrationController;
}());
exports.RegistrationController = RegistrationController;
RegistrationController.$inject = ['LoginService', 'UserProfile', 'Flash', '$location'];


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var UserProfile = (function () {
    function UserProfile() {
        this.setProfile = function (username, token, roles) {
            sessionStorage.setItem('userName', username);
            sessionStorage.setItem('accessToken', token);
            sessionStorage.setItem('roles', roles);
        };
        this.getProfile = function () {
            return {
                isLoggedIn: sessionStorage.getItem('accessToken') != null,
                username: sessionStorage.getItem('userName'),
                token: sessionStorage.getItem('accessToken'),
                roles: sessionStorage.getItem('roles')
            };
        };
        this.clearProfile = function () {
            sessionStorage.removeItem('accessToken');
            sessionStorage.removeItem('userName');
            sessionStorage.removeItem('roles');
        };
        return {
            setProfile: this.setProfile,
            getProfile: this.getProfile,
            clearProfile: this.clearProfile
        };
    }
    return UserProfile;
}());
exports.UserProfile = UserProfile;


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var GameController = (function () {
    function GameController(gameService, Flash, $location) {
        this.gameService = gameService;
        this.Flash = Flash;
        this.$location = $location;
        this.gameList = [];
        this.getGames();
    }
    GameController.prototype.getGames = function () {
        var _this = this;
        this.gameService.getAllGames().then(function (result) {
            _this.gameList = result;
        }).catch(function () {
        });
    };
    GameController.prototype.addGame = function () {
        var _this = this;
        this.gameService.addGame(this.newGame).then(function () {
            _this.Flash.create('success', 'Game \"' + _this.newGame.gameName + '\" added.');
            _this.getGames();
            $('#myModal').modal('hide');
            _this.newGame = null;
        }).catch(function () {
            _this.Flash.create('warning', 'Couldn\'t add game');
            $('#myModal').modal('hide');
        });
    };
    GameController.prototype.openGame = function (gameId) {
        this.$location.path('/games/' + gameId);
    };
    return GameController;
}());
exports.GameController = GameController;
GameController.$inject = ['GameService', 'Flash', '$location'];


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Game_1 = __webpack_require__(1);
var GameService = (function () {
    function GameService($http, appSettings, objectFactory) {
        this.$http = $http;
        this.appSettings = appSettings;
        this.objectFactory = objectFactory;
        this.urlBase = appSettings.serverPath + '/api/games';
    }
    GameService.prototype.getAllGames = function () {
        var _this = this;
        return this.$http.get(this.urlBase + '/getall')
            .then(function (result) {
            if (result) {
                var gameArray_1 = [];
                result.data.forEach(function (each) {
                    var game = _this.objectFactory.generateObject(new Game_1.Game(), each);
                    gameArray_1.push(game);
                });
                return gameArray_1;
            }
        });
    };
    GameService.prototype.addGame = function (game) {
        return this.$http.post(this.urlBase + '/add', game);
    };
    GameService.prototype.getGameById = function (id) {
        var _this = this;
        return this.$http.get(this.urlBase + '/getById/' + id)
            .then(function (result) {
            if (result) {
                return _this.objectFactory.generateObject(new Game_1.Game(), result.data);
            }
        });
    };
    GameService.prototype.saveGame = function (id, game) {
        return this.$http.put(this.urlBase + '/update', game, { params: { id: id } });
    };
    GameService.prototype.deleteGame = function (id) {
        return this.$http.delete(this.urlBase + '/delete/' + id);
    };
    return GameService;
}());
exports.GameService = GameService;
GameService.$inject = ['$http', 'appSettings', 'ObjectFactory'];


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var SingleGameController = (function () {
    function SingleGameController(gameService, $routeParams, Flash, $location, soloService) {
        this.gameService = gameService;
        this.$routeParams = $routeParams;
        this.Flash = Flash;
        this.$location = $location;
        this.soloService = soloService;
        this.init();
    }
    SingleGameController.prototype.init = function () {
        var _this = this;
        this.gameService.getGameById(this.$routeParams.id)
            .then(function (result) {
            _this.game = result;
        })
            .then(function () {
            if (_this.game) {
                _this.soloService.getSolosByGameId(_this.game.gameId)
                    .then(function (result) {
                    _this.soloScores = result;
                });
            }
        }).catch(function () { });
    };
    SingleGameController.prototype.saveGame = function () {
        var _this = this;
        this.gameService.saveGame(this.game.gameId, this.game)
            .then(function () {
            _this.Flash.create('success', 'Game saved');
            $('#editModal').modal('hide');
        }).catch(function () {
            _this.Flash.create('warning', 'Couldn\'t save game');
            $('#editModal').modal('hide');
        });
    };
    SingleGameController.prototype.delete = function () {
        var _this = this;
        this.gameService.deleteGame(this.game.gameId)
            .then(function () {
            _this.Flash.create('success', 'Game deleted');
            $('#editModal').modal('hide');
            _this.$location.path('/games');
        })
            .catch(function () {
            _this.Flash.create('danger', 'Could not delete game');
        });
    };
    return SingleGameController;
}());
exports.SingleGameController = SingleGameController;
SingleGameController.$inject = ['GameService', '$routeParams', 'Flash', '$location', 'SoloService'];


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var MainController = (function () {
    function MainController(Flash, userProfile, loginService, $location) {
        this.Flash = Flash;
        this.userProfile = userProfile;
        this.loginService = loginService;
        this.$location = $location;
        this.username = this.userProfile.getProfile().username;
        this.isLoggedIn = function () {
            return this.userProfile.getProfile().isLoggedIn;
        };
    }
    MainController.prototype.login = function () {
        var _this = this;
        if (!this.validateLogin()) {
            this.Flash.create('warning', 'Username and password must be filled');
            return;
        }
        this.loginService.login(this.user).then(function (response) {
            _this.userProfile.setProfile(response.data.userName, response.data.access_token, response.data.roles);
            _this.user = null;
            _this.username = _this.userProfile.getProfile().username;
        }).catch(function (error) {
        });
    };
    MainController.prototype.userIsInRole = function (role) {
        if (this.isLoggedIn()) {
            return this.userProfile.getProfile().roles.toUpperCase().indexOf(role.toUpperCase()) !== -1;
        }
        return false;
    };
    MainController.prototype.logout = function () {
        var _this = this;
        this.loginService.logout().then(function () {
            _this.userProfile.clearProfile();
            _this.$location.path('');
            _this.user = null;
            _this.username = null;
        }).catch(function (error) {
            _this.userProfile.clearProfile();
            _this.$location.path('');
            _this.user = null;
            _this.username = null;
        });
    };
    ;
    MainController.prototype.validateLogin = function () {
        return !(!this.user || !this.user.login || !this.user.password);
    };
    MainController.prototype.gotoRegisterView = function () {
        this.$location.path('/register');
    };
    return MainController;
}());
exports.MainController = MainController;
MainController.$inject = ['Flash', 'UserProfile', 'LoginService', '$location'];


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Game_1 = __webpack_require__(1);
var Team_1 = __webpack_require__(3);
var TeamInfo_1 = __webpack_require__(4);
var Tournament_1 = __webpack_require__(5);
var PersonInTeam_1 = __webpack_require__(0);
var Solo_1 = __webpack_require__(2);
var TournamentPersonScore_1 = __webpack_require__(7);
var TournamentPartyScore_1 = __webpack_require__(6);
var ObjectFactory = (function () {
    function ObjectFactory() {
    }
    ObjectFactory.prototype.generateObject = function (dtObject, data) {
        var newObj = {};
        switch (dtObject.constructor.prototype.constructor.name) {
            case 'Game':
                newObj = new Game_1.Game();
                break;
            case 'Team':
                newObj = new Team_1.Team();
                break;
            case 'TeamInfo':
                newObj = new TeamInfo_1.TeamInfo();
                break;
            case 'Tournament':
                newObj = new Tournament_1.Tournament();
                break;
            case 'PersonInTeam':
                newObj = new PersonInTeam_1.PersonInTeam();
                break;
            case 'Solo':
                newObj = new Solo_1.Solo();
                break;
            case 'TournamentPersonScore':
                newObj = new TournamentPersonScore_1.TournamentPersonScore();
                break;
            case 'TournamentPartyScore':
                newObj = new TournamentPartyScore_1.TournamentPartyScore();
                break;
        }
        this.fillFromJSON(newObj, data);
        return newObj;
    };
    ObjectFactory.prototype.fillFromJSON = function (obj, json) {
        for (var propName in json) {
            if (propName.charAt(0) !== '$') {
                obj[propName] = json[propName];
            }
        }
    };
    return ObjectFactory;
}());
exports.ObjectFactory = ObjectFactory;


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var SessionInterceptor = (function () {
    function SessionInterceptor(userProfile, $location, Flash, $q) {
        var _this = this;
        this.userProfile = userProfile;
        this.$location = $location;
        this.Flash = Flash;
        this.$q = $q;
        this.request = function (config) {
            if (_this.userProfile.getProfile().isLoggedIn) {
                config.headers['Authorization'] = 'Bearer ' + _this.userProfile.getProfile().token;
            }
            return config;
        };
        this.responseError = function (rejection) {
            if (rejection.status === 400) {
                var errors = _this.getErrorMessages(rejection);
                if (errors) {
                    errors.forEach(function (each) {
                        _this.Flash.create('warning', each);
                    });
                }
            }
            if (rejection.status === 401) {
                _this.$location.path('');
                _this.Flash.create('danger', 'You are not authorized to access this location');
                return;
            }
            if (rejection.status === 404) {
                _this.$location.path('');
                _this.Flash.create('warning', 'Page you are looking for was not found');
                return;
            }
            if (rejection.status === 500) {
                _this.Flash.create('warning', 'Something went wrong. Please contact admin');
            }
            return _this.$q.reject(rejection);
        };
    }
    SessionInterceptor.prototype.getErrorMessages = function (rejection) {
        if (rejection.data.error_description) {
            this.Flash.create('warning', rejection.data.error_description);
        }
        else if (rejection.data) {
            var errors = [];
            for (var key in rejection.data.modelState) {
                if (key.charAt(0) !== '$') {
                    for (var i = 0; i < rejection.data.modelState[key].length; i++) {
                        errors.push(rejection.data.modelState[key][i]);
                    }
                }
            }
            return errors;
        }
    };
    return SessionInterceptor;
}());
exports.SessionInterceptor = SessionInterceptor;
SessionInterceptor.$inject = ['UserProfile', '$location', 'Flash', '$q'];


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var PlayerController = (function () {
    function PlayerController(soloService, $routeParams, $location) {
        this.soloService = soloService;
        this.$routeParams = $routeParams;
        this.$location = $location;
        this.init();
    }
    PlayerController.prototype.init = function () {
        var _this = this;
        this.soloService.getSolosByUserId(this.$routeParams.id)
            .then(function (result) {
            _this.soloScores = result;
        }).catch(function () { });
    };
    PlayerController.prototype.openGame = function (gameId) {
        this.$location.path('/games/' + gameId);
    };
    return PlayerController;
}());
exports.PlayerController = PlayerController;
PlayerController.$inject = ['SoloService', '$routeParams', '$location'];


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var UserInt_1 = __webpack_require__(31);
var PlayerService = (function () {
    function PlayerService($http, appSettings, objectFactory) {
        this.$http = $http;
        this.appSettings = appSettings;
        this.objectFactory = objectFactory;
        this.urlBase = appSettings.serverPath + '/api/PersonInTeams';
    }
    PlayerService.prototype.getAllPlayers = function () {
        var _this = this;
        return this.$http.get(this.appSettings.serverPath + '/api/UsersInt/getall')
            .then(function (result) {
            if (result) {
                var users_1 = [];
                result.data.forEach(function (each) {
                    var user = _this.objectFactory.generateObject(new UserInt_1.UserInt(), each);
                    users_1.push(user);
                });
                return users_1;
            }
        });
    };
    PlayerService.prototype.removeMemberFromTeam = function (personInTeamId) {
        return this.$http.delete(this.urlBase + '/delete/' + personInTeamId);
    };
    return PlayerService;
}());
exports.PlayerService = PlayerService;
PlayerService.$inject = ['$http', 'appSettings', 'ObjectFactory'];


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var PlayersController = (function () {
    function PlayersController(playerService, $location) {
        this.playerService = playerService;
        this.$location = $location;
        this.getPlayers();
    }
    PlayersController.prototype.getPlayers = function () {
        var _this = this;
        this.playerService.getAllPlayers()
            .then(function (result) {
            _this.users = result;
        }).catch(function () { });
        ;
    };
    PlayersController.prototype.openUser = function (userIntId) {
        this.$location.path('/players/' + userIntId);
    };
    return PlayersController;
}());
exports.PlayersController = PlayersController;
PlayersController.$inject = ['PlayerService', '$location'];


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
function routeConf($routeProvider, $locationProvider) {
    $routeProvider
        .when('/games', {
        templateUrl: 'app/game/GameView.html',
        controller: 'GameController',
        controllerAs: 'vm'
    })
        .when('/games/:id', {
        templateUrl: 'app/game/SingleGameView.html',
        controller: 'SingleGameController',
        controllerAs: 'vm'
    })
        .when('/register', {
        templateUrl: 'app/User/RegisterView.html',
        controller: 'RegistrationController',
        controllerAs: 'vm'
    }).when('/teams', {
        templateUrl: 'app/team/TeamsView.html',
        controller: 'TeamsController',
        controllerAs: 'vm'
    })
        .when('/teams/:id', {
        templateUrl: 'app/team/TeamView.html',
        controller: 'TeamController',
        controllerAs: 'vm'
    })
        .when('/tournaments', {
        templateUrl: 'app/tournament/TournamentsView.html',
        controller: 'TournamentsController',
        controllerAs: 'vm'
    })
        .when('/tournaments/:id', {
        templateUrl: 'app/tournament/TournamentView.html',
        controller: 'TournamentController',
        controllerAs: 'vm'
    })
        .when('/players', {
        templateUrl: 'app/players/PlayersView.html',
        controller: 'PlayersController',
        controllerAs: 'vm'
    })
        .when('/players/:id', {
        templateUrl: 'app/players/PlayerView.html',
        controller: 'PlayerController',
        controllerAs: 'vm'
    })
        .otherwise('/');
    $locationProvider
        .hashPrefix('');
}
exports.routeConf = routeConf;
routeConf.$inject = ['$routeProvider', '$locationProvider'];


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Solo_1 = __webpack_require__(2);
var SoloService = (function () {
    function SoloService($http, appSettings, objectFactory) {
        this.$http = $http;
        this.appSettings = appSettings;
        this.objectFactory = objectFactory;
        this.urlBase = appSettings.serverPath + '/api/solos';
    }
    SoloService.prototype.getSolosByGameId = function (id) {
        var _this = this;
        return this.$http.get(this.urlBase + '/GetByGameId/' + id)
            .then(function (result) {
            if (result) {
                var solos_1 = [];
                result.data.forEach(function (each) {
                    var solo = _this.objectFactory.generateObject(new Solo_1.Solo(), each);
                    solos_1.push(solo);
                });
                return solos_1;
            }
        });
    };
    SoloService.prototype.getSolosByUserId = function (id) {
        var _this = this;
        return this.$http.get(this.urlBase + '/GetByUserId/' + id)
            .then(function (result) {
            if (result) {
                var solos_2 = [];
                result.data.forEach(function (each) {
                    var solo = _this.objectFactory.generateObject(new Solo_1.Solo(), each);
                    solos_2.push(solo);
                });
                return solos_2;
            }
        });
    };
    return SoloService;
}());
exports.SoloService = SoloService;
SoloService.$inject = ['$http', 'appSettings', 'ObjectFactory'];


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var PersonInTeam_1 = __webpack_require__(0);
var TeamController = (function () {
    function TeamController(teamService, $routeParams, playerService, user, Flash) {
        this.teamService = teamService;
        this.$routeParams = $routeParams;
        this.playerService = playerService;
        this.user = user;
        this.Flash = Flash;
        this.loaded = false;
        this.init();
    }
    TeamController.prototype.init = function () {
        var _this = this;
        this.teamService.getTeamInfoById(this.$routeParams.id)
            .then(function (result) {
            _this.teamInfo = result;
            _this.loaded = true;
        }).catch(function () { });
        ;
    };
    TeamController.prototype.removeMember = function (id, username) {
        var _this = this;
        this.playerService.removeMemberFromTeam(id)
            .then(function () {
            _this.init();
            _this.Flash.create('success', 'Player ' + username + ' removed');
        }).catch(function (error) { });
    };
    TeamController.prototype.isManager = function () {
        var _this = this;
        var isManager = false;
        this.teamInfo.personsInTeam.forEach(function (each) {
            if (each.teamRollString === 'Manager'
                && each.userName === _this.user.getProfile().username) {
                isManager = true;
            }
        });
        return isManager;
    };
    TeamController.prototype.loadPlayersToAdd = function () {
        var _this = this;
        this.playerService.getAllPlayers()
            .then(function (result) {
            _this.playersToAdd = result;
        }).catch(function () { });
        ;
    };
    TeamController.prototype.addMember = function (userIntId, username) {
        var _this = this;
        var newPersonInTeam = this.generateNewMember(userIntId);
        this.teamService.addPersonToTeam(newPersonInTeam)
            .then(function () {
            _this.Flash.create('success', 'Player ' + username + ' added to team.');
            $('#addMemberModal').modal('hide');
            _this.init();
        }).catch(function () {
            _this.Flash.create('warning', 'Couldn\'t add new member');
            $('#addMemberModal').modal('hide');
        });
    };
    TeamController.prototype.generateNewMember = function (userIntId) {
        var p = new PersonInTeam_1.PersonInTeam();
        p.fromDate = new Date();
        p.toDate = new Date();
        p.teamId = this.teamInfo.team.teamId;
        p.userId = userIntId;
        p.teamRoll = 3;
        return p;
    };
    return TeamController;
}());
exports.TeamController = TeamController;
TeamController.$inject = ['TeamService', '$routeParams', 'PlayerService', 'UserProfile', 'Flash'];


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Team_1 = __webpack_require__(3);
var PersonInTeam_1 = __webpack_require__(0);
var TeamInfo_1 = __webpack_require__(4);
var TeamService = (function () {
    function TeamService($http, appSettings, objectFactory) {
        this.$http = $http;
        this.appSettings = appSettings;
        this.objectFactory = objectFactory;
        this.urlBase = appSettings.serverPath + '/api/teams';
    }
    TeamService.prototype.getAllTeams = function () {
        var _this = this;
        return this.$http.get(this.urlBase + '/getall')
            .then(function (result) {
            if (result) {
                var teamArray_1 = [];
                result.data.forEach(function (each) {
                    var team = _this.objectFactory.generateObject(new Team_1.Team(), each);
                    teamArray_1.push(team);
                });
                return teamArray_1;
            }
        });
    };
    TeamService.prototype.getTeamById = function (id) {
        var _this = this;
        return this.$http.get(this.urlBase + '/getById/' + id)
            .then(function (result) {
            if (result) {
                return _this.objectFactory.generateObject(new Team_1.Team(), result.data);
            }
        });
    };
    TeamService.prototype.getTeamInfoById = function (id) {
        var _this = this;
        return this.$http.get(this.urlBase + '/getTeamInfo/' + id)
            .then(function (result) {
            if (result) {
                var teamInfo_1 = new TeamInfo_1.TeamInfo();
                teamInfo_1.team = _this.objectFactory.generateObject(new Team_1.Team(), result.data.team);
                result.data.personsInTeam.forEach(function (each) {
                    var personInTeam = _this.objectFactory.generateObject(new PersonInTeam_1.PersonInTeam(), each);
                    teamInfo_1.personsInTeam.push(personInTeam);
                });
                return teamInfo_1;
            }
        });
    };
    TeamService.prototype.addTeam = function (team) {
        return this.$http.post(this.urlBase + '/add', team);
    };
    TeamService.prototype.addPersonToTeam = function (person) {
        return this.$http.post(this.appSettings.serverPath + '/api/PersonInTeams/add', person);
    };
    return TeamService;
}());
exports.TeamService = TeamService;
TeamService.$inject = ['$http', 'appSettings', 'ObjectFactory'];


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var TeamsController = (function () {
    function TeamsController(teamService, $location, Flash) {
        this.teamService = teamService;
        this.$location = $location;
        this.Flash = Flash;
        this.teamList = [];
        this.getTeams();
    }
    TeamsController.prototype.getTeams = function () {
        var _this = this;
        this.teamService.getAllTeams()
            .then(function (result) {
            _this.teamList = result;
        }).catch(function () { });
        ;
    };
    TeamsController.prototype.openTeam = function (teamId) {
        this.$location.path('/teams/' + teamId);
    };
    TeamsController.prototype.addTeam = function () {
        var _this = this;
        this.teamService.addTeam(this.newTeam).then(function () {
            _this.Flash.create('success', 'Team \"' + _this.newTeam.teamName + '\" added.');
            _this.getTeams();
            $('#addTeamModal').modal('hide');
            _this.newTeam = null;
        }).catch(function () {
            _this.Flash.create('warning', 'Couldn\'t add new team');
            $('#addTeamModal').modal('hide');
        });
    };
    return TeamsController;
}());
exports.TeamsController = TeamsController;
TeamsController.$inject = ['TeamService', '$location', 'Flash'];


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var TournamentController = (function () {
    function TournamentController(tournamentService, $routeParams, tournamentPersonScoreService, tournamentPartyScoreService) {
        this.tournamentService = tournamentService;
        this.$routeParams = $routeParams;
        this.tournamentPersonScoreService = tournamentPersonScoreService;
        this.tournamentPartyScoreService = tournamentPartyScoreService;
        this.init();
    }
    TournamentController.prototype.init = function () {
        var _this = this;
        this.tournamentService.getTournamentById(this.$routeParams.id)
            .then(function (result) {
            _this.tournament = result;
        }).then(function () {
            _this.tournamentPersonScoreService.getPersonScoresByTournamentId(_this.tournament.tournamentId)
                .then(function (result) {
                _this.soloScores = result;
            });
        }).then(function () {
            _this.tournamentPartyScoreService.getPersonScoresByTournamentId(_this.tournament.tournamentId)
                .then(function (result) {
                _this.partyScores = result;
            });
        }).catch(function () { });
    };
    return TournamentController;
}());
exports.TournamentController = TournamentController;
TournamentController.$inject = ['TournamentService', '$routeParams', 'TournamentPersonScoreService', 'TournamentPartyScoreService'];


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var TournamentPartyScore_1 = __webpack_require__(6);
var TournamentPartyScoreService = (function () {
    function TournamentPartyScoreService($http, appSettings, objectFactory) {
        this.$http = $http;
        this.appSettings = appSettings;
        this.objectFactory = objectFactory;
        this.urlBase = appSettings.serverPath + '/api/TournamentPartyScores';
    }
    TournamentPartyScoreService.prototype.getPersonScoresByTournamentId = function (id) {
        var _this = this;
        return this.$http.get(this.urlBase + '/GetByTournamentId/' + id)
            .then(function (result) {
            if (result) {
                var scores_1 = [];
                result.data.forEach(function (each) {
                    var score = _this.objectFactory.generateObject(new TournamentPartyScore_1.TournamentPartyScore(), each);
                    scores_1.push(score);
                });
                return scores_1;
            }
        });
    };
    return TournamentPartyScoreService;
}());
exports.TournamentPartyScoreService = TournamentPartyScoreService;
TournamentPartyScoreService.$inject = ['$http', 'appSettings', 'ObjectFactory'];


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var TournamentPersonScore_1 = __webpack_require__(7);
var TournamentPersonScoreService = (function () {
    function TournamentPersonScoreService($http, appSettings, objectFactory) {
        this.$http = $http;
        this.appSettings = appSettings;
        this.objectFactory = objectFactory;
        this.urlBase = appSettings.serverPath + '/api/TournamentPersonScores';
    }
    TournamentPersonScoreService.prototype.getPersonScoresByTournamentId = function (id) {
        var _this = this;
        return this.$http.get(this.urlBase + '/GetByTournamentId/' + id)
            .then(function (result) {
            if (result) {
                var solos_1 = [];
                result.data.forEach(function (each) {
                    var solo = _this.objectFactory.generateObject(new TournamentPersonScore_1.TournamentPersonScore(), each);
                    solos_1.push(solo);
                });
                return solos_1;
            }
        });
    };
    return TournamentPersonScoreService;
}());
exports.TournamentPersonScoreService = TournamentPersonScoreService;
TournamentPersonScoreService.$inject = ['$http', 'appSettings', 'ObjectFactory'];


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Tournament_1 = __webpack_require__(5);
var TournamentService = (function () {
    function TournamentService($http, appSettings, objectFactory) {
        this.$http = $http;
        this.appSettings = appSettings;
        this.objectFactory = objectFactory;
        this.urlBase = appSettings.serverPath + '/api/Tournaments';
    }
    TournamentService.prototype.getAllTournaments = function () {
        var _this = this;
        return this.$http.get(this.urlBase + '/getall')
            .then(function (result) {
            if (result) {
                var tournaments_1 = [];
                result.data.forEach(function (each) {
                    var tournament = _this.objectFactory.generateObject(new Tournament_1.Tournament(), each);
                    tournaments_1.push(tournament);
                });
                return tournaments_1;
            }
        });
    };
    TournamentService.prototype.getTournamentById = function (id) {
        var _this = this;
        return this.$http.get(this.urlBase + '/getById/' + id)
            .then(function (result) {
            if (result) {
                return _this.objectFactory.generateObject(new Tournament_1.Tournament(), result.data);
            }
        });
    };
    TournamentService.prototype.addTournament = function (tournament) {
        return this.$http.post(this.urlBase + '/add', tournament);
    };
    return TournamentService;
}());
exports.TournamentService = TournamentService;
TournamentService.$inject = ['$http', 'appSettings', 'ObjectFactory'];


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var TournamentSeason_1 = __webpack_require__(32);
var TournamentsController = (function () {
    function TournamentsController($location, tournamentService, gameService, Flash) {
        this.$location = $location;
        this.tournamentService = tournamentService;
        this.gameService = gameService;
        this.Flash = Flash;
        this.seasons = [];
        this.calendarFrom = {
            opened: false
        };
        this.calendarTo = {
            opened: false
        };
        this.getTournaments();
    }
    TournamentsController.prototype.getTournaments = function () {
        var _this = this;
        this.tournamentService.getAllTournaments()
            .then(function (result) {
            _this.tournaments = result;
        }).catch(function () { });
        ;
    };
    TournamentsController.prototype.openTournament = function (tournamentId) {
        this.$location.path('/tournaments/' + tournamentId);
    };
    TournamentsController.prototype.addTournament = function () {
        var _this = this;
        this.newTournament.tournamentSeason = TournamentSeason_1.TournamentSeason[this.newTournament.tournamentSeason];
        this.tournamentService.addTournament(this.newTournament)
            .then(function () {
            _this.getTournaments();
            _this.Flash.create('success', 'Tournament added');
            $('#addTournamentModal').modal('hide');
        }).catch(function () { });
    };
    TournamentsController.prototype.initTournamentAdd = function () {
        this.getAllGames();
        this.getSeasonsNames();
    };
    TournamentsController.prototype.getAllGames = function () {
        var _this = this;
        this.gameService.getAllGames()
            .then(function (result) {
            _this.games = result;
        }).catch(function () { });
        ;
    };
    TournamentsController.prototype.getSeasonsNames = function () {
        for (var enumMember in TournamentSeason_1.TournamentSeason) {
            var isValueProperty = parseInt(enumMember, 10) >= 0;
            if (isValueProperty) {
                this.seasons.push(TournamentSeason_1.TournamentSeason[enumMember]);
            }
        }
    };
    TournamentsController.prototype.openCalendar = function (calendar) {
        this[calendar].opened = true;
    };
    ;
    return TournamentsController;
}());
exports.TournamentsController = TournamentsController;
TournamentsController.$inject = ['$location', 'TournamentService', 'GameService', 'Flash'];


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var routes_1 = __webpack_require__(20);
var SessionInterceptor_1 = __webpack_require__(16);
var ObjectFactory_1 = __webpack_require__(15);
var LoginService_1 = __webpack_require__(8);
var MainController_1 = __webpack_require__(14);
var RegistrationController_1 = __webpack_require__(9);
var UserProfile_1 = __webpack_require__(10);
var GameService_1 = __webpack_require__(12);
var GameController_1 = __webpack_require__(11);
var SingleGameController_1 = __webpack_require__(13);
var TeamService_1 = __webpack_require__(23);
var TeamsController_1 = __webpack_require__(24);
var TeamController_1 = __webpack_require__(22);
var TournamentService_1 = __webpack_require__(28);
var TournamentsController_1 = __webpack_require__(29);
var TournamentController_1 = __webpack_require__(25);
var PlayerService_1 = __webpack_require__(18);
var PlayersController_1 = __webpack_require__(19);
var PlayerController_1 = __webpack_require__(17);
var SoloService_1 = __webpack_require__(21);
var TournamentPersonScoreService_1 = __webpack_require__(27);
var TournamentPartyScoreService_1 = __webpack_require__(26);
angular
    .module('app', ['ngRoute', 'ngResource', 'ngFlash', 'ui.bootstrap'])
    .config(routes_1.routeConf)
    .config(function (FlashProvider) {
    FlashProvider.setTimeout(5000);
    FlashProvider.setShowClose(true);
})
    .config(['uibDatepickerConfig', function (uibDatepickerConfig) {
        uibDatepickerConfig.startingDay = 1;
    }])
    .config(['uibDatepickerPopupConfig', function (uibDatepickerPopupConfig) {
        uibDatepickerPopupConfig.closeText = 'Close';
        uibDatepickerPopupConfig.placement = 'auto bottom';
        uibDatepickerPopupConfig.datepickerPopup = 'dd.MM.yyyy';
    }])
    .constant('appSettings', { serverPath: 'http://localhost:27326' })
    .factory('UserProfile', UserProfile_1.UserProfile)
    .service('SessionInterceptor', SessionInterceptor_1.SessionInterceptor)
    .config(function ($httpProvider) {
    $httpProvider.interceptors.push('SessionInterceptor');
})
    .service('ObjectFactory', ObjectFactory_1.ObjectFactory)
    .service('LoginService', LoginService_1.LoginService)
    .controller('MainController', MainController_1.MainController)
    .controller('RegistrationController', RegistrationController_1.RegistrationController)
    .service('GameService', GameService_1.GameService)
    .controller('GameController', GameController_1.GameController)
    .controller('SingleGameController', SingleGameController_1.SingleGameController)
    .service('TeamService', TeamService_1.TeamService)
    .controller('TeamsController', TeamsController_1.TeamsController)
    .controller('TeamController', TeamController_1.TeamController)
    .service('TournamentService', TournamentService_1.TournamentService)
    .controller('TournamentsController', TournamentsController_1.TournamentsController)
    .controller('TournamentController', TournamentController_1.TournamentController)
    .service('PlayerService', PlayerService_1.PlayerService)
    .controller('PlayersController', PlayersController_1.PlayersController)
    .controller('PlayerController', PlayerController_1.PlayerController)
    .service('SoloService', SoloService_1.SoloService)
    .service('TournamentPersonScoreService', TournamentPersonScoreService_1.TournamentPersonScoreService)
    .service('TournamentPartyScoreService', TournamentPartyScoreService_1.TournamentPartyScoreService);
angular.element(document)
    .ready(function () {
    angular.bootstrap(document, ['app']);
});


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var UserInt = (function () {
    function UserInt() {
    }
    return UserInt;
}());
exports.UserInt = UserInt;


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var TournamentSeason;
(function (TournamentSeason) {
    TournamentSeason[TournamentSeason["None"] = 0] = "None";
    TournamentSeason[TournamentSeason["Spring"] = 1] = "Spring";
    TournamentSeason[TournamentSeason["Summer"] = 2] = "Summer";
    TournamentSeason[TournamentSeason["Fall"] = 3] = "Fall";
    TournamentSeason[TournamentSeason["Winter"] = 4] = "Winter";
})(TournamentSeason = exports.TournamentSeason || (exports.TournamentSeason = {}));


/***/ })
/******/ ]);