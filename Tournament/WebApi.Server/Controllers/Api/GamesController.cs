﻿using BL.DTO;
using BL.IServices;
using System.Web.Http;
using System.Web.Http.Description;
using Interfaces.IUow;

namespace WebApi.Server.Controllers.Api
{

    [Authorize(Roles = "Admin")]
    [RoutePrefix("api/Games")]
    public class GamesController : ApiController
    {
        private readonly IGamesService _gamesService;
        private readonly IUow _uow;

        public GamesController(IGamesService gamesService, IUow uow)
        {
            _gamesService = gamesService;
            _uow = uow;
        }

        // GET: api/Games
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetAll()
        {
            return Ok(_gamesService.GetAll());
        }

        // GET: api/Games/5
        [HttpGet]
        [AllowAnonymous]
        [ResponseType(typeof(GameDTO))]
        public IHttpActionResult GetById(int id)
        {
            var game = _gamesService.GetById(id);
            if (game == null) return NotFound();
            return Ok(game);
        }

        // PUT: api/Games/5
        [HttpPut]
        [ResponseType(typeof(void))]    
        public IHttpActionResult Update(int id, GameDTO game)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (id != game.GameId) return BadRequest();
            if (!_gamesService.Update(game)) return BadRequest("Incorrect primary or foreign key");
            return Ok();
        }

        // POST: api/Games
        [HttpPost]
        [ResponseType(typeof(GameDTO))]
        public IHttpActionResult Add(GameDTO game)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (!_gamesService.Add(game)) return BadRequest("Incorrect primary or foreign key");
            return CreatedAtRoute("DefaultApi", new { id = game.GameId }, game);
        }

        // DELETE: api/Games/5
        [HttpDelete]
        [ResponseType(typeof(GameDTO))]
        public IHttpActionResult Delete(int id)
        {
            if (!_gamesService.Delete(id)) return NotFound();
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _uow.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}