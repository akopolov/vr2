﻿using System;
using BL.DTO;
using BL.IServices;
using System.Web.Http;
using System.Web.Http.Description;
using Interfaces.IUow;
using Microsoft.AspNet.Identity;

namespace WebApi.Server.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/Parties")]
    public class PartiesController : ApiController
    {
        private readonly IPartyService _partyService;
        private readonly ITeamService _teamService;
        private readonly IUow _uow;

        public PartiesController(IPartyService partyService,
            ITeamService teamService, IUow uow)
        {
            _partyService = partyService;
            _teamService = teamService;
            _uow = uow;
        }

        // GET: api/Parties
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetAll()
        {
            return Ok(_partyService.GetAll());
        }

        // GET: api/Parties/5
        [HttpGet]
        [AllowAnonymous]
        [ResponseType(typeof(PartyDTO))]
        public IHttpActionResult GetById(int id)
        {
            var party = _partyService.GetById(id: id);
            if (party == null) return NotFound();
            return Ok(party);
        }

        // PUT: api/Parties/5
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult Update(int id, PartyDTO party)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (id != party.PartyId) return BadRequest();
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            var teamId = party.TeamId;
            if (!_teamService.IsManager(userId: userId, teamId: teamId)) return BadRequest("Access denied!");
            if (!_partyService.Update(party)) return BadRequest("Incorrect primary or foreign key");
            return Ok();
        }

        // POST: api/Parties
        [HttpPost]
        [ResponseType(typeof(PartyDTO))]
        public IHttpActionResult Add(PartyDTO party)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            var teamId = party.TeamId;
            if (!_teamService.IsManager(userId: userId, teamId: teamId)) return BadRequest("Access denied!");
            if (!_partyService.Add(party)) return BadRequest("Incorrect primary or foreign key");
            return CreatedAtRoute("DefaultApi", new { id = party.PartyId }, party);
        }

        // DELETE: api/Parties/5
        [ResponseType(typeof(PartyDTO))]
        public IHttpActionResult Delete(int id)
        {
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            if (!_partyService.IsManager(userId: userId, partyId: id)) return BadRequest("Access denied!");
            if (!_partyService.Delete(id: id)) return NotFound();
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _uow.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}