﻿using System;
using BL.DTO;
using BL.IServices;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity;

namespace WebApi.Server.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/TeamPersonInParties")]
    public class TeamPersonInPartiesController : ApiController
    {
        private readonly ITeamPersonInPartyService _teamPersonInPartyService;
        private readonly IPersonInTeamService _personInTeamService;

        public TeamPersonInPartiesController(ITeamPersonInPartyService teamPersonInPartyService, IPersonInTeamService personInTeamService)
        {
            _teamPersonInPartyService = teamPersonInPartyService;
            _personInTeamService = personInTeamService;
        }

        // GET: api/TeamPersonInParties
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetAll()
        {
            return Ok(_teamPersonInPartyService.GetAll());
        }

        // GET: api/TeamPersonInParties/5
        [HttpGet]
        [AllowAnonymous]
        [ResponseType(typeof(TeamPersonInPartyDTO))]
        public IHttpActionResult GetById(int id)
        {
            var teamPersonInParty = _teamPersonInPartyService.GetById(id: id);
            if (teamPersonInParty == null) return NotFound();
            return Ok(teamPersonInParty);
        }

        // PUT: api/TeamPersonInParties/5
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult Update(int id, TeamPersonInPartyDTO teamPersonInParty)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (id != teamPersonInParty.TeamPersonInPartyId) return BadRequest();
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            if (!_personInTeamService.IsManager(userId: userId, personInTeamId: teamPersonInParty.PersonInTeamId)) return BadRequest("Access denied!");
            if (!_teamPersonInPartyService.Update(teamPersonInParty)) return BadRequest();
            return Ok();
        }

        // POST: api/TeamPersonInParties
        [HttpPost]
        [ResponseType(typeof(TeamPersonInPartyDTO))]
        public IHttpActionResult Add(TeamPersonInPartyDTO teamPersonInParty)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (!_teamPersonInPartyService.Add(teamPersonInParty)) return BadRequest("Incorrect primary or foreign key");
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            if (!_personInTeamService.IsManager(userId: userId, personInTeamId: teamPersonInParty.PersonInTeamId)) return BadRequest("Access denied!");
            return CreatedAtRoute("DefaultApi", new { id = teamPersonInParty.TeamPersonInPartyId }, teamPersonInParty);
        }

        // DELETE: api/TeamPersonInParties/5
        [HttpDelete]
        [ResponseType(typeof(TeamPersonInPartyDTO))]
        public IHttpActionResult Delete(int id)
        {
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            if (!_teamPersonInPartyService.IsManager(userId: userId, teamPersonInPartyId: id)) return BadRequest("Access denied!");
            if (!_teamPersonInPartyService.Delete(id: id)) return NotFound();
            return Ok();
        }
    }
}