﻿using System;
using BL.DTO;
using BL.IServices;
using System.Web.Http;
using System.Web.Http.Description;
using Interfaces.IUow;
using Microsoft.AspNet.Identity;

namespace WebApi.Server.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/GameMatches")]
    public class GameMatchesController : ApiController
    {
        private readonly IGameMatchService _gameMatchService;
        private readonly ITournamentService _tournamentService;
        private readonly IUow _uow;

        public GameMatchesController(IGameMatchService gameMatchService, 
            ITournamentService tournamentService, IUow uow)
        {
            _gameMatchService = gameMatchService;
            _tournamentService = tournamentService;
            _uow = uow;
        }

        // GET: api/GameMatches
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetAll()
        {
            return Ok(_gameMatchService.GetAll());
        }

        // GET: api/GameMatches/5
        [HttpGet]
        [AllowAnonymous]
        [ResponseType(typeof(GameMatchDTO))]
        public IHttpActionResult GetById(int id)
        {
            var gameMatch = _gameMatchService.GetById(id: id);
            if (gameMatch == null) return NotFound();
            return Ok(gameMatch);
        }

        // PUT: api/GameMatches/5
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult Update(int id, GameMatchDTO gameMatchDto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (id != gameMatchDto.GameMatchId) return BadRequest();
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            var tournametId = gameMatchDto.TournamentId;
            if (!_tournamentService.IsAdmin(userId: userId, tournamentId: tournametId)) return BadRequest("Access denied!");
            if (!_gameMatchService.Update(gameMatchDto)) return BadRequest("Incorrect primary or foreign key");
            return Ok();
        }

        // POST: api/GameMatches
        [HttpPost]
        [ResponseType(typeof(GameMatchDTO))]
        public IHttpActionResult Add(GameMatchDTO gameMatchDto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            var tournametId = gameMatchDto.TournamentId;
            if (!_tournamentService.IsAdmin(userId: userId, tournamentId: tournametId)) return BadRequest("Access denied!");
            if (!_gameMatchService.Add(gameMatchDto)) return BadRequest("Incorrect primary or foreign key");
            return CreatedAtRoute("DefaultApi", new { id = gameMatchDto.GameMatchId }, gameMatchDto);
        }

        // DELETE: api/GameMatches/5
        [HttpDelete]
        [ResponseType(typeof(GameMatchDTO))]
        public IHttpActionResult Delete(int id)
        {
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            if (!_gameMatchService.IsAdmin(userId: userId, elementId: id)) return BadRequest("Access denied!");
            if (!_gameMatchService.Delete(id: id)) return NotFound();
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _uow.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}