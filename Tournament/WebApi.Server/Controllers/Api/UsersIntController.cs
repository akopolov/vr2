﻿using BL.DTO;
using BL.ObjectFactories;
using Domain.Identity;
using Interfaces.IUow;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace WebApi.Server.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/UsersInt")]
    public class UsersIntController : ApiController
    {
        private readonly IUow _uow;

        public UsersIntController(IUow uow)
        {
            _uow = uow;

        }

        [HttpGet]
        public UserInt GetByUserName(string userName)
        {
            return _uow.UsersInt.GetUserByUserName(userName);
        }

        [HttpGet]
        public UserInt GetByEmail(string email)
        {
            return _uow.UsersInt.GetUserByEmail(email);
        }

        [AllowAnonymous]
        [HttpGet]
        public List<UserIntDTO> GetAll()
        {
            // TODO 
            UserIntFactory userIntFactory = new UserIntFactory();
            return _uow.UsersInt.All.Select(p => userIntFactory.Create(p)).ToList();
        }

        [HttpGet]
        [ResponseType(typeof(UserInt))]
        public IHttpActionResult GetbyId(int id)
        {
            var userInt = _uow.UsersInt.Find(id);
            if (userInt == null)
            {
                return NotFound();
            }

            return Ok(userInt);
        }

        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult Update(int id, UserInt userInt)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userInt.Id)
            {
                return BadRequest();
            }

            _uow.UsersInt.Update(userInt);

            try
            {
                _uow.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (_uow.UsersInt.Find(id) != null)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpPost]
        [ResponseType(typeof(UserInt))]
        public IHttpActionResult Add(UserInt userInt)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _uow.UsersInt.Add(userInt);
            _uow.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = userInt.Id }, userInt);
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var userInt = _uow.UsersInt.Find(id);
            if (userInt == null)
            {
                return NotFound();
            }

            _uow.UsersInt.Remove(userInt);
            _uow.SaveChanges();

            return Ok(userInt);
        }

        protected override void Dispose(bool disposing)
        {
        }
    }
}