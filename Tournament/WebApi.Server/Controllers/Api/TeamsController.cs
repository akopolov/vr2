﻿using System;
using BL.DTO;
using BL.IServices;
using System.Web.Http;
using System.Web.Http.Description;
using Interfaces.IUow;
using Microsoft.AspNet.Identity;

namespace WebApi.Server.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/Teams")]
    public class TeamsController : ApiController
    {
        private readonly ITeamService _teamService;
        private readonly IUow _uow;
        public TeamsController(ITeamService teamService, IUow uow)
        {
            _teamService = teamService;
            _uow = uow;
        }

        // GET: api/Teams
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetAll()
        {
            return Ok(_teamService.GetAll());
        }

        // GET: api/Teams/5
        [HttpGet]
        [AllowAnonymous]
        [ResponseType(typeof(TeamDTO))]
        public IHttpActionResult GetById(int id)
        {
            var team = _teamService.GetById(id);
            if (team == null) return NotFound();
            return Ok(team);
        }

        // PUT: api/Teams/5
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult Update(int id, TeamDTO teamDto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (id != teamDto.TeamId) return BadRequest();
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            if (!_teamService.IsManager(userId:userId, teamId:id)) return BadRequest("Access denied!");
            if (!_teamService.Add(teamDto)) return BadRequest();
            return Ok();
        }

        // POST: api/Teams
        [HttpPost]
        [ResponseType(typeof(TeamDTO))]
        public IHttpActionResult Add(TeamDTO teamDto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            if (!_teamService.Add(teamDto: teamDto, userId:userId)) return BadRequest();
            return CreatedAtRoute("DefaultApi", new { id = teamDto.TeamId }, teamDto);
        }

        // DELETE: api/Teams/5
        [HttpDelete]
        [ResponseType(typeof(TeamDTO))]
        public IHttpActionResult Delete(int id)
        {
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId()); ;
            if (!_teamService.IsManager(userId: userId, teamId: id)) return BadRequest("Access denied!");
            if (_teamService.Delete(id)) return NotFound();
            return Ok();
        }

        // GET: api/Teams/5
        [AllowAnonymous]
        [HttpGet]
        [ResponseType(typeof(TeamInfoDTO))]
        public IHttpActionResult GetTeamInfo(int id)
        {
            var teamInfo = _teamService.GetTeamInfo(id);
            if (teamInfo == null) return NotFound();
            return Ok(teamInfo);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _uow.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}