﻿using System;
using BL.DTO;
using BL.IServices;
using System.Web.Http;
using System.Web.Http.Description;
using Interfaces.IUow;
using Microsoft.AspNet.Identity;

namespace WebApi.Server.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/TournamentPersonScores")]
    public class TournamentPersonScoresController : ApiController
    {
        private readonly ITournamentPersonScoreService _tournamentPersonScoreService;
        private readonly ITournamentService _tournamentService;
        private readonly IUow _uow;

        public TournamentPersonScoresController(ITournamentPersonScoreService tournamentPersonScoreService, 
            ITournamentService tournamentService, IUow uow)
        {
            _tournamentPersonScoreService = tournamentPersonScoreService;
            _tournamentService = tournamentService;
            _uow = uow;
        }

        // GET: api/TournamentPersonScores
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetAll()
        {
            return Ok(_tournamentPersonScoreService.GetAll());
        }

        // GET: api/TournamentPersonScores/5
        [HttpGet]
        [AllowAnonymous]
        [ResponseType(typeof(TournamentPersonScoreDTO))]
        public IHttpActionResult GetById(int id)
        {
            var result = _tournamentPersonScoreService.GetById(id);
            if (result == null) return NotFound();
            return Ok(result);
        }


        [HttpGet]
        [AllowAnonymous]
        [ResponseType(typeof(TournamentPersonScoreDTO))]
        public IHttpActionResult GetByTournamentId(int id)
        {
            var result = _tournamentPersonScoreService.GetByTournamentId(id);
            if (result == null) return NotFound();
            return Ok(result);
        }

        // PUT: api/TournamentPersonScores/update/5
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult Update(int id, TournamentPersonScoreDTO tournamentPersonScoreDto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (id != tournamentPersonScoreDto.TournamentPersonScoreId) return BadRequest();
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            var tournametId = tournamentPersonScoreDto.TournamentId;
            if (!_tournamentService.IsAdmin(userId: userId, tournamentId: tournametId)) return BadRequest("Access denied!");
            if (!_tournamentPersonScoreService.Update(tournamentPersonScoreDto)) return BadRequest("Incorrect foreign key");
            return Ok();
        }

        // POST: api/TournamentPersonScores/Add
        [HttpPost]
        [ResponseType(typeof(TournamentPersonScoreDTO))]
        public IHttpActionResult Add(TournamentPersonScoreDTO tournamentPersonScoreDto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            var tournametId = tournamentPersonScoreDto.TournamentId;
            if (!_tournamentService.IsAdmin(userId: userId, tournamentId: tournametId)) return BadRequest("Access denied!");
            if (!_tournamentPersonScoreService.Add(tournamentPersonScoreDto)) return BadRequest("Incorrect primary or foreign key");
            return CreatedAtRoute("DefaultApi", new { id = tournamentPersonScoreDto.TournamentPersonScoreId }, tournamentPersonScoreDto);
        }

        // DELETE: api/TournamentPersonScores/5
        [HttpDelete]
        [ResponseType(typeof(TournamentPersonScoreDTO))]
        public IHttpActionResult Delete(int id)
        {
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            if (!_tournamentPersonScoreService.IsAdmin(userId: userId, elementId: id)) return BadRequest("Access denied!");
            if (_tournamentPersonScoreService.Delete(id: id)) return NotFound();
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _uow.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}