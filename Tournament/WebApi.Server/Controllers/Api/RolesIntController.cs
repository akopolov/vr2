﻿using DAL.Data;
using Domain.Identity;
using Interfaces.IUow;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace WebApi.Server.Controllers.Api
{
    [Authorize(Roles = "Admin")]
    [RoutePrefix("api/RolesInt")]
    public class RolesIntController : ApiController
    {
        private readonly AppDbContext _db;
        private readonly IUow _uow;

        public RolesIntController(IUow uow, AppDbContext db)
        {
            _uow = uow;
            _db = db;
        }

        [HttpGet]
        [AllowAnonymous]
        public List<RoleInt> GetByUserId(int userId)
        {
            return _uow.RolesInt.GetRolesForUser(userId);
        }

        [HttpGet]
        public List<RoleInt> GetAll()
        {
            return _uow.RolesInt.All;
        }

        [HttpGet]
        [ResponseType(typeof(RoleInt))]
        public IHttpActionResult GetById(int id)
        {
            var roleInt = _uow.RolesInt.Find(id);
            if (roleInt == null)
            {
                return NotFound();
            }

            return Ok(roleInt);
        }

        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult Update(int id, RoleInt roleInt)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (id != roleInt.Id) return BadRequest();
            _db.Entry(roleInt).State = EntityState.Modified;
            try
            {
                _uow.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RoleIntExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpPost]
        [ResponseType(typeof(RoleInt))]
        public IHttpActionResult Add(RoleInt roleInt)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            _uow.RolesInt.Add(roleInt);
            _uow.SaveChanges();
            return CreatedAtRoute("DefaultApi", new { id = roleInt.Id }, roleInt);
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var roleInt = _uow.RolesInt.Find(id);
            if (roleInt == null) return NotFound();
            _uow.RolesInt.Remove(roleInt);
            _uow.SaveChanges();
            return Ok(roleInt);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _uow.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RoleIntExists(int id)
        {
            return _db.RolesInt.Count(e => e.Id == id) > 0;
        }
    }
}