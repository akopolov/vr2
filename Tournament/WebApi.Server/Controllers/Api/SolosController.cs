﻿using System;
using BL.DTO;
using BL.IServices;
using System.Web.Http;
using System.Web.Http.Description;
using Interfaces.IUow;
using Microsoft.AspNet.Identity;

namespace WebApi.Server.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/Solos")]
    public class SolosController : ApiController
    {
        private readonly ISoloService _soloService;
        private readonly IUow _uow;

        public SolosController(ISoloService soloService, IUow uow)
        {
            _soloService = soloService;
            _uow = uow;
        }

        // GET: api/Solos
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetAll()
        {
            return Ok(_soloService.GetAll());
        }

        // GET: api/Solos/5
        [HttpGet]
        [AllowAnonymous]
        [ResponseType(typeof(SoloDTO))]
        public IHttpActionResult GetById(int id)
        {
            var solo = _soloService.GetById(id: id);
            if (solo == null) return NotFound();
            return Ok(solo);
        }

        [HttpGet]
        [AllowAnonymous]
        [ResponseType(typeof(SoloDTO))]
        public IHttpActionResult GetByGameId(int id)
        {
            var solo = _soloService.GetByGameId(gameId: id);
            if (solo == null) return NotFound();
            return Ok(solo);
        }

        [HttpGet]
        [AllowAnonymous]
        [ResponseType(typeof(SoloDTO))]
        public IHttpActionResult GetByUserId(int id)
        {
            var solo = _soloService.GetByUserId(userId: id);
            if (solo == null) return NotFound();
            return Ok(solo);
        }

        // PUT: api/Solos/5
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult Update(int id, SoloDTO soloDto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (id != soloDto.SoloId) return BadRequest();
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            if (!_soloService.IsUser(userId: userId, elementId: id)) return BadRequest("Access denied!");
            if (!_soloService.Update(soloDto)) return BadRequest("Incorrect primary or foreign key");
            return Ok();
        }

        // POST: api/Solos
        [HttpPost]
        [ResponseType(typeof(SoloDTO))]
        public IHttpActionResult Add(SoloDTO soloDto)
        {
            soloDto.UserIntId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (!_soloService.Add(soloDto)) return BadRequest("Incorrect primary or foreign key");
            return CreatedAtRoute("DefaultApi", new { id = soloDto.SoloId }, soloDto);
        }

        // DELETE: api/Solos/5
        [HttpDelete]
        [ResponseType(typeof(SoloDTO))]
        public IHttpActionResult Delete(int id)
        {
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            if (!_soloService.IsUser(userId: userId, elementId: id)) return BadRequest("Access denied!");
            if (!_soloService.Delete(id: id)) return NotFound();
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _uow.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}