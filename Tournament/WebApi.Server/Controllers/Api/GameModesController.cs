﻿using BL.DTO;
using BL.IServices;
using System.Web.Http;
using System.Web.Http.Description;
using Interfaces.IUow;

namespace WebApi.Server.Controllers.Api
{
    [Authorize(Roles = "Admin")]
    [RoutePrefix("api/GameModes")]
    public class GameModesController : ApiController
    {
        private readonly IGameModeService _gameModeService;
        private readonly IUow _uow;

        public GameModesController(IGameModeService gameModeService, IUow uow)
        {
            _gameModeService = gameModeService;
            _uow = uow;
        }

        // GET: api/GameModes
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetAll()
        {
            return Ok(_gameModeService.GetAll());
        }

        // GET: api/GameModes/5
        [HttpGet]
        [AllowAnonymous]
        [ResponseType(typeof(GameModeDTO))]
        public IHttpActionResult GetById(int id)
        {
            var gameMode = _gameModeService.GetById(id: id);
            if (gameMode == null) return NotFound();
            return Ok(gameMode);
        }

        // PUT: api/GameModes/5
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult Update(int id, GameModeDTO gameMode)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (id != gameMode.GameModeId) return BadRequest();
            if (!_gameModeService.Update(gameMode)) return BadRequest("Incorrect primary or foreign key");
            return Ok();
        }

        // POST: api/GameModes
        [HttpPost]
        [ResponseType(typeof(GameModeDTO))]
        public IHttpActionResult Add(GameModeDTO gameMode)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (!_gameModeService.Add(gameMode)) return BadRequest("Incorrect foreign key");
            return CreatedAtRoute("DefaultApi", new { id = gameMode.GameModeId }, gameMode);
        }

        // DELETE: api/GameModes/5
        [HttpDelete]
        [ResponseType(typeof(GameModeDTO))]
        public IHttpActionResult Delete(int id)
        {
            if (!_gameModeService.Delete(id)) return NotFound();
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _uow.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}