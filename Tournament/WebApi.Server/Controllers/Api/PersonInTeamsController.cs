﻿using System;
using BL.DTO;
using BL.IServices;
using System.Web.Http;
using System.Web.Http.Description;
using Interfaces.IUow;
using Microsoft.AspNet.Identity;

namespace WebApi.Server.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/PersonInTeams")]
    public class PersonInTeamsController : ApiController
    {
        private readonly IPersonInTeamService _personInTeamService;
        private readonly ITeamService _teamService;
        private readonly IUow _uow;

        public PersonInTeamsController(IPersonInTeamService personInTeamService, ITeamService teamService, IUow uow)
        {
            _personInTeamService = personInTeamService;
            _teamService = teamService;
            _uow = uow;
        }

        // GET: api/PersonInTeams
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetAll()
        {
            return Ok(_personInTeamService.GetAll());
        }

        // GET: api/PersonInTeams/5
        [HttpGet]
        [AllowAnonymous]
        [ResponseType(typeof(PersonInTeamDTO))]
        public IHttpActionResult GetById(int id)
        {
            var personInTeam = _personInTeamService.GetById(id: id);
            if (personInTeam == null) return NotFound();
            return Ok(personInTeam);
        }

        // PUT: api/PersonInTeams/5
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult Update(int id, PersonInTeamDTO personInTeamDto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (id != personInTeamDto.PersonInTeamId) return BadRequest();
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            var teamId = personInTeamDto.TeamId;
            if (!_teamService.IsManager(userId: userId, teamId: teamId)) return BadRequest("Access denied!");
            if (!_personInTeamService.CanUpdate(personInTeamDto: personInTeamDto)) return BadRequest("There must be at least 1 Manager in a Team");
            if (!_personInTeamService.Update(personInTeamDto)) return BadRequest("Incorrect primary or foreign key");
            return Ok();
        }

        // POST: api/PersonInTeams
        [HttpPost]
        [ResponseType(typeof(PersonInTeamDTO))]
        public IHttpActionResult Add(PersonInTeamDTO personInTeamDto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            if (!_teamService.IsManager(userId: userId, teamId: personInTeamDto.TeamId)) return BadRequest("Access denied!");
            if (!_personInTeamService.Add(personInTeamDto)) return BadRequest("Incorrect primary or foreign key");
            return CreatedAtRoute("DefaultApi", new { id = personInTeamDto.PersonInTeamId }, personInTeamDto);
        }

        // DELETE: api/PersonInTeams/5
        [HttpDelete]
        [ResponseType(typeof(PersonInTeamDTO))]
        public IHttpActionResult Delete(int id)
        {
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            if (!_personInTeamService.IsManager(userId: userId, personInTeamId: id)) return BadRequest("Access denied!");
            if (!_personInTeamService.CanDelete(personInTeamId: id)) return BadRequest("There must be at least 1 Manager in a Team");
            if (!_personInTeamService.Delete(id)) return NotFound();
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _uow.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}