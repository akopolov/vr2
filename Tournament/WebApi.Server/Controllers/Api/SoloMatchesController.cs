﻿using BL.DTO;
using BL.IServices;
using Domain;
using Microsoft.AspNet.Identity;
using System;
using System.Web.Http;
using System.Web.Http.Description;
using Interfaces.IUow;

namespace WebApi.Server.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/SoloMatches")]
    public class SoloMatchesController : ApiController
    {
        private readonly ISoloMatchService _soloMatchService;
        private readonly IGameMatchService _gameMatchService;
        private readonly IUow _uow;

        public SoloMatchesController(ISoloMatchService soloMatchService, 
            IGameMatchService gameMatchService, IUow uow)
        {
            _soloMatchService = soloMatchService;
            _gameMatchService = gameMatchService;
            _uow = uow;
        }

        // GET: api/SoloMatches
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetAll()
        {
            return Ok(_soloMatchService.GetAll());
        }

        // GET: api/SoloMatches/5
        [HttpGet]
        [AllowAnonymous]
        [ResponseType(typeof(SoloMatchDTO))]
        public IHttpActionResult GetById(int id)
        {
            var soloMatch = _soloMatchService.GetById(id: id);
            if (soloMatch == null) return NotFound();
            return Ok(soloMatch);
        }

        // PUT: api/SoloMatches/5
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult Update(int id, SoloMatchDTO soloMatchDto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (id != soloMatchDto.SoloMatchId) return BadRequest();
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            if (!_soloMatchService.IsAdmin(userId: userId, elementId:id)) return BadRequest("Access denied!");
            if (!_soloMatchService.Update(soloMatchDto)) return BadRequest("Incorrect primary or foreign key");
            return Ok();
        }

        // POST: api/SoloMatches
        [HttpPost]
        [ResponseType(typeof(SoloMatch))]
        public IHttpActionResult Add(SoloMatchDTO soloMatchDto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            var gameMatchId = soloMatchDto.GameMatchId;
            if (!_gameMatchService.IsAdmin(userId: userId, elementId: gameMatchId)) return BadRequest("Access denied!");
            if (!_soloMatchService.Add(soloMatchDto)) return BadRequest("Incorrect primary or foreign key");
            return CreatedAtRoute("DefaultApi", new { id = soloMatchDto.SoloMatchId }, soloMatchDto);
        }

        // DELETE: api/SoloMatches/5
        [HttpDelete]
        [ResponseType(typeof(SoloMatch))]
        public IHttpActionResult Delete(int id)
        {
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            if (!_soloMatchService.IsAdmin(userId: userId, elementId: id)) return BadRequest("Access denied!");
            if (!_soloMatchService.Delete(id: id)) return NotFound();
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _uow.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}