﻿using System;
using BL.DTO;
using BL.IServices;
using System.Web.Http;
using System.Web.Http.Description;
using Interfaces.IUow;
using Microsoft.AspNet.Identity;

namespace WebApi.Server.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/TournamentPartyScores")]
    public class TournamentPartyScoresController : ApiController
    {
        private readonly ITournamentPartyScoreService _tournamentPartyScoreService;
        private readonly ITournamentService _tournamentService;
        private readonly IUow _uow;

        public TournamentPartyScoresController(ITournamentPartyScoreService tournamentPartyScoreService, 
            ITournamentService tournamentService, IUow uow)
        {
            _tournamentPartyScoreService = tournamentPartyScoreService;
            _tournamentService = tournamentService;
            _uow = uow;
        }

        // GET: api/TournamentPartyScores
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetAll()
        {
            return Ok(_tournamentPartyScoreService.GetAll());
        }

        // GET: api/TournamentPartyScores/5
        [HttpGet]
        [AllowAnonymous]
        [ResponseType(typeof(TournamentPartyScoreDTO))]
        public IHttpActionResult GetById(int id)
        {
            var tournamentPartyScore = _tournamentPartyScoreService.GetById(id: id);
            if (tournamentPartyScore == null) return NotFound();
            return Ok(tournamentPartyScore);
        }

        [HttpGet]
        [AllowAnonymous]
        [ResponseType(typeof(TournamentPartyScoreDTO))]
        public IHttpActionResult GetByTournamentId(int id)
        {
            var tournamentPartyScore = _tournamentPartyScoreService.GetByTournamentId(tournamentId: id);
            if (tournamentPartyScore == null) return NotFound();
            return Ok(tournamentPartyScore);
        }

        // PUT: api/TournamentPartyScores/5
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult Update(int id, TournamentPartyScoreDTO tournamentPartyScoreDto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            var tournametId = tournamentPartyScoreDto.TournamentId;
            if (!_tournamentService.IsAdmin(userId: userId, tournamentId: tournametId)) return BadRequest("Access denied!");
            if (id != tournamentPartyScoreDto.TournamentPartyScoreId) return BadRequest();
            if (!_tournamentPartyScoreService.Update(tournamentPartyScoreDto)) return BadRequest("Incorrect foreign or prime key");
            return Ok();
        }

        // POST: api/TournamentPartyScores
        [HttpPost]
        [ResponseType(typeof(TournamentPartyScoreDTO))]
        public IHttpActionResult Add(TournamentPartyScoreDTO tournamentPartyScoreDto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            var tournametId = tournamentPartyScoreDto.TournamentId;
            if (!_tournamentService.IsAdmin(userId: userId, tournamentId: tournametId)) return BadRequest("Access denied!");
            if (!_tournamentPartyScoreService.Add(tournamentPartyScoreDto)) return BadRequest("Incorrect foreign key");
            return CreatedAtRoute("DefaultApi", new { id = tournamentPartyScoreDto.TournamentPartyScoreId }, tournamentPartyScoreDto);
        }

        // DELETE: api/TournamentPartyScores/5
        [HttpDelete]
        [ResponseType(typeof(TournamentPartyScoreDTO))]
        public IHttpActionResult Delete(int id)
        {
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            if (!_tournamentPartyScoreService.IsAdmin(userId: userId, elementId: id)) return BadRequest("Access denied!");
            if (!_tournamentPartyScoreService.Delete(id: id)) return NotFound();
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _uow.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}