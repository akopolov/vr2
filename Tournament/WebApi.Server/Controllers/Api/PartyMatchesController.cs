﻿using System;
using BL.DTO;
using BL.IServices;
using System.Web.Http;
using System.Web.Http.Description;
using Interfaces.IUow;
using Microsoft.AspNet.Identity;

namespace WebApi.Server.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/PartyMatches")]
    public class PartyMatchesController : ApiController
    {
        private readonly IPartyMatchService _partyMatchService;
        private readonly IGameMatchService _gameMatchService;
        private readonly IUow _uow;

        public PartyMatchesController(IPartyMatchService partyMatchService, 
            IGameMatchService gameMatchService, IUow uow)
        {
            _partyMatchService = partyMatchService;
            _gameMatchService = gameMatchService;
            _uow = uow;
        }

        // GET: api/PartyMatches
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetAll()
        {
            return Ok(_partyMatchService.GetAll());
        }

        // GET: api/PartyMatches/5
        [HttpGet]
        [AllowAnonymous]
        [ResponseType(typeof(PartyMatchDTO))]
        public IHttpActionResult GetById(int id)
        {
            var partyMatch = _partyMatchService.GetById(id: id);
            if (partyMatch == null) return NotFound();
            return Ok(partyMatch);
        }

        // PUT: api/PartyMatches/5
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult Update(int id, PartyMatchDTO partyMatchDto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (id != partyMatchDto.PartyMatchId) return BadRequest();
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            if (!_partyMatchService.IsAdmin(userId: userId, elementId: id)) return BadRequest("Access denied!");
            if (!_partyMatchService.Update(partyMatchDto)) return BadRequest("Incorrect primary or foreign key");
            return Ok();
        }

        // POST: api/PartyMatches
        [HttpPost]
        [ResponseType(typeof(PartyMatchDTO))]
        public IHttpActionResult Add(PartyMatchDTO partyMatchDto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            var gameMatchId = partyMatchDto.GameMatchId;
            if (!_gameMatchService.IsAdmin(userId: userId, elementId: gameMatchId)) return BadRequest("Access denied!");
            if (!_partyMatchService.Add(partyMatchDto)) return BadRequest("Incorrect primary or foreign key");
            return CreatedAtRoute("DefaultApi", new { id = partyMatchDto.PartyMatchId }, partyMatchDto);
        }

        // DELETE: api/PartyMatches/5
        [HttpDelete]
        [ResponseType(typeof(PartyMatchDTO))]
        public IHttpActionResult Delete(int id)
        {
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            if (!_partyMatchService.IsAdmin(userId: userId, elementId: id)) return BadRequest("Access denied!");
            if (!_partyMatchService.Delete(id: id)) return NotFound();
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _uow.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}