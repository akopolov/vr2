﻿using Interfaces.IUow;
using System;
using System.Web.Http;

namespace WebApi.Server.Controllers.Api
{

    [RoutePrefix("api/Values")]
    public class ValuesController : ApiController
    {
        private readonly IUow _uow;

        public ValuesController(IUow uow)
        {
            _uow = uow;
        }

        // GET api/values
        [Authorize]
        public string Get()
        {
            System.Diagnostics.Debug.WriteLine("Get");

            System.Diagnostics.Debug.WriteLine(User.IsInRole("Admin"));

            return DateTime.Now + " " + _uow.UsersInt.All.Count;

        }

        // ROLE auth test
        [Route("GetAdminValues")]
        [Authorize(Roles = "Admin")]
        public string GetAdminValues()
        {
            System.Diagnostics.Debug.WriteLine("GetAdminValues");

            System.Diagnostics.Debug.WriteLine(User.IsInRole("Admin"));
            return DateTime.Now + " " + _uow.UsersInt.All.Count;
        }
    }
}
