﻿using Domain.Identity;
using Interfaces.IUow;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace WebApi.Server.Controllers.Api
{
    [Authorize(Roles = "Admin")]
    [RoutePrefix("api/UserClaimsInt")]
    public class UserClaimsIntController : ApiController
    {
        private readonly IUow _uow;

        public UserClaimsIntController(IUow uow)
        {
            _uow = uow;

        }

        [HttpGet]
        public List<UserClaimInt> GetByUserId(int userId)
        {
            return _uow.UserClaimsInt.AllForUserId(userId);
        }

        [HttpGet]
        public List<UserClaimInt> GetAll()
        {
            return _uow.UserClaimsInt.All;
        }

        [HttpGet]
        [ResponseType(typeof(UserClaimInt))]
        public IHttpActionResult GetById(int id)
        {
            var userClaimInt = _uow.UserClaimsInt.Find(id);
            if (userClaimInt == null)
            {
                return NotFound();
            }

            return Ok(userClaimInt);
        }

        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult Update(int id, UserClaimInt userClaimInt)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userClaimInt.Id)
            {
                return BadRequest();
            }

            _uow.UserClaimsInt.Update(userClaimInt);

            try
            {
                _uow.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (_uow.UserClaimsInt.Find(id) != null)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpPost]
        [ResponseType(typeof(UserClaimInt))]
        public IHttpActionResult Add(UserClaimInt userClaimInt)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _uow.UserClaimsInt.Add(userClaimInt);
            _uow.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = userClaimInt.Id }, userClaimInt);
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var userClaimInt = _uow.UserClaimsInt.Find(id);
            if (userClaimInt == null)
            {
                return NotFound();
            }

            _uow.UserClaimsInt.Remove(userClaimInt);
            _uow.SaveChanges();

            return Ok(userClaimInt);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _uow.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}