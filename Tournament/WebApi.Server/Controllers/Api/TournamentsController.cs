﻿using System;
using BL.DTO;
using BL.IServices;
using System.Web.Http;
using System.Web.Http.Description;
using Interfaces.IUow;
using Microsoft.AspNet.Identity;

namespace WebApi.Server.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/Tournaments")]
    public class TournamentsController : ApiController
    {
        private readonly ITournamentService _tournamentService;
        private readonly IUow _uow;

        public TournamentsController(ITournamentService tournamentService, IUow uow)
        {
            _tournamentService = tournamentService;
            _uow = uow;
        }

        // GET: api/Tournaments
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetAll()
        {
            return Ok(_tournamentService.GetAll());
        }

        // GET: api/Tournaments/5
        [HttpGet]
        [AllowAnonymous]
        [ResponseType(typeof(TournamentDTO))]
        public IHttpActionResult GetById(int id)
        {
            var tournament = _tournamentService.GetById(id: id);
            if (tournament == null) return NotFound();
            return Ok(tournament);
        }

        // PUT: api/Tournaments/5
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult Update(int id, TournamentDTO tournamentDto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (id != tournamentDto.TournamentId) return BadRequest();
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            if (!_tournamentService.IsAdmin(userId:userId, tournamentId: id)) return BadRequest("Access denied!");
            if (!_tournamentService.Update(tournamentDto)) return BadRequest("Incorrect primary or foreign key");
            return Ok();
        }

        // POST: api/Tournaments
        [HttpPost]
        [ResponseType(typeof(TournamentDTO))]
        public IHttpActionResult Add(TournamentDTO tournamentDto)
        {
            tournamentDto.AdminUserId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (!_tournamentService.Add(tournamentDto)) return BadRequest("Incorrect foreign key");
            return CreatedAtRoute("DefaultApi", new { id = tournamentDto.TournamentId }, tournamentDto);
        }

        // DELETE: api/Tournaments/5
        [HttpDelete]
        [ResponseType(typeof(TournamentDTO))]
        public IHttpActionResult Delete(int id)
        {
            var userId = Int32.Parse(RequestContext.Principal.Identity.GetUserId());
            if (!_tournamentService.IsAdmin(userId: userId, tournamentId: id)) return BadRequest("Access denied!");
            if (!_tournamentService.Delete(id: id)) return NotFound();
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _uow.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}