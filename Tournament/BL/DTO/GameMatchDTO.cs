﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BL.DTO
{
    public class GameMatchDTO
    {
        public int GameMatchId { get; set; }

        [Required(ErrorMessage = "Starting time is requierd")]
        [DataType(DataType.DateTime, ErrorMessage = "Start must be a Date Time")]
        public DateTime Start { get; set; }

        [DataType(DataType.DateTime, ErrorMessage = "End must be a Date Time")]
        public DateTime? End { get; set; }

        [Required(ErrorMessage = "Game mode Id is requierd")]
        public int GameModeId { get; set; }

        [Required(ErrorMessage = "Tournament Id is requierd")]
        public int TournamentId { get; set; }


        public string TournamentName { get; set; }
        public DateTime TournamentYear { get; set; }
        public string GameMode { get; set; }
        public string GameName { get; set; }

        //List<>
        //List<>
    }
}
