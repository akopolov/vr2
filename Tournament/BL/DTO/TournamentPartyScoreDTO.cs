﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BL.DTO
{
    public class TournamentPartyScoreDTO
    {
        public int TournamentPartyScoreId { get; set; }

        [Required(ErrorMessage = "Party score is required.")]
        public decimal Score { get; set; }

        [Required(ErrorMessage = "Party Id is required.")]
        public int ParyId { get; set; }

        [Required(ErrorMessage = "Party Id is required.")]
        public int TournamentId { get; set; }

        public string PartyName { get; set; }
        public string TournamentName { get; set; }
        [DataType(DataType.Date, ErrorMessage = "Match day must be a DATE")]
        public DateTime TournamentYear { get; set; }
    }
}
