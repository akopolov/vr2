﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BL.DTO
{
    public class PartyDTO
    {
        public int PartyId { get; set; }

        [Required(ErrorMessage = "Party name is required")]
        public string PartyName { get; set; }

        [Required(ErrorMessage = "Party score is required")]
        [Range(0, Double.MaxValue)]
        public decimal PartyScore { get; set; }

        [Required(ErrorMessage = "Game Id is required")]
        public int GameId { get; set; }

        [Required(ErrorMessage = "Team Id is required")]
        public int TeamId { get; set; }


        public string TeamName { get; set; }

        public string GameName { get; set; }
    }
}
