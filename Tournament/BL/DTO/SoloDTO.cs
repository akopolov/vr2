﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BL.DTO
{
    public class SoloDTO
    {
        public int SoloId { get; set; }

        [Range(0, Double.MaxValue)]
        public decimal SoloScore { get; set; }

        [Required(ErrorMessage = "User Id is required")]
        public int UserIntId { get; set; }

        [Required(ErrorMessage = "Game Id is required")]
        public int GameId { get; set; }

        public string GameName { get; set; }
        public string UserFullName { get; set; }
        public string UserName { get; set; }
    }
}
