﻿using System.ComponentModel.DataAnnotations;

namespace BL.DTO
{
    public class TeamDTO
    {
        public int TeamId { get; set; }

        [Required(ErrorMessage = "Team name is required")]
        public string TeamName { get; set; }

        [Required(ErrorMessage = "Team short name is required")]
        public string TeamShortName { get; set; }

        [Required(ErrorMessage = "Team Rating is required")]
        public decimal TeamRating { get; set; }
    }
}
