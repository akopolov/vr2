﻿using System;
using System.ComponentModel.DataAnnotations;
using Domain.Enum;

namespace BL.DTO
{
    public class TournamentDTO
    {
        public int TournamentId { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Tournament name can not be longer that 100 char.")]
        public string TournamentName { get; set; }

        [Required(ErrorMessage = "Tournament starting date must be provided.")]
        [DataType(DataType.Date, ErrorMessage = "Datatype must be a date.")]
        public DateTime TournamentFromDate { get; set; }

        [Required(ErrorMessage = "Tournament end date must be provided.")]
        [DataType(DataType.Date, ErrorMessage = "Datatype must be a date.")]
        public DateTime TournamentToDate { get; set; }

        [Required(ErrorMessage = "Game Id is required")]
        public int GameId { get; set; }

        [Required(ErrorMessage = "Tournament season is required")]
        [Range(0, 4, ErrorMessage = "Tournament season can not be smaller than 0 or biger than 4")]
        public TournamentSeason TournamentSeason { get; set; }

        [Required(ErrorMessage = "Admin Id is required")]
        public int AdminUserId { get; set; }


        public string GameName { get; set; }
        public string AdminFullName { get; set; }
    }
}
