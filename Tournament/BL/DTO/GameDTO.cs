﻿using System.ComponentModel.DataAnnotations;

namespace BL.DTO
{
    public class GameDTO
    {
        public int GameId { get; set; }

        [Required]
        [StringLength(50)]
        public string GameName { get; set; }
    }
}
