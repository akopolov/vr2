﻿using System.ComponentModel.DataAnnotations;
using Domain.Enum;

namespace BL.DTO
{
    public class TeamPersonInPartyDTO
    {
        public int TeamPersonInPartyId { get; set; }

        [Required(ErrorMessage = "party id is required")]
        public int PartyId { get; set; }

        [Required(ErrorMessage = "Person in team is required")]
        public int PersonInTeamId { get; set; }

        [Required(ErrorMessage = "Party roll is required")]
        [Range(0, 6 ,ErrorMessage = "party roll can not be lower than 0 or greater than 6")]
        public PartyRoll PartyRoll { get; set; }

        public string PartyName { get; set; }
        public string TeamName { get; set; }
        public string UserFullName { get; set; }
    }
}
