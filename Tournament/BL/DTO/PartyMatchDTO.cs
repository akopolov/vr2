﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BL.DTO
{
    public class PartyMatchDTO
    {
        public int PartyMatchId { get; set; }

        [Display(Name = "Match result")]
        [Range(0, Int32.MaxValue)]
        public int Result { get; set; }

        [Required(ErrorMessage = "Party Id is required")]
        public int PartyId { get; set; }

        [Required(ErrorMessage = "Match Id is required")]
        public int GameMatchId { get; set; }

        public string GameName { get; set; }

        public string PartyName { get; set; }

        public string TournamentName { get; set; }

        [DataType(DataType.Date, ErrorMessage = "Match day must be a DATE")]
        public DateTime MatchDay { get; set; }
    }
}
