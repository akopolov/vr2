﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BL.DTO
{
    public class TournamentPersonScoreDTO
    {
        public int TournamentPersonScoreId { get; set; }

        [Required(ErrorMessage = "Person score is required.")]
        public decimal Score { get; set; }

        [Required(ErrorMessage = "Person Id is requierd.")]
        public int SoloId { get; set; }

        [Required(ErrorMessage = "Tournament Id is requierd.")]
        public int TournamentId { get; set; }

        public string PersonFullName { get; set; }
        public string TournamentName { get; set; }
        public string GameName { get; set; }
        [DataType(DataType.Date, ErrorMessage = "Match day must be a DATE")]
        public DateTime TournamentYear { get; set; }
    }
}
