﻿using Domain.Enum;
using System;
using System.ComponentModel.DataAnnotations;

namespace BL.DTO
{
    public class PersonInTeamDTO
    {
        public int PersonInTeamId { get; set; }

        [Required(ErrorMessage = "From date is required")]
        [DataType(DataType.Date)]
        public DateTime FromDate { get; set; }

        [Required(ErrorMessage = "To date is required")]
        [DataType(DataType.Date)]
        public DateTime? ToDate { get; set; }

        [Required(ErrorMessage = "Team Id is required")]
        public int TeamId { get; set; }

        [Required(ErrorMessage = "User Id is required")]
        public int UserId { get; set; }

        [Required(ErrorMessage = "Team roll is required")]
        [Range(0, 3, ErrorMessage = "Team roll can not be lower that 0 or graiter than 3")]
        public TeamRoll TeamRoll { get; set; }

        public string TeamRollString { get; set; }


        public string UserFullName { get; set; }
        public string UserName { get; set; }
        public string TeamName { get; set; }
    }
}
