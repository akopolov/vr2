﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BL.DTO
{
    public class GameModeDTO
    {
        public int GameModeId { get; set; }

        [Required]
        [StringLength(50,ErrorMessage = "Game mode name can not be longer that 50 char")]
        public string GameModeName { get; set; }

        [Required]
        [Range(1, Int32.MaxValue)]
        public int PlayersInOneTeam { get; set; }

        [Required]
        public int GameId { get; set; }


        public string GameName { get; set; }
    }
}
