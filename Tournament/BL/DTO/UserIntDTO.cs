﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTO
{
    public class UserIntDTO
    {
        public int UserIntId { get; set; }
        public string firstLastName { get; set; }
        public string userName { get; set; }

    }
}
