﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BL.DTO
{
    public class SoloMatchDTO
    {
        public int SoloMatchId { get; set; }

        [Range(0, Int32.MaxValue)]
        public int GameResult { get; set; }

        public int SoloId { get; set; }

        [Required(ErrorMessage = "Game match id is required")]
        public int GameMatchId { get; set; }

        public string GameName { get; set; }

        public string UserFullName { get; set; }

        public string TournamentName { get; set; }

        [DataType(DataType.Date, ErrorMessage = "Match day must be a DATE")]
        public DateTime MatchDay { get; set; }
    }
}
