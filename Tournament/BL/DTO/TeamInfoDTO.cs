﻿using System.Collections.Generic;

namespace BL.DTO
{
    public class TeamInfoDTO
    {
        public TeamDTO Team { get; set; }
        public List<PersonInTeamDTO> PersonsInTeam { get; set; }
    }
}
