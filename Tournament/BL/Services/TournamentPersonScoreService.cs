﻿using System;
using System.Collections.Generic;
using System.Linq;
using BL.DTO;
using BL.IServices;
using BL.ObjectFactories;
using Interfaces.IUow;

namespace BL.Services
{
    public class TournamentPersonScoreService : ITournamentPersonScoreService
    {
        private readonly IUow _uow;
        private readonly TournamentPersonScoreFactory _tournamentPersonScoreFactory;

        public TournamentPersonScoreService(IUow uow, TournamentPersonScoreFactory tournamentPersonScoreFactory)
        {
            _uow = uow;
            _tournamentPersonScoreFactory = tournamentPersonScoreFactory;
        }

        public List<TournamentPersonScoreDTO> GetAll()
        {
            return _uow.TournamentPersonScores
                .FetchAll()
                .Select(p => _tournamentPersonScoreFactory.Create(p))
                .ToList();
        }

        public bool Add(TournamentPersonScoreDTO tournamentPersonScoreDto)
        {
            if (_uow.Tournaments.Find(tournamentPersonScoreDto.TournamentId) == null ||
                _uow.Solos.Find(tournamentPersonScoreDto.SoloId) == null
                ) return false;
            _uow.TournamentPersonScores.Add(_tournamentPersonScoreFactory.Create(tournamentPersonScoreDto: tournamentPersonScoreDto));
            _uow.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            if (_uow.TournamentPersonScores.Find(id: id) == null) return false;
            _uow.TournamentPersonScores.Remove(id: id);
            _uow.SaveChanges();
            return true;
        }

        public TournamentPersonScoreDTO GetById(int id)
        {
            var score = _uow.TournamentPersonScores.FetchById(id: id);
            return score == null ? null : _tournamentPersonScoreFactory.Create(tournamentPersonScore: score);
        }

        public bool Update(TournamentPersonScoreDTO tournamentPersonScoreDto)
        {
            if (_uow.TournamentPersonScores.Find(id: tournamentPersonScoreDto.TournamentPersonScoreId) == null ||
                _uow.Tournaments.Find(tournamentPersonScoreDto.TournamentId) == null ||
                _uow.Solos.Find(tournamentPersonScoreDto.SoloId) == null
                ) return false;
            _uow.TournamentPersonScores.Update(_tournamentPersonScoreFactory.Create(tournamentPersonScoreDto: tournamentPersonScoreDto));
            _uow.SaveChanges();
            return true;
        }

        public bool IsAdmin(int userId, int elementId)
        {
            if (_uow.TournamentPersonScores.FetchById(id: elementId)?.TournamentId != userId) return false;
            return true;
        }

        public List<TournamentPersonScoreDTO> GetByTournamentId(int tournamentId)
        {
            return _uow.TournamentPersonScores
                .GetByTournamentId(tournamentId)
                .Select(p => _tournamentPersonScoreFactory.Create(p))
                .ToList();
        }
    }
}
