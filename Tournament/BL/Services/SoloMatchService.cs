﻿using BL.DTO;
using BL.IServices;
using BL.ObjectFactories;
using Interfaces.IUow;
using System.Collections.Generic;
using System.Linq;

namespace BL.Services
{
    public class SoloMatchService : ISoloMatchService
    {
        private readonly IUow _uow;
        private readonly SoloMatchFactory _soloMatchFactory;

        public SoloMatchService(IUow uow, SoloMatchFactory soloMatchFactory)
        {
            _uow = uow;
            _soloMatchFactory = soloMatchFactory;
        }

        public List<SoloMatchDTO> GetAll()
        {
            return _uow.SoloMatches.FetchAll().Select(p => _soloMatchFactory.Create(p)).ToList();
        }

        public bool Add(SoloMatchDTO soloMatchDto)
        {
            if (_uow.Solos.Find(id: soloMatchDto.SoloId) == null ||
                _uow.GameMatches.Find(id: soloMatchDto.GameMatchId) == null
                ) return false;
            _uow.SoloMatches.Add(_soloMatchFactory.Create(soloMatchDto: soloMatchDto));
            _uow.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            if (_uow.SoloMatches.Find(id: id) == null) return false;
            _uow.SoloMatches.Remove(id: id);
            _uow.SaveChanges();
            return true;
        }

        public SoloMatchDTO GetById(int id)
        {
            var item = _uow.SoloMatches.FetchById(id: id);
            return item == null ? null : _soloMatchFactory.Create(soloMatch: item);
        }

        public bool Update(SoloMatchDTO soloMatchDto)
        {
            if (_uow.SoloMatches.Find(id: soloMatchDto.SoloMatchId) == null ||
                _uow.Solos.Find(id: soloMatchDto.SoloId) == null ||
                _uow.GameMatches.Find(id: soloMatchDto.GameMatchId) == null
                ) return false;
            _uow.SoloMatches.Update(_soloMatchFactory.Create(soloMatchDto: soloMatchDto));
            _uow.SaveChanges();
            return true;
        }

        public bool IsAdmin(int userId, int elementId)
        {
            if (_uow.SoloMatches.FetchById(id: elementId)?.GameMatch?.Tournament?.AdminUserId != userId) return false;
            return true;
        }
    }
}
