﻿using BL.DTO;
using BL.IServices;
using Interfaces.IUow;
using System.Collections.Generic;
using System.Linq;
using BL.ObjectFactories;

namespace BL.Services
{
    public class GamesService : IGamesService
    {
        private readonly IUow _uow;
        private readonly GameFactory _gameFactory;

        public GamesService(IUow uow, GameFactory gameFactory)
        {
            _uow = uow;
            _gameFactory = gameFactory;
        }

        public List<GameDTO> GetAll()
        {
            return _uow.Games.All.Select(p => _gameFactory.Create(p)).ToList();
        }

        public bool Add(GameDTO game)
        {
            _uow.Games.Add(_gameFactory.Create(gameDto: game));
            _uow.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            if (_uow.Games.Find(id: id) == null) return false;
            _uow.Games.Remove(id: id);
            _uow.SaveChanges();
            return true;
        }

        public GameDTO GetById(int id)
        {
            var game = _uow.Games.Find(id: id);
            return game == null ? null : _gameFactory.Create(game: game);
        }

        public bool Update(GameDTO game)
        {
            if (_uow.Games.Find(id: game.GameId) == null) return false;
            _uow.Games.Update(_gameFactory.Create(gameDto: game));
            _uow.SaveChanges();
            return true;
        }
    }
}
