﻿using System.Collections.Generic;
using System.Linq;
using BL.DTO;
using BL.IServices;
using BL.ObjectFactories;
using Interfaces.IUow;

namespace BL.Services
{
    public class GameMatchService : IGameMatchService
    {
        private readonly IUow _uow;
        private readonly GameMatchFactory _gameMatchFactory;
        public GameMatchService(IUow uow, GameMatchFactory gameMatchFactory)
        {
            _uow = uow;
            _gameMatchFactory = gameMatchFactory;
        }
        public List<GameMatchDTO> GetAll()
        {
            return _uow.GameMatches.FetchAll().Select(p => _gameMatchFactory.Create(p)).ToList();
        }

        public bool Add(GameMatchDTO gameMatchDto)
        {
            if (_uow.Tournaments.Find(gameMatchDto.TournamentId) == null ||
                _uow.GameModes.Find(gameMatchDto.GameModeId) == null
                ) return false;
            _uow.GameMatches.Add(_gameMatchFactory.Create(gameMatchDto: gameMatchDto));
            _uow.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            if (_uow.GameMatches.Find(id: id) == null) return false;
            _uow.GameMatches.Remove(id: id);
            _uow.SaveChanges();
            return true;
        }

        public GameMatchDTO GetById(int id)
        {
            var gameMatch = _uow.GameMatches.FetchById(id: id);
            return gameMatch == null ? null : _gameMatchFactory.Create(gameMatch: gameMatch);
        }

        public bool Update(GameMatchDTO gameMatchDto)
        {
            var match = _uow.GameMatches.Find(id: gameMatchDto.GameMatchId);
            if (match == null ||
                _uow.Tournaments.Find(id: gameMatchDto.TournamentId) == null ||
                _uow.GameModes.Find(id: gameMatchDto.GameModeId) == null
               ) return false;
            gameMatchDto.TournamentId = match.TournamentId;
            _uow.GameMatches.Update(_gameMatchFactory.Create(gameMatchDto: gameMatchDto));
            _uow.SaveChanges();
            return true;
        }

        public bool IsAdmin(int userId, int elementId)
        {
            if (_uow.GameMatches.FetchById(id: elementId)?.Tournament.AdminUserId != userId) return false;
            return true;
        }
    }
}