﻿using System.Collections.Generic;
using System.Linq;
using BL.DTO;
using BL.IServices;
using BL.ObjectFactories;
using Domain.Enum;
using Interfaces.IUow;

namespace BL.Services
{
    public class PersonInTeamService : IPersonInTeamService
    {
        private readonly IUow _uow;
        private readonly PersonInTeamFactory _personInTeamFactory;

        public PersonInTeamService(IUow uow, PersonInTeamFactory personInTeamFactory)
        {
            _uow = uow;
            _personInTeamFactory = personInTeamFactory;
        }

        public List<PersonInTeamDTO> GetAll()
        {
            return _uow.PersonInTeams.FetchAll().Select(p => _personInTeamFactory.Create(p)).ToList();
        }

        public bool Add(PersonInTeamDTO personInTeamDto)
        {
            if (_uow.UsersInt.Find(personInTeamDto.UserId) == null ||
                _uow.Teams.Find(personInTeamDto.TeamId) == null
               ) return false;
            _uow.PersonInTeams.Add(_personInTeamFactory.Create(personInTeamDto));
            _uow.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            if (_uow.PersonInTeams.Find(id: id) == null) return false;
            foreach (var teamPersoneInParty in _uow.TeamPersoneInParties.FetchByPersonInTeam(id))
            {
                _uow.TeamPersoneInParties.Remove(teamPersoneInParty);
            }
            _uow.SaveChanges();
            _uow.PersonInTeams.Remove(id: id);
            _uow.SaveChanges();
            return true;
        }

        public PersonInTeamDTO GetById(int id)
        {
            var item = _uow.PersonInTeams.FetchById(id: id);
            return item == null ? null : _personInTeamFactory.Create(item);
        }

        public bool Update(PersonInTeamDTO personInTeamDto)
        {
            if (_uow.PersonInTeams.Find(personInTeamDto.PersonInTeamId) == null ||
                _uow.UsersInt.Find(personInTeamDto.UserId) == null ||
                _uow.Teams.Find(personInTeamDto.TeamId) == null
               ) return false;
            _uow.PersonInTeams.Update(_personInTeamFactory.Create(personInTeamDto));
            _uow.SaveChanges();
            return true;
        }

        public bool CanUpdate(PersonInTeamDTO personInTeamDto)
        {
            var updateUser = _uow.PersonInTeams.Find(personInTeamDto.PersonInTeamId);
            if (updateUser?.TeamRoll != TeamRoll.Manager) return true;
            if (_uow.PersonInTeams.FetchTeamManagers(updateUser.TeamId).Count > 1) return true;
            if (personInTeamDto.TeamRoll == TeamRoll.Manager) return true;
            return false;
        }

        public bool CanDelete(int personInTeamId)
        {
            var deleteUser = _uow.PersonInTeams.Find(id: personInTeamId);
            if (deleteUser.TeamRoll != TeamRoll.Manager) return true;
            if (_uow.PersonInTeams.FetchTeamManagers(deleteUser.TeamId).Count > 1) return true;
            return false;
        }

        public bool IsManager(int userId, int personInTeamId)
        {
            var personInTeam = _uow.PersonInTeams.Find(personInTeamId);
            if (personInTeam == null) return false;
            if (_uow.PersonInTeams.FetchTeamManagers(teamId: personInTeam.TeamId).FirstOrDefault(p => p.UserId == userId) == null) return false;
            return true;
        }

        public List<PersonInTeamDTO> GetByTeamId(int id)
        {
            return _uow.PersonInTeams.FetchByTeamId(id: id)
                .Select(p => _personInTeamFactory.Create(p)).ToList();
        }
    }
}
