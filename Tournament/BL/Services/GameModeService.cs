﻿using System.Collections.Generic;
using System.Linq;
using BL.DTO;
using BL.IServices;
using BL.ObjectFactories;
using Interfaces.IUow;

namespace BL.Services
{
    public class GameModeService : IGameModeService
    {
        private readonly IUow _uow;
        private readonly GameModeFactory _gameModeFactory;

        public GameModeService(IUow uow, GameModeFactory gameModeFactory)
        {
            _uow = uow;
            _gameModeFactory = gameModeFactory;
        }

        public List<GameModeDTO> GetAll()
        {
            return _uow.GameModes.FetchAll().Select(p => _gameModeFactory.Create(p)).ToList();
        }

        public bool Add(GameModeDTO gameMode)
        {
            if (_uow.Games.Find(gameMode.GameId) == null) return false;
            _uow.GameModes.Add(_gameModeFactory.Create(gameModeDto: gameMode));
            _uow.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            if (_uow.GameModes.Find(id: id) == null) return false; 
            _uow.GameModes.Remove(id: id);
            _uow.SaveChanges();
            return true;
        }

        public GameModeDTO GetById(int id)
        {
            var gameMode = _uow.GameModes.FetchById(id: id);
            return gameMode == null ? null : _gameModeFactory.Create(gameMode: gameMode);
        }

        public bool Update(GameModeDTO gameMode)
        {
            if (_uow.GameModes.Find(gameMode.GameModeId) == null ||
                _uow.Games.Find(gameMode.GameId) == null
                ) return false;
            _uow.GameModes.Update(_gameModeFactory.Create(gameModeDto: gameMode));
            _uow.SaveChanges();
            return true;
        }
    }
}