﻿using BL.DTO;
using BL.IServices;
using BL.ObjectFactories;
using Interfaces.IUow;
using System.Collections.Generic;
using System.Linq;
using System;

namespace BL.Services
{
    public class SoloService : ISoloService
    {
        private readonly IUow _uow;
        private readonly SoloFactory _soloFactory;

        public SoloService(IUow uow, SoloFactory soloFactory)
        {
            _uow = uow;
            _soloFactory = soloFactory;
        }

        public List<SoloDTO> GetAll()
        {
            return _uow.Solos.FetchAll().Select(p => _soloFactory.Create(solo: p)).ToList();
        }

        public bool Add(SoloDTO soloDto)
        {
            if (_uow.UsersInt.Find(soloDto.UserIntId) == null ||
                _uow.Games.Find(soloDto.GameId) == null
                ) return false;
            _uow.Solos.Add(_soloFactory.Create(soloDto));
            _uow.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            if (_uow.Solos.Find(id: id) == null) return false;
            _uow.Solos.Remove(id: id);
            _uow.SaveChanges();
            return true;
        }

        public SoloDTO GetById(int id)
        {
            var item = _uow.Solos.FetchById(id: id);
            return item == null ? null : _soloFactory.Create(solo: item);
        }

        public bool Update(SoloDTO soloDto)
        {
            if (_uow.Solos.Find(soloDto.SoloId) == null ||
                _uow.UsersInt.Find(soloDto.UserIntId) == null ||
                _uow.Games.Find(soloDto.SoloId) == null
                ) return false;
            _uow.Solos.Update(_soloFactory.Create(soloDto: soloDto));
            _uow.SaveChanges();
            return true;
        }

        public List<SoloDTO> GetByGameId(int gameId)
        {
            return _uow.Solos.FetchByGameId(gameId).Select(p => _soloFactory.Create(solo: p)).ToList();
        }

        public List<SoloDTO> GetByUserId(int userId)
        {
            return _uow.Solos.FetchByUserId(userId).Select(p => _soloFactory.Create(solo: p)).ToList();
        }

        public bool IsUser(int userId, int elementId)
        {
            if (_uow.Solos.FetchById(id: elementId).UserIntId != userId) return false;
            return true;
        }
    }
}
