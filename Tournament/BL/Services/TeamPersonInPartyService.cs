﻿using BL.DTO;
using BL.IServices;
using BL.ObjectFactories;
using Interfaces.IUow;
using System.Collections.Generic;
using System.Linq;

namespace BL.Services
{
    public class TeamPersonInPartyService : ITeamPersonInPartyService
    {
        private readonly IUow _uow;
        private readonly TeamPersonInPartyFactory _teamPersonInPartyFactory;
        public TeamPersonInPartyService(IUow uow, TeamPersonInPartyFactory teamPersonInPartyFactory)
        {
            _uow = uow;
            _teamPersonInPartyFactory = teamPersonInPartyFactory;
        }

        public List<TeamPersonInPartyDTO> GetAll()
        {
            return _uow.TeamPersoneInParties.FetchAll().Select(p => _teamPersonInPartyFactory.Create(p)).ToList();
        }

        public bool Add(TeamPersonInPartyDTO teamPersonInPartyDto)
        {
            if (_uow.Parties.Find(teamPersonInPartyDto.PartyId) == null ||
                _uow.PersonInTeams.Find(teamPersonInPartyDto.PersonInTeamId) == null
                ) return false;
            _uow.TeamPersoneInParties.Add(_teamPersonInPartyFactory.Create(teamPersonInPartyDto));
            _uow.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            if (_uow.TeamPersoneInParties.Find(id: id) == null) return false;
            _uow.TeamPersoneInParties.Remove(id: id);
            _uow.SaveChanges();
            return true;
        }

        public TeamPersonInPartyDTO GetById(int id)
        {
            var item = _uow.TeamPersoneInParties.FetchById(id: id);
            return item == null ? null : _teamPersonInPartyFactory.Create(item);
        }

        public bool Update(TeamPersonInPartyDTO teamPersonInPartyDto)
        {
            if (_uow.TeamPersoneInParties.Find(teamPersonInPartyDto.TeamPersonInPartyId) == null ||
                _uow.Parties.Find(teamPersonInPartyDto.PartyId) == null ||
                _uow.PersonInTeams.Find(teamPersonInPartyDto.PersonInTeamId) == null
            ) return false;
            _uow.TeamPersoneInParties.Update(_teamPersonInPartyFactory.Create(teamPersonInPartyDto));
            _uow.SaveChanges();
            return true;
        }

        public bool IsManager(int userId, int teamPersonInPartyId)
        {
            var personInteam = _uow.TeamPersoneInParties.Find(id: teamPersonInPartyId);
            if (_uow.PersonInTeams
                .FetchTeamManagers(personInteam.PersonInTeamId)
                .FirstOrDefault(p => p.UserId == userId) == null
                ) return false;
            return true;
        }
    }
}
