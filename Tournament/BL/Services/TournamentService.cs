﻿using BL.DTO;
using BL.IServices;
using BL.ObjectFactories;
using Interfaces.IUow;
using System.Collections.Generic;
using System.Linq;

namespace BL.Services
{
    public class TournamentService : ITournamentService
    {
        private readonly IUow _uow;
        private readonly TournamentFactory _tournamentFactory;

        public TournamentService(IUow uow, TournamentFactory tournamentFactory)
        {
            _uow = uow;
            _tournamentFactory = tournamentFactory;
        }

        public List<TournamentDTO> GetAll()
        {
            return _uow.Tournaments
                .FetchAll()
                .Select(p => _tournamentFactory.Create(p))
                .ToList();
        }

        public bool Add(TournamentDTO tournamentDto)
        {
            if (_uow.Games.Find(tournamentDto.GameId) == null || 
                _uow.UsersInt.Find(tournamentDto.AdminUserId) == null
                ) return false;
            _uow.Tournaments.Add(_tournamentFactory.Create(tournamentDto: tournamentDto));
            _uow.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            if (_uow.Tournaments.Find(id: id) == null) return false;
            _uow.Tournaments.Remove(id: id);
            _uow.SaveChanges();
            return true;
        }

        public TournamentDTO GetById(int id)
        {
            var tournament = _uow.Tournaments.FetchById(id: id);
            return tournament == null ? null : _tournamentFactory.Create(tournament: tournament);
        }

        public bool Update(TournamentDTO tournamentDto)
        {
            if (_uow.Tournaments.Find(tournamentDto.TournamentId) == null ||
                _uow.Games.Find(tournamentDto.GameId) == null ||
                _uow.UsersInt.Find(tournamentDto.AdminUserId) == null
                ) return false;
            _uow.Tournaments.Update(_tournamentFactory.Create(tournamentDto: tournamentDto));
            return true;
        }

        public bool IsAdmin(int userId, int tournamentId)
        {
            if (_uow.Tournaments.Find(id: tournamentId)?.AdminUserId != userId) return false;
            return true;
        }
    }
}
