﻿using BL.DTO;
using BL.IServices;
using System.Collections.Generic;
using System.Linq;
using BL.ObjectFactories;
using Interfaces.IUow;
using System;

namespace BL.Services
{
    public class TournamentPartyScoreService : ITournamentPartyScoreService
    {
        private readonly IUow _uow;
        private readonly TournamentPartyScoreFactory _tournamentPartyScoreFactory;
        public TournamentPartyScoreService(IUow uow, TournamentPartyScoreFactory tournamentPartyScoreFactory)
        {
            _uow = uow;
            _tournamentPartyScoreFactory = tournamentPartyScoreFactory;
        }

        public List<TournamentPartyScoreDTO> GetAll()
        {
            return _uow.TournamentPartyScores
                .FetchAll()
                .Select(p => _tournamentPartyScoreFactory.Create(p))
                .ToList();
        }

        public bool Add(TournamentPartyScoreDTO tournamentPartyScoreDto)
        {
            if (_uow.Tournaments.Find(id: tournamentPartyScoreDto.TournamentId) == null ||
                _uow.Parties.Find(id: tournamentPartyScoreDto.ParyId) == null
                ) return false;
            _uow.TournamentPartyScores.Add(_tournamentPartyScoreFactory.Create(tournamentPartyScoreDto: tournamentPartyScoreDto));
            _uow.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            if (_uow.TournamentPartyScores.Find(id: id) == null) return false;
            _uow.TournamentPartyScores.Remove(id: id);
            _uow.SaveChanges();
            return true;
        }

        public TournamentPartyScoreDTO GetById(int id)
        {
            var item = _uow.TournamentPartyScores.FetchById(id: id);
            return item == null ? null : _tournamentPartyScoreFactory.Create(tournamentPartyScore: item);
        }

        public bool Update(TournamentPartyScoreDTO tournamentPartyScoreDto)
        {
            if (_uow.TournamentPartyScores.Find(id: tournamentPartyScoreDto.TournamentPartyScoreId) == null ||
                _uow.Tournaments.Find(id: tournamentPartyScoreDto.TournamentId) == null ||
                _uow.Parties.Find(id: tournamentPartyScoreDto.ParyId) == null
                ) return false;
            _uow.TournamentPartyScores.Update(_tournamentPartyScoreFactory.Create(tournamentPartyScoreDto: tournamentPartyScoreDto));
            _uow.SaveChanges();
            return true;
        }

        public bool IsAdmin(int userId, int elementId)
        {
            if (_uow.TournamentPartyScores.FetchById(id: elementId)?.Tournament?.AdminUserId != userId) return false;
            return true;
        }

        public List<TournamentPartyScoreDTO> GetByTournamentId(int tournamentId)
        {
            return _uow.TournamentPartyScores
                .GetByTournamentId(tournamentId)
                .Select(p => _tournamentPartyScoreFactory.Create(p))
                .ToList();
        }
    }
}
