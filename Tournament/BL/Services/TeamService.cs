﻿using System;
using BL.DTO;
using BL.IServices;
using BL.ObjectFactories;
using Interfaces.IUow;
using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Enum;

namespace BL.Services
{
    public class TeamService : ITeamService
    {
        private readonly IUow _uow;
        private readonly TeamFactory _teamFactory;
        private readonly PersonInTeamService _personInTeamService;

        public TeamService(IUow uow, TeamFactory teamFactory, PersonInTeamService personInTeamService)
        {
            _uow = uow;
            _teamFactory = teamFactory;
            _personInTeamService = personInTeamService;
        }

        public List<TeamDTO> GetAll()
        {
            return _uow.Teams.All.Select(p => _teamFactory.Create(p)).ToList();
        }

        public bool Add(TeamDTO teamDto)
        {
            throw new System.NotImplementedException();
        }

        public bool Add(TeamDTO teamDto, int userId)
        {
            var team = _teamFactory.Create(teamDto);
            _uow.Teams.Add(team);
            _uow.SaveChanges();
            _uow.PersonInTeams.Add(new PersonInTeam()
            {
                Team = team,
                UserId = userId,
                PersonInTeamFromDate = DateTime.Now,
                TeamRoll = TeamRoll.Manager
            });
            _uow.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            if (_uow.Teams.Find(id) == null) return false;
            _uow.Teams.Remove(id);
            _uow.SaveChanges();
            return true;
        }

        public TeamDTO GetById(int id)
        {
            var item = _uow.Teams.Find(id);
            return item == null ? null : _teamFactory.Create(item);
        }

        public bool Update(TeamDTO teamDto)
        {
            if (_uow.Teams.Find(teamDto.TeamId) == null) return false;
            _uow.Teams.Update(_teamFactory.Create(teamDto));
            _uow.SaveChanges();
            return true;
        }

        public TeamInfoDTO GetTeamInfo(int id)
        {
            TeamInfoDTO teamInfoDTO = new TeamInfoDTO();
            teamInfoDTO.Team = GetById(id);
            teamInfoDTO.PersonsInTeam = _personInTeamService.GetByTeamId(id);
            return teamInfoDTO;
        }

        public bool IsManager(int userId, int teamId)
        {
            if (_uow.PersonInTeams
                .FetchTeamManagers(teamId: teamId)
                .FirstOrDefault(p => p.UserId == userId) == null
                ) return false;
            return true;
        }
    }
}
