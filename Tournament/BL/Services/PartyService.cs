﻿using System.Collections.Generic;
using System.Linq;
using BL.DTO;
using BL.IServices;
using BL.ObjectFactories;
using Interfaces.IUow;

namespace BL.Services
{
    public class PartyService : IPartyService
    {
        private readonly IUow _uow;
        private readonly PartyFactory _partyFactory;

        public PartyService(IUow uow, PartyFactory partyFactory)
        {
            _uow = uow;
            _partyFactory = partyFactory;
        }

        public List<PartyDTO> GetAll()
        {
            return _uow.Parties.FetchAll().Select(p => _partyFactory.Create(party: p)).ToList();
        }

        public bool Add(PartyDTO partyDto)
        {
            if (_uow.Games.Find(id: partyDto.GameId) == null ||
                _uow.Teams.Find(id: partyDto.TeamId) == null
                ) return false;
            _uow.Parties.Add(_partyFactory.Create(partyDto: partyDto));
            _uow.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            if (_uow.Parties.Find(id: id) == null) return false;
            _uow.Parties.Remove(id: id);
            _uow.SaveChanges();
            return true;
        }

        public PartyDTO GetById(int id)
        {
            var item = _uow.Parties.FetchById(id: id);
            return item == null ? null : _partyFactory.Create(party: item);
        }

        public bool Update(PartyDTO partyDto)
        {
            if (_uow.Parties.Find(id: partyDto.PartyId) == null ||
                _uow.Games.Find(id: partyDto.GameId) == null ||
                _uow.Teams.Find(id: partyDto.TeamId) == null
                ) return false;
            _uow.Parties.Update(_partyFactory.Create(partyDto: partyDto));
            _uow.SaveChanges();
            return true;
        }

        public bool IsManager(int userId, int partyId)
        {
            var party = _uow.Parties.Find(id: partyId);
            if (_uow.PersonInTeams.FetchMembersByTeamId(id: party.TeamId).FirstOrDefault(p => p.UserId == userId) == null) return false;
            return true;
        }
    }
}
