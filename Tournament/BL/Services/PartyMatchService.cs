﻿using BL.DTO;
using BL.IServices;
using BL.ObjectFactories;
using Interfaces.IUow;
using System.Collections.Generic;
using System.Linq;

namespace BL.Services
{
    public class PartyMatchService : IPartyMatchService
    {
        private readonly IUow _uow;
        private readonly PartyMatchFactory _partyMatchFactory;

        public PartyMatchService(IUow uow, PartyMatchFactory partyMatchFactory)
        {
            _uow = uow;
            _partyMatchFactory = partyMatchFactory;
        }

        public List<PartyMatchDTO> GetAll()
        {
            return _uow.PartyMatches.FetchAll().Select(p => _partyMatchFactory.Create(partyMatch: p)).ToList();
        }

        public bool Add(PartyMatchDTO partyMatchDto)
        {
            if (_uow.GameMatches.Find(id: partyMatchDto.GameMatchId) == null ||
                _uow.Parties.Find(id: partyMatchDto.PartyId) == null 
                ) return false;
            _uow.PartyMatches.Add(_partyMatchFactory.Create(partyMatchDto: partyMatchDto));
            return true;
        }

        public bool Delete(int id)
        {
            if (_uow.PartyMatches.Find(id: id) == null) return false;
            _uow.PartyMatches.Remove(id: id);
            _uow.SaveChanges();
            return true;
        }

        public PartyMatchDTO GetById(int id)
        {
            var item = _uow.PartyMatches.FetchById(id: id);
            return item == null ? null : _partyMatchFactory.Create(partyMatch: item);
        }

        public bool Update(PartyMatchDTO partyMatchDto)
        {
            if (_uow.PartyMatches.Find(id: partyMatchDto.PartyMatchId) == null||
                _uow.GameMatches.Find(id: partyMatchDto.GameMatchId) == null ||
                _uow.Parties.Find(id: partyMatchDto.PartyId) == null
                ) return false;
            _uow.PartyMatches.Update(_partyMatchFactory.Create(partyMatchDto: partyMatchDto));
            _uow.SaveChanges();
            return true;
        }

        public bool IsAdmin(int userId, int elementId)
        {
            if (_uow.PartyMatches.FetchById(id: elementId)?.GameMatch?.Tournament.AdminUserId != userId) return true;
            return false;
        }
    }
}
