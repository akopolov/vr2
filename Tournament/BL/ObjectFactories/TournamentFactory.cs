﻿using BL.DTO;
using Domain;

namespace BL.ObjectFactories
{
    public class TournamentFactory
    {
        public Tournament Create(TournamentDTO tournamentDto)
        {
            return new Tournament()
            {
                AdminUserId = tournamentDto.AdminUserId,
                GameId = tournamentDto.GameId,
                TournamentId = tournamentDto.TournamentId,
                TournamentFromDate = tournamentDto.TournamentFromDate,
                TournamentToDate = tournamentDto.TournamentToDate,
                TournamentSeason = tournamentDto.TournamentSeason,
                TournamentName = tournamentDto.TournamentName
            };
        }

        public TournamentDTO Create(Tournament tournament)
        {
            return new TournamentDTO()
            {
                TournamentName = tournament.TournamentName,
                AdminUserId = tournament.AdminUserId,
                GameId = tournament.GameId,
                TournamentId = tournament.TournamentId,
                TournamentFromDate = tournament.TournamentFromDate,
                TournamentToDate = tournament.TournamentToDate,
                TournamentSeason = tournament.TournamentSeason,
                GameName = tournament.Game.GameName,
                AdminFullName = tournament.AdminUser.FirstLastName
            };
        }
    }
}
