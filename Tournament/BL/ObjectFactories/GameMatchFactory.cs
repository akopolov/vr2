﻿using BL.DTO;
using Domain;

namespace BL.ObjectFactories
{
    public class GameMatchFactory
    {
        public GameMatch Create(GameMatchDTO gameMatchDto)
        {
            return new GameMatch()
            {
                GameMatchId = gameMatchDto.GameMatchId,
                GameModeId = gameMatchDto.GameModeId,
                TournamentId = gameMatchDto.TournamentId,
                GameMatchStart = gameMatchDto.Start,
                GameMatchEnd = gameMatchDto.End
            };
        }

        public GameMatchDTO Create(GameMatch gameMatch)
        {
            return new GameMatchDTO()
            {
                GameMatchId = gameMatch.GameMatchId,
                GameModeId = gameMatch.GameModeId,
                TournamentId = gameMatch.TournamentId,
                Start = gameMatch.GameMatchStart,
                End = gameMatch.GameMatchEnd,
                TournamentName = gameMatch.Tournament.TournamentName,
                TournamentYear = gameMatch.Tournament.TournamentFromDate,
                GameMode = gameMatch.GameMode.GameModeName,
                GameName = gameMatch.GameMode.Game.GameName
            };
        }
    }
}