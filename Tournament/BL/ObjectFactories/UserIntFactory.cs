﻿using BL.DTO;
using Domain.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.ObjectFactories
{
    public class UserIntFactory
    {
        public UserIntDTO Create(UserInt userInt)
        {
            return new UserIntDTO()
            {
                userName = userInt.UserName,
                firstLastName = userInt.FirstLastName,
                UserIntId = userInt.Id
            };
        }
    }
}
