﻿using BL.DTO;
using Domain;

namespace BL.ObjectFactories
{
    public class PersonInTeamFactory
    {
        public PersonInTeam Create(PersonInTeamDTO personInTeamDto)
        {
            return new PersonInTeam()
            {
                PersonInTeamId = personInTeamDto.PersonInTeamId,
                TeamId = personInTeamDto.TeamId,
                UserId = personInTeamDto.UserId,
                TeamRoll = personInTeamDto.TeamRoll,
                PersonInTeamFromDate = personInTeamDto.FromDate,
                PersonInTeamToDate = personInTeamDto.ToDate
            };
        }

        public PersonInTeamDTO Create(PersonInTeam personInTeam)
        {
            return new PersonInTeamDTO()
            {
                PersonInTeamId = personInTeam.PersonInTeamId,
                TeamId = personInTeam.TeamId,
                UserId = personInTeam.UserId,
                TeamRoll = personInTeam.TeamRoll,
                TeamRollString = personInTeam.TeamRoll.ToString(),
                FromDate = personInTeam.PersonInTeamFromDate,
                ToDate = personInTeam.PersonInTeamToDate,
                UserFullName = personInTeam.User.FirstLastName,
                TeamName = personInTeam.Team.TeamName,
                UserName = personInTeam.User.UserName
            };
        }
    }
}
