﻿using BL.DTO;
using Domain;

namespace BL.ObjectFactories
{
    public class PartyMatchFactory
    {
        public PartyMatch Create(PartyMatchDTO partyMatchDto)
        {
            return new PartyMatch()
            {
                PartyMatchId = partyMatchDto.PartyMatchId,
                GameMatchId = partyMatchDto.GameMatchId,
                PartyId = partyMatchDto.PartyId,
                Result = partyMatchDto.Result
            };
        }
        public PartyMatchDTO Create(PartyMatch partyMatch)
        {
            return new PartyMatchDTO()
            {
                PartyMatchId = partyMatch.PartyMatchId,
                GameMatchId = partyMatch.GameMatchId,
                PartyId = partyMatch.PartyId,
                Result = partyMatch.Result,
                PartyName = partyMatch.Party.PartyName,
                GameName = partyMatch.Party.Game.GameName,
                MatchDay = partyMatch.GameMatch.GameMatchStart,
                TournamentName = partyMatch.GameMatch.Tournament.TournamentName
            };
        }
    }
}
