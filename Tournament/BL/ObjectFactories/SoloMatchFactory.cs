﻿using BL.DTO;
using Domain;

namespace BL.ObjectFactories
{
    public class SoloMatchFactory
    {
        public SoloMatch Create(SoloMatchDTO soloMatchDto)
        {
            return new SoloMatch()
            {
                SoloMatchId = soloMatchDto.SoloMatchId,
                GameMatchId = soloMatchDto.GameMatchId,
                SoloId = soloMatchDto.SoloId,
                GameResult = soloMatchDto.GameResult,
            };
        }

        public SoloMatchDTO Create(SoloMatch soloMatch)
        {
            return new SoloMatchDTO()
            {
                SoloMatchId = soloMatch.SoloMatchId,
                GameMatchId = soloMatch.GameMatchId,
                SoloId = soloMatch.SoloId,
                GameResult = soloMatch.GameResult,
                UserFullName = soloMatch.Solo.UserInt.FirstLastName,
                GameName = soloMatch.Solo.Game.GameName,
                MatchDay = soloMatch.GameMatch.GameMatchStart,
                TournamentName = soloMatch.GameMatch.Tournament.TournamentName,
            };
        }
    }
}
