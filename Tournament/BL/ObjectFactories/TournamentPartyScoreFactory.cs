﻿using BL.DTO;
using Domain;

namespace BL.ObjectFactories
{
    public class TournamentPartyScoreFactory
    {
        public TournamentPartyScore Create(TournamentPartyScoreDTO tournamentPartyScoreDto)
        {
            return new TournamentPartyScore()
            {
                TournamentPartyScoreId = tournamentPartyScoreDto.TournamentPartyScoreId,
                PartyId = tournamentPartyScoreDto.ParyId,
                TournamentId = tournamentPartyScoreDto.TournamentId,
                Score = tournamentPartyScoreDto.Score,
            };
        }

        public TournamentPartyScoreDTO Create(TournamentPartyScore tournamentPartyScore)
        {
            return new TournamentPartyScoreDTO()
            {
                TournamentPartyScoreId = tournamentPartyScore.TournamentPartyScoreId,
                TournamentId = tournamentPartyScore.TournamentId,
                ParyId = tournamentPartyScore.PartyId,
                Score = tournamentPartyScore.Score,

                TournamentName = tournamentPartyScore.Tournament.TournamentName,
                PartyName = tournamentPartyScore.Party.PartyName,
                TournamentYear = tournamentPartyScore.Tournament.TournamentFromDate
            };
        }
    }
}
