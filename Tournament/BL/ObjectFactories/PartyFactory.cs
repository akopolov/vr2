﻿using BL.DTO;
using Domain;

namespace BL.ObjectFactories
{
    public class PartyFactory
    {
        public PartyDTO Create(Party party)
        {
            return new PartyDTO()
            {
                PartyId = party.PartyId,
                TeamId = party.TeamId,
                GameId = party.GameId,
                PartyName = party.PartyName,
                PartyScore = party.PartyScore,
                GameName = party.Game.GameName,
                TeamName = party.Team.TeamName,
            };
        }

        public Party Create(PartyDTO partyDto)
        {
            return new Party()
            {
                PartyId = partyDto.PartyId,
                TeamId = partyDto.TeamId,
                GameId = partyDto.GameId,
                PartyName = partyDto.PartyName,
                PartyScore = partyDto.PartyScore,
            };
        }
    }
}
