﻿using BL.DTO;
using Domain;

namespace BL.ObjectFactories
{
    public class SoloFactory
    {
        public Solo Create(SoloDTO soloDto)
        {
            return new Solo()
            {
                SoloId = soloDto.SoloId,
                GameId = soloDto.GameId,
                SoloScore = soloDto.SoloScore,
                UserIntId = soloDto.UserIntId
            };
        }

        public SoloDTO Create(Solo solo)
        {
            return new SoloDTO()
            {
                SoloId = solo.SoloId,
                GameId = solo.GameId,
                UserIntId = solo.UserIntId,
                SoloScore = solo.SoloScore,
                GameName = solo.Game.GameName,
                UserFullName = solo.UserInt.FirstLastName,
                UserName = solo.UserInt.UserName
            };
        }
    }
}
