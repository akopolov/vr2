﻿using BL.DTO;
using Domain;

namespace BL.ObjectFactories
{
    public class GameModeFactory
    {
        public GameModeDTO Create(GameMode gameMode)
        {
            return new GameModeDTO()
            {
                GameModeId = gameMode.GameModeId,
                GameModeName = gameMode.GameModeName,
                PlayersInOneTeam = gameMode.PlayersInOneTeam,
                GameId = gameMode.GameId,
                GameName = gameMode.Game.GameName
            };
        }

        public GameMode Create(GameModeDTO gameModeDto)
        {
            return new GameMode()
            {
                GameModeId = gameModeDto.GameModeId,
                GameModeName = gameModeDto.GameModeName,
                PlayersInOneTeam = gameModeDto.PlayersInOneTeam,
                GameId = gameModeDto.GameId,
            };
        }
    }
}
