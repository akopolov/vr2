﻿using BL.DTO;
using Domain;

namespace BL.ObjectFactories
{
    public class TeamPersonInPartyFactory
    {
        public TeamPersonInParty Create(TeamPersonInPartyDTO teamPersonInPartyDto)
        {
            return new TeamPersonInParty()
            {
                TeamPersonInPartyId = teamPersonInPartyDto.TeamPersonInPartyId,
                PartyId = teamPersonInPartyDto.PartyId,
                PersonInTeamId = teamPersonInPartyDto.PersonInTeamId,
                PartyRoll = teamPersonInPartyDto.PartyRoll,
            };
        }

        public TeamPersonInPartyDTO Create(TeamPersonInParty teamPersonInParty)
        {
            return new TeamPersonInPartyDTO()
            {
                TeamPersonInPartyId = teamPersonInParty.TeamPersonInPartyId,
                PartyId = teamPersonInParty.PartyId,
                PersonInTeamId = teamPersonInParty.PersonInTeamId,
                PartyRoll = teamPersonInParty.PartyRoll,
                PartyName = teamPersonInParty.Party.PartyName,
                TeamName = teamPersonInParty.PersonInTeam.Team.TeamName,
                UserFullName = teamPersonInParty.PersonInTeam.User.FirstLastName,
            };
        }
    }
}
