﻿using BL.DTO;
using Domain;

namespace BL.ObjectFactories
{
    public class TeamFactory
    {
        public Team Create(TeamDTO teamDto)
        {
            return new Team()
            {
                TeamId = teamDto.TeamId,
                TeamName = teamDto.TeamName,
                TeamShortName = teamDto.TeamShortName,
                TeamRating = teamDto.TeamRating,
            };
        }

        public TeamDTO Create(Team team)
        {
            return new TeamDTO()
            {
                TeamId = team.TeamId,
                TeamName = team.TeamName,
                TeamShortName = team.TeamShortName,
                TeamRating = team.TeamRating,
            };
        }
    }
}
