﻿using BL.DTO;
using Domain;

namespace BL.ObjectFactories
{
    public class GameFactory
    {
        public Game Create(GameDTO gameDto)
        {
            return new Game()
            {
                GameId = gameDto.GameId,
                GameName = gameDto.GameName
            };
        }
        public GameDTO Create(Game game)
        {
            return new GameDTO()
            {
                GameId = game.GameId,
                GameName = game.GameName
            };
        }
    }
}
