﻿using BL.DTO;
using Domain;

namespace BL.ObjectFactories
{
    public class TournamentPersonScoreFactory
    {
        public TournamentPersonScore Create(TournamentPersonScoreDTO tournamentPersonScoreDto)
        {
            return new TournamentPersonScore()
            {
                TournamentPersonScoreId = tournamentPersonScoreDto.TournamentPersonScoreId,
                TournamentId = tournamentPersonScoreDto.TournamentId,
                SoloId = tournamentPersonScoreDto.SoloId,
                Score = tournamentPersonScoreDto.Score
            };
        }

        public TournamentPersonScoreDTO Create(TournamentPersonScore tournamentPersonScore)
        {
            return new TournamentPersonScoreDTO()
            {
                TournamentPersonScoreId = tournamentPersonScore.TournamentPersonScoreId,
                TournamentId = tournamentPersonScore.TournamentId,
                SoloId = tournamentPersonScore.SoloId,
                Score = tournamentPersonScore.Score,
                PersonFullName = tournamentPersonScore.Solo.UserInt.FirstLastName,
                TournamentName = tournamentPersonScore.Tournament.TournamentName,
                GameName = tournamentPersonScore.Tournament.Game.GameName,
                TournamentYear = tournamentPersonScore.Tournament.TournamentFromDate
            };
        }
    }
}
