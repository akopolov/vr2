﻿using BL.DTO;

namespace BL.IServices
{
    public interface IGameMatchService : IService<GameMatchDTO>
    {
        bool IsAdmin(int userId, int elementId);
    }
}
