﻿using BL.DTO;

namespace BL.IServices
{
    public interface ITeamService : IService<TeamDTO>
    {
        bool Add(TeamDTO teamDto, int userId);
        TeamInfoDTO GetTeamInfo(int id);
        bool IsManager(int userId, int teamId);
    }
}
