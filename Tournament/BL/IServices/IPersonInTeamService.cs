﻿using BL.DTO;

namespace BL.IServices
{
    public interface IPersonInTeamService : IService<PersonInTeamDTO>
    {
        bool CanUpdate(PersonInTeamDTO personInTeamDto);
        bool CanDelete(int personInTeamId);
        bool IsManager(int userId, int personInTeamId);
    }
}
