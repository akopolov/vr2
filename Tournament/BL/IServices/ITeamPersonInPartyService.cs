﻿using BL.DTO;

namespace BL.IServices
{
    public interface ITeamPersonInPartyService : IService<TeamPersonInPartyDTO>
    {
        bool IsManager(int userId, int teamPersonInPartyId);
    }
}
