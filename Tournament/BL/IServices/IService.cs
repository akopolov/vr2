﻿using System.Collections.Generic;

namespace BL.IServices
{
    public interface IService<T>
    {
        List<T> GetAll();
        //before ADD check if the all the FORIGEN KEYS EXISTS
        bool Add(T t);
        //before DELETE check if the ELEMENT EXISTS
        bool Delete(int id);
        T GetById(int id);
        //before UPDATE check if the PRIMARY KEY and the all the FORIGEN KEYS EXISTS
        bool Update(T t);
    }
}
