﻿using BL.DTO;

namespace BL.IServices
{
    public interface IPartyService : IService<PartyDTO>
    {
        bool IsManager(int userId, int partyId);
    }
}
