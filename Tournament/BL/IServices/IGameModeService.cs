﻿using BL.DTO;

namespace BL.IServices
{
    public interface IGameModeService : IService<GameModeDTO>
    {

    }
}
