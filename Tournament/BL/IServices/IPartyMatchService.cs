﻿using BL.DTO;

namespace BL.IServices
{
    public interface IPartyMatchService : IService<PartyMatchDTO>
    {
        bool IsAdmin(int userId, int elementId);
    }
}
