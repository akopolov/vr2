﻿using BL.DTO;

namespace BL.IServices
{
    public interface ISoloMatchService : IService<SoloMatchDTO>
    {
        bool IsAdmin(int userId, int elementId);
    }
}
