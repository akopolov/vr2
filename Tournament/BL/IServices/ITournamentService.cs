﻿using BL.DTO;

namespace BL.IServices
{
    public interface ITournamentService : IService<TournamentDTO>
    {
        bool IsAdmin(int userId, int tournamentId);
    }
}
