﻿using BL.DTO;
using System.Collections.Generic;

namespace BL.IServices
{
    public interface ITournamentPersonScoreService : IService<TournamentPersonScoreDTO>
    {
        bool IsAdmin(int userId, int elementId);

        List<TournamentPersonScoreDTO> GetByTournamentId(int tournamentId);
    }
}
