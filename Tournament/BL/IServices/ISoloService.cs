﻿using BL.DTO;
using System.Collections.Generic;

namespace BL.IServices
{
    public interface ISoloService : IService<SoloDTO>
    {

        List<SoloDTO> GetByGameId(int gameId);
        List<SoloDTO> GetByUserId(int userId);
        bool IsUser(int userId, int elementId);
    }
}
