﻿using BL.DTO;
using System.Collections.Generic;

namespace BL.IServices
{
    public interface ITournamentPartyScoreService : IService<TournamentPartyScoreDTO>
    {
        bool IsAdmin(int userId, int elementId);

        List<TournamentPartyScoreDTO> GetByTournamentId(int tournamentId);
    }
}
